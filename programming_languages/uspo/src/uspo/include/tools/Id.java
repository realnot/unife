package uspo.include.tools;

import uspo.interfaces.Global;

/**
 * La classe Id si occupa di implementare i metodi necessari alla generazione 
 * di un id casuale per l'individuazione univoca degli oggetti istanziati o 
 * salvati su file
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0405
 * @since 12.0405
 */
public class Id implements Global {

	private static long ID;
	
	private static boolean error;
	
	/** <p>
	 * Il metodo setUID ha il compito di generare (IDentificator),
	 * in modo da riconosce ogni utente, pacco, veicolo ecc... in modo univoco. 
	 * Certo generando il numero in modo casuale vi e' sempre una probabilita' 
	 * che due cliente ottengano lo stesso UID, anche se, bisogna tenere conto 
	 * che il numero generato e' compreso:
	 * </p>
	 *  	
	 *  <pre>
	 *  	tra '1.000.000.000.000.000.000' 
	 *  	  e '9.223.372.036.854.775.807'
	 *  </pre>
	 *  
 	 * <p>
 	 * Come potete vedere le probabilita' che due oggetti abbiano lo stesso ID
 	 * e' veramente minima. L'altra soluzione sarebbe quella di memorizzare
 	 * l'ID in un file a parte, leggerlo, ed assegnare all'utente successivo
 	 * l'ID successivo, in modo progressivo. Sicuramente l'ultimo metodo
 	 * risulta piu' elegante e professionale di quello utilizzato in questa sede,
 	 * ma visto che rimaniamo in ambito universitario e puramente didattico,
 	 * la soluzione adottata andra' sicuramente bene.
 	 * </p>
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.Math#random()
	 * 
	 * @version 12.0405
	 * @since 12.0405
	 */
	private static void setID() {
		do {
			error = false;
			
			try {
				ID = (long)(Math.random() * MAX + MIN);
			} catch (ArithmeticException e) {
				error = true;
			}
		} while(error);
	}
	
	/** 
	 * @retur l'ID appena calcolato
	 */
	public static long getID() { setID(); return ID; }
}
