package uspo.include.tools;

import uspo.include.io.Data;
import uspo.include.shipment.Queue;
import uspo.interfaces.Global;
import uspo.main.Vehicle;

/**
 * La classe si occupa di gestire l'eccezione, in questo caso un guasto del 
 * motore, la classe eredita da Exception
 *
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.lang.Exception
 * 
 * @version 12.0904
 * @since 12.0904
 */
public class EngineFaulthException extends Exception implements Global{
	
	private static final long serialVersionUID = 2363920611072600961L;

	/**
	 * il metodo si occupa di esegure l'azione per gestire l'eccezione, in 
	 * questo caso un guasto del motore
	 * 
	 * @param the_vehicle, il veicolo in cui effettuare le operazioni
	 * @param the_queue, l'oggetto che contiene le varie code, necessario per 
	 * eseguire lo spostamento del veicolo da una coda all'altra
	 * 
	 * @version 12.0907
	 * @since 12.0904
	 */
	public void doAction(Vehicle the_vehicle, Queue the_queue) {
		
		System.out.println("Il veicolo " + the_vehicle.getName() + " ha avuto" +
				" un guasto al motore durante il viaggio.");
		the_queue.getMoving().remove(the_vehicle);
		the_queue.getBroken().add(the_vehicle);
		@SuppressWarnings("unused")
		Data shipmentHistory = new Data("Il veicolo " + the_vehicle + "ha " +
				"terminato il suo viaggio per un guasto al motore.\n",
				SHIPMENTS_PATH, WRITE_STRING);
	}
}