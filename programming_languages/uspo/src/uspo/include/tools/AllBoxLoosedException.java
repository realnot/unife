package uspo.include.tools;

import uspo.include.io.Data;
import uspo.interfaces.Global;
import uspo.main.Vehicle;

/**
 * La classe si occupa di gestire l'eccezione, in questo caso la perdita di tutti 
 * i pacchi, la classe eredita da Throwable
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.lang.Exception
 * 
 * @version 12.0904
 * @since 12.0904
 */
public class AllBoxLoosedException extends Exception implements Global {
	
	private static final long serialVersionUID = 2150929763048727647L;

	/**
	 * il metodo si occupa di esegure l'azione per gestire l'eccezione, in 
	 * questo caso tutti i pacchi vengono persi
	 * 
	 * @param the_vehicle, il veicolo in cui effettuare le operazioni
	 * 
	 * @version 12.0907
	 * @since 12.0904
	 */
	public void doAction(Vehicle the_vehicle) {
		
		if(!the_vehicle.getTheLoad().isEmpty()) {
			System.out.println("Il veicolo " + the_vehicle + " ha perso tutti " +
					"i pacchi durante il viaggio.");
			the_vehicle.getTheLoad().clear();
			@SuppressWarnings("unused")
			Data shipmentHistory = new Data(" - ha perso tutti i pacchi " +
					"durante il viaggio.\n", SHIPMENTS_PATH, WRITE_STRING);
		}
	}
}
