package uspo.include.tools;

import uspo.include.io.Data;
import uspo.interfaces.Global;
import uspo.main.Box;
import uspo.main.Vehicle;

/**
 * La classe si occupa di gestire l'eccezione, in questo caso il destinatario 
 * inesistente, la classe eredita da Throwable
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.lang.Exception
 * 
 * @version 12.0904
 * @since 12.0904
 */
public class RecipientNotFoundException extends Exception implements Global{
	
	private static final long serialVersionUID = 1846886187159397235L;
	
	/**
	 * il metodo si occupa di esegure l'azione per gestire l'eccezione, in 
	 * questo caso non viene trovato il destinatario di un pacco, il pacco 
	 * viene semplicemente eliminato
	 * 
	 * @param the_vehicle, il veicolo in cui effettuare le operazioni
	 * 
	 * @version 12.0907
	 * @since 12.0904
	 */
	public void doAction(Vehicle the_vehicle) {
		
		int index;
		Box the_box = null;
		
		if(!the_vehicle.getTheLoad().isEmpty()) {
			System.out.println("Impossibile trovare il destinatario di un pacco " +
					"il pacco verra' quindi eliminato");
			index = (int)(Math.random() * (the_vehicle.getTheLoad().size() -1) + 0);
		
			the_vehicle.getTheLoad().get(index);
			the_vehicle.getTheLoad().remove(index);
		
			@SuppressWarnings("unused")
			Data shipmentHistory = new Data(" - Non ha trovato il destinatario" +
					" del " + the_box + ".\n", SHIPMENTS_PATH, WRITE_STRING);
		}
	}
}
