package uspo.include.algorithms.dijkstra;

import java.util.List;

/**
 * Una volta definite le basi di un grafo, ovvero i nodi e gli archi, possiamo
 * creare il grafo stesso. Il grafo e' basato su due liste. La prima contiene
 * l'insieme di tutti i vertici, mentre la seconda contiene l'insieme di tutti
 * gli archi.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see uspo.include.algorithms.dijkstra.Vertex
 * @see uspo.include.algorithms.dijkstra.Edge
 * 
 * L'interfaccia List estende Collection (che e' la base con cui viene 
 * costruito il Collections Framework) e serve per memorizzare una sequenza di
 * elementi.
 * 
 * Alcuni metodi possono lanciare veri tipi di Exception, come:
 * 
 * - UnsupportedOperationException: se l'elenco non puo' essere modificato
 * @see java.lang.Runtime.UnsupportedOperationException
 * 
 * - ClassCastException: quando un oggetto e' incompatibile con un'altro
 * @see java.lang.Runtime.ClassCastException
 * 
 * - IndexOfBoundsException: se viene utilizzato un indice non valido
 * @see java.lang.Runtime.IndexOutOfBoundsException
 * 
 * - NullPointerException: se si tenta di memorizzare un oggetto 'Null' e
 * nell'elenco non sono ammessi elementi 'null'.
 * @see java.lang.Runtime.NullPointerException
 * 
 * @version 12.0831
 * @since 12.0824
 */
public class Graph {
	
	private final List<Vertex> vertexes;
	private final List<Edge> edges;

	/**
	 * Graph(List<Vertex>, List<Edge>) e' un costruttore che inizializza le 
	 * variabili di istanza del grafo copiando le liste nelle variabili di 
	 * istanza
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	public Graph(List<Vertex> vertexes, List<Edge> edges) {
		this.vertexes = vertexes;
		this.edges = edges;
	}

	/**
	 * @return la lista dei nodi
	 */
	public List<Vertex> getVertexes() {
		return vertexes;
	}

	/**
	 * @return la lista degli archi
	 */
	public List<Edge> getEdges() {
		return edges;
	}
}
