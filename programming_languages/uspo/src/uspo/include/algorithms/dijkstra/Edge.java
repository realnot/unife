package uspo.include.algorithms.dijkstra;

/**
 * I nodi sono collegati tra loro da archi (edge, margin, board, ecc...). 
 * Ogni arco e' identificato da 'id', collega il nodo di partenza 'source' al 
 * nodo di arrivo/destinazione 'destination'. Il cambio di uno stato/posizione 
 * dal nodo X al nodo Y ha un peso/costo (puo' essere il tempo, il costo della 
 * benzina, un semplice valore intero, ecc..).
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0831
 * @since 12.0824
 */
public class Edge {
	
	private final String id; 
	private final Vertex source;
	private final Vertex destination;
	private final int weight; 

	/**
	 * Edge(String,Vertex,Vertex,int) e' un costruttore che inizializza le 
	 * variabili di istanza copiando il valore dai parametri
	 * 
	 * @param id, l'id del cammino
	 * @param source, il nodo di partenza
	 * @param destination, il nodo di arrivo
	 * @param weight, il peso dell'arco tra source e destination
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	public Edge(String id, Vertex source, Vertex destination, int weight) {
		this.id = id;
		this.source = source;
		this.destination = destination;
		this.weight = weight;
	}

	/**
	 * @return l'id dell'arco
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return il nodo di destinazione dell'arco
	 */
	public Vertex getDestination() {
		return destination;
	}

	/**
	 * @return il nodo di partenza dell'arco
	 */
	public Vertex getSource() {
		return source;
	}
	
	/**
	 * @return il peso dell'arco
	 */
	public int getWeight() {
		return weight;
	}
	
	/**
	 * Il metodo e' un Override del metodo toString() per semplificare l'utilizzo 
	 * dei dati che sono utilizzati maggiormente dell'oggetto
	 * 
	 * @return una stringa contenente i dati dell'oggetto
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	@Override
	public String toString() {
		return source + " " + destination;
	}
}
