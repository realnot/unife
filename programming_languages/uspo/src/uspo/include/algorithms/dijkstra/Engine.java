package uspo.include.algorithms.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import uspo.include.algorithms.dijkstra.Edge;
import uspo.include.algorithms.dijkstra.Graph;
import uspo.include.algorithms.dijkstra.Vertex;

/**
 * L'algoritmo di dijkstra divide tutti i nodi in due gruppi (meglio dire
 * in due Set) di tipo 'Vertex', settledNodes ed unSettledNodes. Inizialmente
 * tutti i nodi appartengono a unSettledNodes e vengono rimossi da questo
 * set per essere inserite nel set SettledNodes solo quando viene trovato
 * il cammino minimo da 'source' a 'destination'.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.util.Set
 * @see uspo.include.algorithms.dijkstra.Vertex
 * 
 * @version 12.0831
 * @since 12.0824
 */
public class Engine {

	@SuppressWarnings("unused")
	private final List<Vertex> nodes;
	private final List<Edge> edges;
	private Set<Vertex> settledNodes;
	private Set<Vertex> unSettledNodes;
	private Map<Vertex, Vertex> predecessors;
	private Map<Vertex, Integer> distance;

	/**
	 * Engine(Graph) e' un costruttore che copia la lista di nodi e archi del 
	 * grafo nelle variabili di istanza in modo da poterci lavorare senza 
	 * modificare il grafo originale
	 * 
	 * @param graph, il grafo che contiene gli archi e i nodi da copiare
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	public Engine(Graph graph) {
		
		this.nodes = new ArrayList<Vertex>(graph.getVertexes());
		this.edges = new ArrayList<Edge>(graph.getEdges());
	}
	
	/**
	 * Il metodo esecute, esegue la sequenza necessaria di operazioni
	 * per inizializzare l'algoritmo dei cammini minimi.
	 * 
	 * @param source, il nodo da cui partire
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	public void execute(Vertex source) {
		
		settledNodes = new HashSet<Vertex>();
		unSettledNodes = new HashSet<Vertex>();
		distance = new HashMap<Vertex, Integer>();
		predecessors = new HashMap<Vertex, Vertex>();
		distance.put(source, 0);
		unSettledNodes.add(source);
		
		while (unSettledNodes.size() > 0) {
			Vertex node = getMinimum(unSettledNodes);
			
			settledNodes.add(node);
			unSettledNodes.remove(node);
			
			findMinimalDistances(node);
		}
	}

	/**
	 * Dato in ingresso un nodo, il metodo trova la distanza minima tra quel
	 * nodo e quelli adiacenti.
	 * 
	 * @param node, il nodo da cui partire.
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	private void findMinimalDistances(Vertex node) {
		
		List<Vertex> adjacentNodes = getNeighbors(node);
		
		for (Vertex target : adjacentNodes) {
			if (getShortestDistance(target) > getShortestDistance(node) 
					+ getDistance(node, target)) {
				
				distance.put(target, getShortestDistance(node) + 
						getDistance(node, target));
					
				predecessors.put(target, node);
				unSettledNodes.add(target);
			}
		}
	}

	/**
	 * Dati due nodi, source and destination, il metodo ritorna la distanza in
	 * km tra i due punti.
	 * 
	 * @param source, il nodo di partenza
	 * @parma dest, il nodo di arrivo
	 * @return weight, the cost from source to dest
	 * 
	 * @version 12.0831
	 * @since 12.0825
	 */
	public short getDistance(Vertex source, Vertex dest) {
	
		for (Edge edge : edges) {
			if (edge.getSource().equals(source) && edge.getDestination().equals(dest)) {
				return (short) edge.getWeight();
			}
		}
		return -1;
	}

	/**
	 * Dato un nodo in ingresso, il metodo si occupa di trovare tutti i nodi
	 * adiacenti,
	 * 
	 * @param node, il nodo da cui partire
	 * @return List<Vertex> la lista di adiacenze
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	private List<Vertex> getNeighbors(Vertex node) {
	
		List<Vertex> neighbors = new ArrayList<Vertex>();
		
		for (Edge edge : edges) {
			if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
				neighbors.add(edge.getDestination());
			}
		}
		
		return neighbors;
	}

	/**
	 * Dato un nodo in ingresso, il metodo si occupa di trovare il nodo che ha
	 * la distanza minima da tutti gli altri, appena trovato lo ritorna.
	 * 
	 * @param vertex, un set di nodi
	 * @return Il nodo minimo
	 * 
	 * @version 12.0831
	 * @since 12.0824
	 */
	private Vertex getMinimum(Set<Vertex> vertexes) {

		Vertex minimum = null;
			
		for (Vertex vertex : vertexes) {
			if (minimum == null) {
				minimum = vertex;
			} else {
				if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
					minimum = vertex;
				}
			}
		}
		return minimum;
	}

	/**
	 * Dato in ingresso un nodo, il metodo controlla che esso sia presente nella
	 * lista dei nodi settati.
	 * 
	 * @return true se il nodo e' presente nella lista, altrimenti false
	 * @param vertex, il nodo da controllare
	 * 
	 * @version
	 * @since
	 */
	private boolean isSettled(Vertex vertex) {
		return settledNodes.contains(vertex);
	}

	/**
	 * Data in ingresso una destinazione, il metodo cerca se esiste una key
	 * corrispondente in 'distance', se la trova allora quella e' la distanza
	 * minima.
	 * 
	 * @return la distanza minima
	 * @param destination, la destinazione
	 * 
	 * @version
	 * @since
	 */
	private int getShortestDistance(Vertex destination) {
		
		Integer d = distance.get(destination);
		
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

	/**
	* Il metodo ritorna il path da source al target selezionato (destination)
	* oppure NULL se non esiste alcun path.
	* 
	* @param target, la destinazione.
	* @return path, il path oppure null
	* 
	* @version 12.0822
	* @since 12.0818
	*/
	public LinkedList<Vertex> getPath(Vertex target) {
	
		LinkedList<Vertex> path = new LinkedList<Vertex>();
		Vertex step = target;
	
		// Check if a path exists
		if (predecessors.get(step) == null) {
			return null;
		}
		
		path.add(step);
	
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add(step);
		}

		// Put it into the correct order
		Collections.reverse(path);
		
		return path;
	}
}