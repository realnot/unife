package uspo.include.algorithms.dijkstra;

/**
 * La classe vertex() descrive le caratteristiche dei nodi. Ogni nodo ha un nome
 * ed un id. Per l'id viene effettuato in override del metodo hashCode() di
 * Object() per ritornare un hash della stringa id.
 * 
 * Se due nodi sono uguali equals() ritornera' true ed hashCode() restituira' lo
 * stesso valore per entrambi.
 * 
 * Se x.equals(y) <=> x.hashCode() == y.hashCode()
 * 
 * Due nodi devono risultare uguali anche se sono due istanze
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0824
 * @since 12.0824
 */
public class Vertex {

	final private String id;
	final private String name;

	/**
	 * Vertex(String, String) e' un costruttore che inizializza le variabili di 
	 * istanza copiando i valori dei parametri
	 * 
	 * @param id, l'id del nodo
	 * @param name, il nome del nodo
	 * 
	 * @version 12.0824
	 * @since 12.0824
	 */
	public Vertex(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	/**
	 * @return l'id del nodo
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return il nome del nodo
	 */
	public String getName() {
		return name;
	}

	/**
	 * Viene effettuato un override del metodo hashCode di Object perche' abbiamo 
	 * bisogno che due nodi, anche se sono istanze diverse, risultino uguali, 
	 * non e' possibile con il metodo hashCode di Object in quanto istanze diverse 
	 * hanno hashCode diverso, in caso di nodi uguali questo metodo ritorna lo 
	 * stesso risultato
	 * 
	 * @return il risultato delle operazioni
	 * 
	 * @see Object#hashCode()
	 * 
	 * @version 12.0824
	 * @since 12.0824
	 */
	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Viene effettuato un overriding del metodo equals di Object perche' sono 
	 * necessari controlli specifici per dire se un nodo e' identico all'altro
	 * 
	 * @return true se i nodi sono uguali, altrimenti false
	 * 
	 * @see Object#equals(Object)
	 * 
	 * @version 12.0824
	 * @since 12.0824
	 */
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) 
			return true; 
		if (obj == null) 
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Vertex other = (Vertex) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	/**
	 * Il metodo e' un Override del metodo toString() per semplificare l'utilizzo 
	 * dei dati che sono utilizzati maggiormente dell'oggetto
	 * 
	 * @return una stringa contenente i dati dell'oggetto
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0824
	 * @since 12.0824
	 */
	@Override
	public String toString() {
		return name;
	}
}
