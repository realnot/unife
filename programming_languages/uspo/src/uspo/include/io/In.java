package uspo.include.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;

/**
 * La classe incorpora i vari metodi per gestire gli input da tastiera. 
 * Ogni metodo cerca di recuperare il tipo di input richiesto all'interno di un 
 * Buffer per poi ritornarlo come valore dello stesso tipo. Per fare questo 
 * viene implementata l'interfaccia Serializable
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.io.Serializable
 * @see java.io.BufferedReader
 * @see java.io.InputStreamReader
 * 
 * @version 12.0901
 * @since 12.0415
 */

@SuppressWarnings("serial")
public class In implements Serializable {
	
	protected static char c;
	protected static String s;
	protected static boolean error;
	
	/**
	 * Il metodo utilizza BufferReader per recuperare un flusso di input 
	 * memorizzato in un buffer. Successivamente viene usato InputStramReader 
	 * (sottoclasse di Reader) per convertire il flusso da byte in caratteri 
	 * 
	 * @return Char. Il metodo torna il carattere inserito da tastiera.
	 * @exception e L'eccezione si verifica quando il metodo read() non e' 
	 * stato in grado di leggere il carattere dal flusso di input. Nel caso 
	 * questo dovesse accadere, il metodo torna 'E'
	 * 
	 * @version 12.0901
	 * @since 12.0415
	 */
	public static char readChar() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			c = (char) br.read();
			error = false;
			return c;
		}
		catch(Exception e) {
			System.out.println("Parametro non consentito.");
			error = true;
			return 'E';
		}
	}
	
	/**
	 * Il metodo utilizza BufferReader per recuperare un flusso di input 
	 * memorizzato in un buffer. Successivamente viene usato InputStramReader 
	 * (sottoclasse di Reader) per convertire il flusso da byte in caratteri 
	 * 
	 * @return string. Il metodo ritorna la stringa inserita da tastiera 
	 * @exception e L'eccezione si verifica nel caso in cui readLine(); 
	 * membro di BufferedReader non e' stato in grado di restituire l'oggetto 
	 * di tipo String, ovvero la stringa nel buffer. Nel caso questo dovesse 
	 * accadere, il metodo torna 'Error'
	 * 
	 * @version 12.0901
	 * @since 12.0415
	 */
	public static String readString() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			s = br.readLine();
			error = false;
			return s;
		}
		catch (Exception e) {
			System.out.println("Parametro non consentito.");
			error = true;
			return "Error";
		}
	}
	
	/**
	 * Il metodo sfrutta il metodo readString() per leggere una stringa 
	 * e successivamente chiama valueOf() per convertire la stringa in un 
	 * valore di tipo intero
	 * 
	 * @return int Il metodo torna il carattere inserito da tastiera
	 * @exception e L'eccezione si verifica nel caso in cui intValue(); non 
	 * sia stato in grado di convertire la stringa nel rispettivo int.
	 * Nel caso questo dovesse accadere, il metodo torna '0'
	 * 
	 * @version 12.0901
	 * @since 12.0415
	 */
	public static int readInt() {
		s = readString();
		if(error) return 0;
		else {
			try {
				return Integer.valueOf(s).intValue();
			}
			catch(Exception e) {
				System.out.println("Parametro non consentito.");
				error = true;
				return 0;
			}
		}
	}
	
	/**
	 * Il metodo sfrutta il metodo readString() per leggere una stringa 
	 * e successivamente chiama valueOf() per convertire la stringa in un 
	 * valore di tipo double
	 * 
	 * @return double. Il metodo torna il carattere inserito da tastiera
	 * @exception e L'eccezione si verifica nel caso in cui doubleValue() 
	 * non sia stato in grado di convertire la stringa nel rispettivo double.
	 * Nel caso questo dovesse accadere, il metodo torna '0'
	 * 
	 * @version 12.0901
	 * @since 12.0415
	 */
	public static double readDouble() {
		s = readString();
		if(error) return 0;
		else {
			try {
				return Double.valueOf(s).doubleValue();
			}
			catch(Exception e) {
				System.out.println("Parametro non consentito.");
				error = true;
				return 0;
			}
		}
	}
}