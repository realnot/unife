package uspo.include.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

// Custom Interfaces
import uspo.interfaces.Global;
import uspo.interfaces.Lang;

/** 
 * La classe Data si occupa di fornire l'implementazione tramite vari metodi 
 * per l'utilizzo dei file (scrittura, lettura, cancellazione, test, ecc)
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0901
 * @since 12.0127
 */
public class Data implements Global, Lang {
	
	private Object the_object;
	
	private String the_path = "";
	private String the_string = "";
	private String the_file_name = "";
	
	private boolean is_file = false;
	private boolean is_directory = false;
	private boolean is_readable = false;
	private boolean is_writeable = false;
	private boolean exists = false;
	@SuppressWarnings("unused")
	private short the_action;
	
	/**
	 * Il costruttore riceve in ingresso tre parametri che serviranno per andare 
	 * ad inizializzare le variabili di istanza, successivamente lancia due 
	 * metodi: getPath() per memorizzare le informazioni sul file e in fine 
	 * getPerformOperation() per eseguire l'operazione voluta<p>
	 * 
	 * @param the_object l'oggetto su cui si vuole lavorare
	 * @param the_path la posizione del file (il percorso)
	 * @param the_action l'operazione da effettuare sul file (read/write)
	 * 
	 * @see #getPath()
	 * @see #getPerformAction(short)
	 * 
	 * @version 12.0901
	 * @since 12.0127
	 */
	public Data(Object the_object, String the_path, short the_action) {

		this.the_object = the_object;
		this.the_path = the_path;
		this.the_action = the_action;
		
		getPath();
		getPerformAction(the_action);
	}
	
	/**
	 * Questo costruttore riceve in ingresso tre parametri, inizializza le 
	 * variabili di istanza e lancia i relativi metodi in base ai parametri 
	 * inseriti.<p>
	 * 
	 * @param the_string la stringa su cui si vuole lavorare
	 * @param the_path la posizone del file
	 * @param the_action l'azione che si intende effettuare sul file.
	 * 
	 * @see #getPath()
	 * @see #getPerformAction(short)
	 * 
	 * @version 12.0901
	 * @since 12.0127
	 */
	public Data(String the_string, String the_path, short the_action) {
		
		this.the_string = the_string;
		this.the_path = the_path;
		this.the_action = the_action;
		
		getPath();
		getPerformAction(the_action);
	}
	
	/**
	 * Il metodo effettua il caching delle informazioni recuperate tramite 
	 * l'istanza di un oggetto File su 'the_path'. Queste informazioni 
	 * inizializzano le variabili di istanza di questa classe, rendendo di fatto 
	 * semplice il controllo e la gestione degli errori per tutte le classi che 
	 * sono contenute in questa.<p>
	 * 
	 * @see java.io.File
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	final private void setPath() {
		
		File the_file = new File(the_path);
		
		exists = the_file.exists();
		is_file = the_file.isFile();
		is_directory = the_file.isDirectory();
		is_readable = the_file.canRead();
		is_writeable = the_file.canWrite();
		the_file_name = the_file.getName();
	}
	
	/**
	 * Il metodo crea una nuova istanza di File basata su 'the_path' dato in 
	 * ingresso, successivemante chiama i relativi metodi per ottenere 
	 * informazioni sul 'path' e stamparle a video. Metodo molto utile in fase 
	 * di debugging<p>
	 * 
	 * @see java.io.File
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	final private void setPathTest() {
		
		File the_test_file = new File(the_path);
		
		System.out.println("Testing on... " + the_path + "\n");
		System.out.println("File name: " + the_test_file.getName());
		System.out.println("The path: " + the_test_file.getPath());
		System.out.println("The abosolute path: " + the_test_file.getAbsolutePath());
		System.out.println("Parent: " + the_test_file.getParent());
		System.out.println(the_test_file.exists() ? "exists" : "does not exists");
		System.out.println(the_test_file.isHidden() ? "is hiden" : "is not hiden");
		System.out.println(the_test_file.canRead() ? "is readable" : "is not readable");
		System.out.println(the_test_file.canWrite() ? "is writeable" : "is not writeable");
		System.out.println(the_test_file.canExecute() ? "is executable" : "is not executable");
		System.out.println("is " + (the_test_file.isDirectory() ? "not " : " a directory"));
		System.out.println(the_test_file.isFile() ? "is file" : "is not a file");
		System.out.println(the_test_file.isAbsolute() ? "is absolute" : "is not absolute");
		System.out.println("Last modified: " + the_test_file.lastModified());
		System.out.println("File size: " + the_test_file.length() + "Bytes");
		System.out.println("Free space: " + the_test_file.getFreeSpace() + "Bytes");
		System.out.println("Usable space: " + the_test_file.getUsableSpace() + "Bytes");
		System.out.println("Total space: " + the_test_file.getTotalSpace() + "Bytes");
	}
	
	/**
	 * Questo metodo riceve in ingresso l'operazione da effettuare, crea un' 
	 * istanza dell'oggetto (in base a the_action) per poi invocare il 
	 * il relativo metodo atto ad eseguire l'azione definita da 'the_action'<p>
	 * 
	 * @param the_action l'operazione da effettuare (scrittura/lettura).
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	final private void setPerformAction(short the_action) {
		
		switch(the_action) {
			case WRITE_OBJECT:
				DataObject writeObject = new DataObject();
				writeObject.getWriteObject();
				break;
			case READ_OBJECT: 
				DataObject readObject = new DataObject();
				readObject.getReadObject();
				break;
			case WRITE_STRING:
				DataString writeString = new DataString();
				writeString.getWriteString(true);
				break;
			case READ_STRING:
				DataString readString = new DataString();
				readString.getReadString();
				break;
			case DELETE_FILE:
				DataString deleteFile = new DataString();
				deleteFile.getWriteString(false);
				break;
		}
	}
	
	/**
	 * Metodo getter per invocare setPath ed eseguire il caching delle 
	 * informazioni relative ad un file.
	 * 
	 * @see #setPath()
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	public final void getPath() { 
		setPath(); 
	}
	
	/**
	 * Metodo getter per invocare setPerformAction ed eseguire l'operazione 
	 * voluta, definita da 'the_action'.
	 * 
	 * @param the_action il tipo di operazione da effettuare (lettura/scrittura).
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	public final void getPerformAction(short the_action) { 
		setPerformAction(the_action); 
	}
	
	/**
	 * Il metodo invoca setPathTest() per eseguire un test sul file, 
	 * stampando a video tutte le informazioni.
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	public final void getPathTest() { 
		setPathTest(); 
	}
	
	/**
	 * Questa classe fa uso della serializzazione. La serializzazione e' il 
	 * processo di scrittura dello stato di un oggeto in un flusso di byte. 
	 * Questo processo ci torna utile ogni qual volta desideriamo salvare 
	 * lo stato di un programma in un'area di memorizzazione persistente, come 
	 * ad esempio un file.<p>Per fare questo, la classe fa uso di due 
	 * metodi, che sono rispettivamente setWriteObject() e setReadObject(). 
	 * Il primo metodo viene usato per salvare lo stato di un oggetto in un file 
	 * (serialization). Il secondo viene usato per leggere lo stato di un 
	 * oggetto da un file (deserialization).<p>
	 * 
	 * @see java.io.Serializable
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	private final class DataObject {
		
		/**
		 * Questo metodo crea un FileOutputStream che fa riferimento ad un 
		 * file dato in ingresso 'path' e viene generato un ObjectOutputStream 
		 * per quel flusso di file.<p>Il metodo writeObject() di ObjectOutputStream 
		 * viene quindi utilizzato per serializzare l'oggetto. Il flusso di output 
		 * dell'oggetto viene svuotato con flush() e successivamente chiuso con 
		 * close().<p>
		 * 
		 * @param object L'oggetto che si vuole serializzare
		 * @param the_path Il percorso del file dell'oggetto che si vuole 
		 * deserializzare.<p>
		 * 
		 * @exception NotSerializableException Viene lanciata nel caso in 
		 * cui la classe a cui fa riferimento l'oggetto non implementa 
		 * l'interfaccia Serializable.
		 * 
		 * @see java.io.IOException
		 * @see java.io.FileOutputStream
		 * @see java.io.ObjectOutputStream
		 * 
		 * @version 12.0901
		 * @since 12.0402
		 */
		private final void setWriteObject() {
			
			try {
				
				System.out.println("Inizialized object serialization...");
				
				FileOutputStream fos = new FileOutputStream(the_path);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				
				oos.writeObject(the_object);
				oos.flush();
				oos.close();
				
				System.out.println("[ OK ]\n");
				
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * Questo metodo crea un FileInputStream che fa riferimento al file 
		 * dato in ingresso 'the_path' e viene generato un ObjectInputStream per 
		 * quel flusso di file.<p> Successivamente viene utilizzato il metodo 
		 * readObject() di ObjectOutputStream per deserializzare l'oggetto. 
		 * In fine, il flusso di input dell'oggetto viene chiuso con close().<p>
		 * 
		 * <strong>Nota:</strong> l'esistenza del metodo toString() e' veramente 
		 * utile in questo caso, permette di mostrare a video l'ogetto stampato 
		 * se lo usiamo insieme al metodo print() o println();
		 * 
		 * @exception NotSerializableException Viene lanciata nel caso in 
		 * cui la classe a cui fa riferimento l'oggetto non implementa 
		 * l'interfaccia Serializable.<p>
		 *
		 * @exception ClassNotFoundException il metodo readObject() vuole sapere 
		 * la firma dell'oggetto che si intende deserializzare,
		 *
		 * @see java.lang.Object#toString()
		 * @see java.lang.ClassNotFoundException
		 * @see java.io.IOException
		 * @see java.io.FileInputStream
		 * @see java.io.ObjectInputStream
		 * 
		 * @version 12.0901
		 * @since 12.0402
		 */
		private final void setReadObject() {
			
			try {
				
				System.out.println("Inizialized object deserialization...");
				
				FileInputStream fis = new FileInputStream(the_path);
				ObjectInputStream ois = new ObjectInputStream(fis);
				the_object = (Object)ois.readObject(); 
				
				ois.close();
				
				System.out.println("[ OK ]\n");
				
			} catch(IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * Il metodo lancia la serializzazione di un oggetto
		 * 
		 * @version 12.0901
		 * @since 12.0402
		 */
		public void getWriteObject() { setWriteObject(); }
		
		/**
		 * Il metodo lancia la deserializzazione di un oggetto
		 * 
		 * @version 12.0901
		 * @since 12.0402
		 */
		public Object getReadObject() { setReadObject(); return the_object; }
		
	}
	
	/**
	 * Questa classe utilizza FileReader, FileWriter e BufferedReader per 
	 * leggere/scrivere una stringa da/su file.<p>
	 * 
	 * @version 12.0901
	 * @since 12.0402
	 */
	private final class DataString {
		
		private FileReader fr;
		private FileWriter fw;
		private BufferedReader br;
		private BufferedWriter bw;
		
		/**
		 * Questo metodo utilizza FileWriter che estende la classe Writer per 
		 * scrivere il contenuto di una stringa su un file. FileWriter genera 
		 * il file prima di aprirlo per l'output quando si crea l'oggetto e, nel 
		 * caso in cui si cerchi di aprire un file di sola lettura, viene 
		 * lanciata una IOException.<p>
		 * 
		 * @exception IOException avviene nel caso in cui non sia stato 
		 * possibile scrivere su file, o qualsiasi altra operazione di input sul 
		 * file non sia andata a buon fine
		 * 
		 * @param true se il file viene aperto in modalita append, in caso 
		 * contrario false
		 * 
		 * @see java.io.IOException
		 * @see java.io.FileWriter
		 * 
		 * @version 12.0906
		 * @since 12.0407
		 */
		final private void setWriteString(boolean append) {
			
			if(!exists) {
				try {
					fw = new FileWriter(the_path, append);
					bw = new BufferedWriter(fw);
					bw.write(the_string);
					bw.close();
				} catch (IOException e) {
					System.out.println("Eseguo un test sul file:\n");
					getPathTest();
					
					e.printStackTrace();
				}
			} else {
				if(is_file) {
					if(is_writeable) {
						try {
							fw = new FileWriter(the_path, append);
							bw = new BufferedWriter(fw);
							bw.write(the_string);
							bw.close();
						} catch (IOException e) {
							System.out.println("Eseguo un test sul file:\n");
							getPathTest();
							
							e.printStackTrace();
						}
					} else {
						System.out.print("Errore, il file non e' leggibile.");
					}
				} else if(is_directory) {	
					System.out.println("e' una directory");
				} else {
					System.out.println(the_file_name + 
							"non e' un file" + " e " +
							"non e' una directory");
					
					System.exit(EXIT_FAILURE);
				}
			}
		}
		
		/**
		 * Questo metodo fa uso della classe astratta FileReader (che estende) 
		 * Reader per leggere il contenuto di un file. Per leggere il file, il 
		 * metodo fa uso della classe BufferedReader, che incrementa le 
		 * prestazioni memorizzando il flusso in un buffer.<p>
		 * 
		 * @exception IOException avviene nel caso in cui non sia stato 
		 * possibile leggere una stringa dal file.
		 * 
		 * @exception FileNotFoundException  avviene nel caso in cui il file 
		 * non e' leggibile, oppure il percorso del file non e' corretto.
		 * 
		 * @see java.io.IOException
		 * @see java.io.FileNotFoundException
		 * @see java.io.FileReader
		 * @see java.io.BufferedReader
		 * 
		 * @version 12.0901
		 * @since 12.0407
		 */
		private final void setReadString() {
			
			boolean found = false;
			
			if(exists) {
				if(is_file) {
					if(is_readable) {
						try {
							fr = new FileReader(the_path);
							br = new BufferedReader(fr);
							
							try {
								while((the_string = br.readLine()) != null) {
									//stampo la stringa letta
									System.out.println(the_string);
									found = true;
								}
								if(!found)
									System.out.println("File vuoto");
							} catch (IOException e) {
								System.out.println("Impossibile leggere la stringa");
								System.out.println("Eseguo un test sul file:\n");
								getPathTest();
								
								e.printStackTrace();
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					} else {
						System.out.print("Errore, il file non e' leggibile.");
					}
				} else if(is_directory) {
					System.out.println("e' una directory");
				} else {
					System.out.println(the_file_name + 
							"non e' un file" + " e " +
							"non e' una directory");
					
					System.exit(EXIT_FAILURE);
				}
			} else {
				System.out.println("Errore, il file non esiste");
			}
		}
		
		/**
		 * Il metodo lancia setWriteString per scrivere una string su un file
		 * 
		 * @version 12.0901
		 * @param b 
		 * @since 12.0407
		 */
		public void getWriteString(boolean append) { setWriteString(append); }
		
		/**
		 * Il metodo lancia setReadString per leggere un file.
		 * 
		 * @version 12.0901
		 * @since 12.0407
		 */
		public void getReadString() { setReadString(); }
	}
}
