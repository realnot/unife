package uspo.include.shipment;

import uspo.include.algorithms.dijkstra.*;
import uspo.include.tools.AllBoxLoosedException;
import uspo.include.tools.EngineFaulthException;
import uspo.include.tools.RecipientNotFoundException;
import uspo.include.io.Data;
import uspo.interfaces.Global;

import java.util.LinkedList;
import java.util.ArrayList;

import uspo.main.Box;
import uspo.main.Vehicle;

/**
 * La classe si occupa di implementare tutti gli aspetti inerenti alla spedizione: 
 * scegliere i percorsi che devono percorrere i veicoli, gestire quindi i veicoli 
 * e i pacchi con controlli specifici tramite vari metodi, gestire le eccezioni 
 * che rappresentano i vari problemi che potrebbero capitare al veicolo in fase 
 * di viaggio
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0906
 * @since 12.0405
 */
public class Shipment implements Global {
	
	private Vehicle the_vehicle;
	private Queue queue;

	Data shipmentHistory;
	Engine dijkstra;
	short paths[][] = new short[20][];

	/**
	 * Shipment(Queue) e' un costruttore che inizializza la variabile di istanza 
	 * queue copiando il valore del riferimento dal parametro
	 * 
	 * @param queue, il riferimento all'oggetto con le code da copiare
	 * 
	 * @version 12.0906
	 * @since 12.0405
	 */
	public Shipment(Queue queue) {
		
		this.queue = queue;
	}
	
	/**
	 * addBox aggiunge un pacco nella coda delle richieste
	 * 
	 * @param the_box, il pacco da aggiungere alla coda delle richieste
	 * 
	 * @version 12.0904
	 * @since 12.0904
	 * */
	public void addBox(Box the_box) {
		queue.getRequest().add(the_box);
	}
	
	/**
	 * Il metodo sequence si occupa di gestire la spedizione, operando su tutte 
	 * le code presenti in Queue tramite i vari metodi della classe shipment, 
	 * in modo da gestire il corretto movimento dei veicoli tramite cammini 
	 * minimi, di conseguenza gestire i pacchi e le code delle richieste, 
	 * gestire le eccezioni che interessano i veicoli e scrivere su file tutte 
	 * le informazioni rilevanti
	 * 
	 * @param priority, la priorita' della spedizione da eseguire
	 * 
	 * @version 12.0907
	 * @since 12.0824
	 */
	public void sequence(short priority) {
		
		boolean deposit = false;
		boolean the_parking = false;
		boolean is_loading = false;
		boolean is_ready = false;
		boolean done = false;
		
		LinkedList<Vertex> the_path;
		
		if(!queue.getBroken().isEmpty()) {
			for(Vehicle the_vehicle : queue.getBroken()) {
				System.out.println("Il veicolo " + the_vehicle + 
						" e' stato riparato");
				if(the_vehicle.getTheLoad().isEmpty())
					switchQueues(the_vehicle, queue.getBroken(), queue.getParking());
				else
					switchQueues(the_vehicle, queue.getBroken(), queue.getLoading());
			}
		}
		
		deposit = requestQueue(queue.getRequest(), queue.getDeposit());
		if ( ! (deposit))
			deposit = ! queue.getDeposit().isEmpty();
		
		for(Vehicle the_vehicle : queue.getLoading())
			if(isFull(the_vehicle)) {
				switchQueues(the_vehicle, queue.getLoading(), queue.getReady());
				is_ready = true;
			}

		if(is_ready) 
			the_vehicle = vehicleSelector(queue.getReady(), priority);
		else {
			
			the_parking = vehicleAvailability(queue.getParking(), priority);
			is_loading = vehicleAvailability(queue.getLoading(), priority);
		
			if(is_loading) {
				the_vehicle = vehicleSelector(queue.getLoading(), priority);
			} else if (the_parking) {
				the_vehicle = vehicleSelector(queue.getParking(), priority);
			}
		}
		
		if(deposit)
			for(Box the_box : queue.getDeposit())
				loadVehicle(the_vehicle, the_box);
		
		if(the_vehicle == null){
			System.out.println("Nessun veicolo disponibile per la spedizione" +
					", ritorni domani");
			return;
			
		} else if((!somethingLeft(the_vehicle, priority)) & 
				queue.getDeposit().isEmpty() & queue.getReady().isEmpty() & 
				queue.getLoading().isEmpty()) {
			System.out.println("Non ci sono pacchi da spedire o da recuperare " +
					", attendere nuove prenotazioni e riprovare");
			return;
			
		} else if(priority == FAST & somethingLeft(the_vehicle, FAST)) {
			
			done = true;
			short source;
			shipmentHistory = new Data("Il veicolo " + the_vehicle + " esegue" +
					" queste operazioni :\n", SHIPMENTS_PATH, WRITE_STRING);
			
			System.out.println("Il veicolo e' in viaggio, attraversa");
			switchQueues(the_vehicle, queue.getReady(), queue.getMoving());
			
			the_path = getMinWeight(the_vehicle, SEDE);
			
			do {
				
				shipmentHistory = new Data("Attraversa :\n",
						 SHIPMENTS_PATH, WRITE_STRING);
				
				for(Vertex vertex : the_path) {
					System.out.print(" - " + vertex);
					shipmentHistory = new Data(" - " + vertex,
							 SHIPMENTS_PATH, WRITE_STRING);
				}
				
				System.out.println("");
				shipmentHistory = new Data("\n",
						 SHIPMENTS_PATH, WRITE_STRING);
				
				source = getRegionsIndex(the_path.getLast().toString());
				
				try {
					exceptionLauncher();
				} catch (EngineFaulthException e) {
					e.doAction(the_vehicle, queue);
				} catch (AllBoxLoosedException e) {
					e.doAction(the_vehicle);
				} catch (RecipientNotFoundException e) {
					e.doAction(the_vehicle);
				}
				
				/* nel caso in cui il veicolo abbia il motore rotto
				 * devo far terminare il viaggio all'istante */
				if(queue.getBroken().indexOf(the_vehicle) != -1) {
					source = SEDE;
					return;
				}
				
				if(deleteBox(the_vehicle,source))
					System.out.println("Pacco consegnato");
				
				the_path = getMinWeight(the_vehicle, source);
				
				if (loadOnce(the_vehicle, source))
					System.out.println("Pacco caricato");
				
			} while(somethingLeft(the_vehicle, FAST) & source != SEDE);
			
			if(source != SEDE){
				the_path = getPath(source, SEDE);
			
				shipmentHistory = new Data("Attraversa :\n",
						 SHIPMENTS_PATH, WRITE_STRING);
				
				for(Vertex vertex : the_path) {
					System.out.print(" - " + vertex);
					shipmentHistory = new Data(" - " + vertex,
							 SHIPMENTS_PATH, WRITE_STRING);
				}
				
				System.out.println("");
				shipmentHistory = new Data("\n",
						 SHIPMENTS_PATH, WRITE_STRING);
			}
			
			shipmentHistory = new Data("Il veicolo " + the_vehicle + " ha" +
					" terminato il viaggio\n", SHIPMENTS_PATH, WRITE_STRING);
			System.out.println("Il viaggio e' terminato!");
			
			switchQueues(the_vehicle, queue.getMoving(), queue.getParking());
		} else {
			
			if(somethingLeft(the_vehicle, SLOW)) {
				
				done = true;
				short source = SEDE;
				shipmentHistory = new Data("Il veicolo " + the_vehicle + " esegue" +
						" queste operazioni :\n", SHIPMENTS_PATH, WRITE_STRING);
				
				System.out.println("Il veicolo e' in viaggio, attraversa :");
				switchQueues(the_vehicle, queue.getReady(), queue.getMoving());
				
				do {
					the_path = getMinWeight(the_vehicle, source);
					shipmentHistory = new Data("Attraversa :\n",
							 SHIPMENTS_PATH, WRITE_STRING);
					
					for(Vertex vertex : the_path) {
						System.out.println(" - " + vertex);
						shipmentHistory = new Data(" - " + vertex,
								 SHIPMENTS_PATH, WRITE_STRING);
					}
					
					System.out.println("");
					shipmentHistory = new Data("\n",
							 SHIPMENTS_PATH, WRITE_STRING);
						
					for(Vertex vertex: the_path) {
						
						source = getRegionsIndex(vertex.toString());
						
						try {
							exceptionLauncher();
						} catch (EngineFaulthException e) {
							e.doAction(the_vehicle, queue);
						} catch (AllBoxLoosedException e) {
							e.doAction(the_vehicle);
						} catch (RecipientNotFoundException e) {
							e.doAction(the_vehicle);
						}
						
						/* nel caso in cui il veicolo abbia il motore rotto
						 * devo far terminare il viaggio all'istante */
						if(queue.getBroken().indexOf(the_vehicle) != -1) {
							source = SEDE;
							return;
						}
						if(deleteBox(the_vehicle, source))
							System.out.println("Pacco/hi consegnato/i");
						if(switchBox(the_vehicle, source))
							System.out.println("Pacco/hi prelevato/i");
					}
				} while(somethingLeft(the_vehicle, SLOW) & source != SEDE);
				
				if(source != SEDE){
					the_path = getPath(source, SEDE);
				
					shipmentHistory = new Data("Attraversa :\n",
							 SHIPMENTS_PATH, WRITE_STRING);
					
					for(Vertex vertex : the_path) {
						System.out.print(" - " + vertex);
						shipmentHistory = new Data(" - " + vertex,
								 SHIPMENTS_PATH, WRITE_STRING);
					}
					
					System.out.println("");
					shipmentHistory = new Data("\n",
							 SHIPMENTS_PATH, WRITE_STRING);
				}
				
				shipmentHistory = new Data("Il veicolo " + the_vehicle + " ha" +
						" terminato il viaggio\n", SHIPMENTS_PATH, WRITE_STRING);
				System.out.println("Il viaggio e' terminato!");
				
				switchQueues(the_vehicle, queue.getMoving(), queue.getParking());
			}
		}
		
		if(!done)
			System.out.println("Impossibile eseguire la spedizione in quanto" +
					" non ci sono pacchi che abbiano la priorita' selezionata");
	}
	
	/**
	 * Data una regione di partenza ed una di arrivo, il metodo ritorna il 
	 * cammino minimo per andare dalla regione di partenza a quella di arrivo
	 * 
	 * @param source, l'indice corrispondente alla regione di partenza
	 * @param dest, l'indice corrispondente alla regione di destinazione
	 * @return il cammino minimo tra source e dest
	 * 
	 * @version 12.0904
	 * @since 12.0825
	 */
	private LinkedList<Vertex> getPath(short source, short dest) {
		
		dijkstra = new Engine(queue.getGraph());
		
		// the path from source to dest
		dijkstra.execute(queue.getNodes().get(source));
		LinkedList<Vertex> path = dijkstra.getPath(queue.getNodes().get(dest));
		
		return path;
	}
	
	/**
	 * Data una regione di partenza ed una di arrivo, il metodo ritorna il 
	 * costo per percorrere tale distanza (bisogna lanciare prima il metodo 
	 * getPath per avere il costo del cammino corretto).
	 * 
	 * @param source, l'indice corrispondente alla regione di partenza
	 * @param dest, l'indice corrispondente alla regione di destinazione
	 * @return il costo del cammino tra source e dest
	 * 
	 * @see Engine#getDistance(Vertex, Vertex)
	 * 
	 * @version 12.0831
	 * @since 12.0831
	 */
	private short getWeightPath(short source, short dest) {
		
		return (short) dijkstra.getDistance(queue.getNodes().get(source), 
				queue.getNodes().get(dest));
	}
	
	/**
	 * getMinWeight cerca il cammino di costo minore tra source in ingresso e 
	 * le sorgenti dei pacchi nella coda delle richieste e le destinazioni dei 
	 * pacchi nel carico del veicolo
	 * 
	 * @param the_vehicle, il veicolo per scegliere i percorsi
	 * @param source, la sorgente per la creazione del cammino
	 * @return il cammino di costo minimo oppure null se non esiste alcun 
	 * cammino 
	 * 
	 * @see Shipment#getPath(short, short)
	 * @see Shipment#getWeight(short, short)
	 * 
	 * @version 12.0906
	 * @since 12.0831
	 */
	private LinkedList<Vertex> getMinWeight(Vehicle the_vehicle, short source) {
		
		LinkedList<Vertex> temp_path = null, the_path = null;
		short temp, min = 30000;
		
		for(Box the_box : the_vehicle.getTheLoad()) {
				
				temp_path = getPath(source, the_box.getDestination());
				if(temp_path != null  & temp_path != the_path) {
					temp = getWeightPath(source, the_box.getDestination());
					if(temp < min){
						min = temp;
						the_path = temp_path;
				}
			}
		}
		
		for(Box the_box : queue.getRequest()) {
			if(priorityCheck(the_vehicle, the_box)) {
				
				temp_path = getPath(source, the_box.getSource());
				if(temp_path != null & temp_path != the_path) {
					temp = getWeightPath(source, the_box.getSource());
					if(temp < min){
						min = temp;
						the_path = temp_path;
					}
				}
				
				temp_path = getPath(source, the_box.getDestination());
				if(temp_path != null  & temp_path != the_path) {
					temp = getWeightPath(source, the_box.getDestination());
					if(temp < min){
						min = temp;
						the_path = temp_path;
					}
				}
			}
		}
		return the_path;
	}
	
	/**
	 * switchBox sposta i pacchi con la stessa priorita' e stessa source di 
	 * quella in ingresso dalla coda delle richieste al veicolo e scrive nello 
	 * storico delle spedizioni
	 * 
	 * @param the_vehicle, il veicolo da caricare
	 * @param source, la source dei pacchi da spostare
	 * @return true se almeno un pacco e' stato spostato, in caso contrario false
	 * 
	 * @version 12.0904
	 * @since 12.0831
	 */
	private boolean switchBox(Vehicle the_vehicle, short source) {
		
		boolean found = false;
		
		for(Box the_box : queue.getRequest()){
			if(the_box.getSource() == source & priorityCheck(the_vehicle, the_box)) {
				the_vehicle.getTheLoad().add(the_box);
				found = true;
				shipmentHistory = new Data(" - Preleva " + the_box + " da " +
						regions[source] + "\n", SHIPMENTS_PATH, WRITE_STRING);
				}
		}
		
		short i = 0;
		Box the_box;
		
		while(i < queue.getRequest().size()){
			the_box = queue.getRequest().get(i);
			if(the_box.getSource() == source & priorityCheck(the_vehicle, the_box)) {
				queue.getRequest().remove(the_box);
				i--;
			}
			i++;
		}
		return found;
	}
	
	/**
	 * loadOnce sposta un pacco con la stessa source di quella in ingresso 
	 * dalla coda delle richieste al veicolo usando loadVehicle e scrive nello 
	 * storico delle spedizioni
	 * 
	 * @param the_vehicle, il veicolo da caricare
	 * @param source, la source dei pacchi da spostare
	 * @return true se il pacco e' stato spostato, in caso contrario false
	 * 
	 * @see Shipment#loadVehicle(Vehicle, Box)
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private boolean loadOnce(Vehicle the_vehicle, short source) {
		
		for(Box the_box : queue.getRequest()) {
			if(the_box.getSource() == source & the_box.getPriority() == FAST) {
				the_vehicle.getTheLoad().add(the_box);
				queue.getRequest().remove(the_box);
				shipmentHistory = new Data(" - Preleva " + the_box + " da " +
						regions[source] + "\n", SHIPMENTS_PATH, WRITE_STRING);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * deleteBox cancella dal carico del veicolo i pacchi con destinazione 
	 * uguale a dest in ingresso (come se il pacco fosse consegnato), controllando 
	 * che la priorita' del pacco e veicolo siano uguali e scrive sullo storico 
	 * delle spedizioni
	 * 
	 * @param the_vehicle, il veicolo da cui eliminare i pacchi
	 * @param dest, la destinazione che serve per rimuovere i pacchi
	 * @return true se e' stato eliminato almeno un pacco, altrimenti false
	 * 
	 * @see Math#rint(double)
	 * 
	 * @version 12.0904
	 * @since 12.0831
	 */
	private boolean deleteBox(Vehicle the_vehicle, short dest) {
		
		boolean found = false;
		short i;
		Box the_box;
		double price;
		
		if(!the_vehicle.getTheLoad().isEmpty()){
			for(i = 0; i < the_vehicle.getTheLoad().size(); i++) {
				the_box = the_vehicle.getTheLoad().get(i);
				price = Math.rint(the_box.getPrice() * 100) / 100;
				if(the_box.getDestination() == dest){
					shipmentHistory = new Data(" - Consegna " + the_box + " in " + 
							regions[dest] + ", il pacco e' stato pagato " + 
							price + "€\n", SHIPMENTS_PATH, WRITE_STRING);
					the_vehicle.getTheLoad().remove(the_box);
					found = true;
					i--;
				}
			}
		}
		return found;
	}
	
	/**
	 * Il metodo exceptionLauncher genera un numero casuale ed in base al numero 
	 * generato invoca le eccezioni
	 * 
	 * @throws EngineFaulthException 
	 * @throws AllBoxLoosedException 
	 * @throws RecipientNotFoundException 
	 * 
	 * @version 12.0904
	 * @since 12.0830
	 */
	private void exceptionLauncher() throws EngineFaulthException, 
			AllBoxLoosedException, RecipientNotFoundException {
		
		int num = (int)(Math.random() * 60 + 1);
		switch (num) {
			case 58: 
				throw new EngineFaulthException(); 
			case 59:
				throw new AllBoxLoosedException();
			case 60:
				throw new RecipientNotFoundException();
		}
	}
	
	/**
	 * requestQueue legge la lista delle richieste effettuate (tutte le 
	 * spedizioni accettate) cercando tutti i pacchi che hanno source == SEDE 
	 * (ovvero trova tutti i pacchi che partono da SEDE), se li trova li mette 
	 * nel deposito pacchi
	 * 
	 * @param the_request_queue, la coda che contiene tutte le richieste
	 * @param the_deposit, la coda che contiene tutti i pacchi nel deposito
	 * @return true se il metodo ha aggiunto almeno un pacco nel deposito, in 
	 * caso contrario ritorna false
	 * 
	 * @version 12.0829
	 * @since 12.0828
	 */
	private boolean requestQueue(ArrayList<Box> the_request_queue, 
			ArrayList<Box> the_deposit) {
		
		boolean found = false;
		
		for(Box the_box : the_request_queue) {
			if(SEDE == the_box.getSource()) {
				the_deposit.add(the_box);
				the_request_queue.remove(the_box);
				found = true;
			}
		}
		return found;
	}
	
	/**
	 * Il metodo vehicleSelector sceglie il veicolo adatto dalla coda in ingresso 
	 * con la priorita' identica a quella in ingresso
	 * 
	 * @param the_list, la coda in cui cercare il veicolo
	 * @param priority, la priorita' da confrontare
	 * @return il riferimento del veicolo adatto oppure null
	 * 
	 * @version 12.0907
	 * @since 12.0830
	 */
	private Vehicle vehicleSelector(ArrayList<Vehicle> the_list, short priority) {
		
		if( ! (the_list.isEmpty())) {
			for (Vehicle vehicle : the_list)
				if(vehicle.getSpeed() == priority)
					return vehicle;
		}
		return null;
	}
	
	/**
	 * Il metodo vehicleAvailability controlla che esista un veicolo nella coda 
	 * in ingresso con la stessa priorita' di quella passata come parametro
	 * 
	 * @param the_list, la coda in cui cercare il veicolo
	 * @param priority, la priorita' da confrontare
	 * @return true se esiste un veicolo con la stessa priorita', altrimenti 
	 * false
	 * 
	 * @version 12.0831
	 * @since 12.0830
	 */
	private boolean vehicleAvailability(ArrayList<Vehicle> the_list, short priority) {
		
		if( ! (the_list.isEmpty())) {
			for (Vehicle vehicle : the_list)
				if(vehicle.getSpeed() == priority)
					return true;
		}
		return false;
	}
	
	/**
	 * Dato in ingresso il nome di una regione, il metodo torna il suo indice
	 * 
	 * @param the_regions, il nome della regione
	 * @return l'indice della regione oppure -1
	 * 
	 * @see java.lang.String#equalsIgnoreCase(String)
	 * 
	 * @version 12.0831
	 * @since 12.0825
	 */
	private short getRegionsIndex(String the_regions) {
		
		for (short i = 0; i < regions.length; i++)
			if(regions[i].compareTo(the_regions) == 0)
				return i;
		return -1;
	}
	
	/**
	 * Il metodo priorityCheck controlla, dato un veicolo ed un pacco in 
	 * ingresso, che il veicolo abbia la stessa priorità del pacco
	 * 
	 * @param the_vehicle, il veicolo da controllare
	 * @param the_box, il pacco da controllare
	 * @return true se il veicolo ed il pacco hanno la stessa priorità , 
	 * altrimenti false.
	 *
	 * @version 12.0829
	 * @since 12.0829
	 */
	private boolean priorityCheck(Vehicle the_vehicle, Box the_box) {
		return the_vehicle.getSpeed() == the_box.getPriority(); 
	}
	
	/**
	 * Il metodo loadVehicle controlla, dato un veicolo ed un pacco in ingresso, 
	 * che il veicolo abbia la stessa priorità del pacco, se si, il pacco viene 
	 * caricato nel veicolo, se il carico del veicolo lo consente, viene 
	 * eliminato dalle code in cui si potrebbe trovare (the_deposit e 
	 * the_request_queue) ed eventualmente si sposta il veicolo dalla coda 
	 * the_parking a is_loading
	 *
	 * @param the_vehicle, il veicolo da caricare
	 * @param the_box, il pacco da caricare nel veicolo
	 * @return true se i controlli effettuati determinano il caricamento del 
	 * pacco nel veicolo, altrimenti false.
	 *
	 * @version 12.0906
	 * @since 12.0829
	 */
	private boolean loadVehicle(Vehicle the_vehicle, Box the_box) {
		
		if(priorityCheck(the_vehicle, the_box)) {
			if(isLoadable(the_vehicle, the_box)) {
				
				the_vehicle.getTheLoad().add(the_box);
				queue.getDeposit().remove(the_box);
				queue.getRequest().remove(the_box);
				
				switchQueues(the_vehicle, queue.getParking(), 
						queue.getLoading());
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Il metodo isLoadable controlla che il pacco passato in ingresso sia 
	 * caricabile nel veicolo passato in ingresso
	 * 
	 * @param the_vehicle, il veicolo da caricare
	 * @param the_box, il pacco da caricare sul veicolo
	 * @return true se è possibile caricare il pacco, in caso contrario false
	 * 
	 * @version 12.0830
	 * @since 12.0830
	 */
	private boolean isLoadable(Vehicle the_vehicle, Box the_box) {
		
		boolean b = the_vehicle.getCurrentLoad() + the_box.getWeight() < 
				the_vehicle.getLoad();
		return b & the_vehicle.getCurrentVolume() + the_box.getVolume() / 10000 
				< the_vehicle.getVolume();
	}
	
	/**
	 * Il metodo switchQueues, dato un veicolo e due code in ingresso, sposta il 
	 * veicolo da una lista all'altra
	 * 
	 * @param the_vehicle, il veicolo da spostare
	 * @param source, la lista da cui togliere il veicolo
	 * @param dest, la lista in cui inserire il veicolo
	 * @return true se lo spostamento viene eseguito senza errori, altrimenti 
	 * false
	 * 
	 * @version 12.0830
	 * @since 12.0829
	 */
	private boolean switchQueues(Vehicle the_vehicle, ArrayList<Vehicle> source,
			ArrayList<Vehicle> dest) {
		
		if(source.remove(the_vehicle)){
			dest.add(the_vehicle);
			return true;
		}
		return false;
	}
	
	/**
	 * Il metodo isFull controlla, dato un veicolo in ingresso, che il veicolo 
	 * sia pieno, se non e' pieno si controlla la priorita':
	 * 
	 * - se la priorita' e' FAST ritorna false
	 * - se la priorita' e' SLOW allora e' necessario controllare nella coda 
	 * delle richieste se il veicolo puo' essere riempito con i pacchi ancora 
	 * da ritirare
	 * 
	 * @param the_vehicle, il veicolo da controllare
	 * @return true se i controlli effettuati indicano che il veicolo è pieno o 
	 * puo' essere riempito (se veicolo e' fast torna true, altrimenti false.
	 *
	 * @version 12.0830
	 * @since 12.0828
	 */
	private boolean isFull(Vehicle the_vehicle) {
		
		double boxes_weight;
		double boxes_volume;
		
		if(the_vehicle.isFull()) {
			return true;
		} else {
			if(the_vehicle.getSpeed() == SLOW ) {
				if( ! (queue.getRequest().isEmpty())) {
					boxes_weight = the_vehicle.getCurrentLoad();
					boxes_volume = the_vehicle.getVolume();
					
					for (Box b : queue.getRequest()) {
						boxes_weight += b.getWeight();
						boxes_volume += b.getVolume();
						
						if(boxes_weight >= the_vehicle.getLoad() - 200 | 
							boxes_volume / 10000 >= the_vehicle.getVolume() - 4)
								return true;
						}
					}
				} else
					return true;
			}
		return false;
	}
	
	/**
	 * Il metodo controlla che siano presenti dei pacchi nella coda delle 
	 * richieste e nel carico del veicolocon la stessa priorita' di quella 
	 * passata come parametro
	 * 
	 * @param the_vehicle, il veicolo a cui controllare il carico
	 * @param priority, la priorita' per eseguire il controllo
	 * @return true se viene trovato almeno un pacco con la prorita' cercata 
	 * altrimenti false
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private boolean somethingLeft(Vehicle the_vehicle, short priority) {
		
		for(Box the_box : the_vehicle.getTheLoad())
			if(the_box.getPriority() == priority)
				return true;
		for(Box the_box : queue.getRequest())
			if(the_box.getPriority() == priority)
				return true;
		return false;
	}
}