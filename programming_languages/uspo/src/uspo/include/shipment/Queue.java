package uspo.include.shipment;

import java.util.ArrayList;

import uspo.include.algorithms.dijkstra.Edge;
import uspo.include.algorithms.dijkstra.Graph;
import uspo.include.algorithms.dijkstra.Vertex;
import uspo.main.Box;
import uspo.main.Vehicle;

/**
 * La classe si occupa di fornire l'implementazione delle code che verranno 
 * utilizzate per tutto il corso del programma, le code si riferiscono ai vari
 * stati dei veicoli (parcheggio, sta caricando, pronto, in movimento, rotto), 
 * ai vari stati dei pacchi (deposito, coda delle richieste), ai nodi e i lati 
 * del grafo per la costruzione dei cammini minimi noche' il grafo stesso.
 * I vari elementi della classe sono per la maggior parte ArrayList
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.util.ArrayList
 * 
 * @version 12.0907
 * @since 12.0825
 */
public final class Queue {
	
	private static ArrayList<Vehicle> the_parking;
	private static ArrayList<Vehicle> is_moving;
	private static ArrayList<Vehicle> is_broken;
	private static ArrayList<Vehicle> is_ready;
	private static ArrayList<Vehicle> is_loading;
	
	private static ArrayList<Box> the_deposit;
	private static ArrayList<Box> the_request_queue;
	
	private static ArrayList<Vertex> nodes;
	private static ArrayList<Edge> edges;
	
	private static Graph graph;
	
	/**
	 * Costruttore di default che inizializza le strutture dati e le prepara
	 * per contenere i dati dei vari oggetti
	 * 
	 * @version 12.0904
	 * @since 12.0825
	 */
	public Queue(){
		
		the_parking = new ArrayList<Vehicle>(0);
		is_moving = new ArrayList<Vehicle>(0);
		is_broken = new ArrayList<Vehicle>(0);
		is_ready = new ArrayList<Vehicle>(0);
		is_loading = new ArrayList<Vehicle>(0);
		
		the_deposit = new ArrayList<Box>(0);
		the_request_queue = new ArrayList<Box>(0);
		
		nodes = new ArrayList<Vertex>(0);
		edges = new ArrayList<Edge>(0);
	}
	
	/** 
	 * @return la coda dei veicoli nel parcheggio (variabile di istanza)
	 */
	public ArrayList<Vehicle> getParking() {
		return the_parking;
	}
	
	/**
	 * @return la coda dei veicoli in movimento (variabile di istanza)
	 */
	public ArrayList<Vehicle> getMoving() {
		return is_moving;
	}
	
	/**
	 * @return la coda dei veicoli rotti (variabile di istanza)
	 */
	public ArrayList<Vehicle> getBroken() {
		return is_broken;
	}
	
	/**
	 * @return la coda dei veicoli pronti (variabile di istanza)
	 */
	public ArrayList<Vehicle> getReady() {
		return is_ready;
	}
	
	/**
	 * @return la coda dei veicoli in carico (variabile di istanza)
	 */
	public ArrayList<Vehicle> getLoading() {
		return is_loading;
	}
	
	/**
	 * @return la coda dei pacchi nel deposito (variabile di istanza)
	 */
	public ArrayList<Box> getDeposit() {
		return the_deposit;
	}
	
	/**
	 * @return la coda delle richieste (variabile di istanza)
	 */
	public ArrayList<Box> getRequest() {
		return the_request_queue;
	}
	
	/**
	 * @return la lista dei nodi (variabile di istanza)
	 */
	public ArrayList<Vertex> getNodes() {
		return nodes;
	}
	
	/**
	 * @return la lista dei lati (variabile di istanza)
	 */
	public ArrayList<Edge> getEdges() {
		 return edges;
	}
	
	/**
	 * @return il grafo
	 */
	public Graph getGraph() {
		return graph;
	}
	
	/**
	 * Il metodo si occupa di creare un nuovo oggetto grafo e ne associa il
	 * riferimento alla variabile di istanza
	 * 
	 * @version 12.0904
	 * @since 12.0829
	 */
	public void setGraph() {
		graph = new Graph(nodes, edges);
	}
}
