package uspo.interfaces;

/**
 * L'interfaccia Global contiene tutte le costanti che verranno utilizzate per 
 * tutto il programma
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0907
 * @since 12.0405
 */
public interface Global {
	
	/*=( Global )
	 **************************************************************************/
	
	final short SEDE = 11; 
	
	String regions[] = {
		"Piemonte",
		"Valle D'Aosta",
		"Lombardia",
		"Trentino",
		"Venento",
		"Friuli-Venezia Giulia",
		"Liguria",
		"Emilia-Romagna",
		"Toscana",
		"Umbria",
		"Marche",
		"Lazio",
		"Abruzzo",
		"Molise",
		"Campania",
		"Puglia",
		"Basilicata",
		"Calabria",
		"Sicilia",
		"Sardegna",
	};
	
	final short PIEMONTE = 0;
	final short V_DAOSTA = 1;
	final short LOMBARDIA = 2;
	final short TRENTINO = 3;
	final short VENETO = 4;
	final short FRIULI = 5;
	final short LIGURIA = 6;
	final short E_ROMAGNA = 7;
	final short TOSCANA = 8;
	final short UMBRIA = 9;
	final short MARCHE = 10;
	final short LAZIO = 11;
	final short ABRUZZO = 12;
	final short MOLISE = 13;
	final short CAMPANIA = 14; 
	final short PUGLIA = 15;
	final short BASILICATA = 16;
	final short CALABRIA = 17;
	final short SICILIA = 18;
	final short SARDEGNA = 19;
	
	enum Type { 
		FastShipping, 
		SlowShipping,
		
		FiatDucato, 
		FiatScudo, 
		FiatFiorino, 
		FiatDoblo,
		
		Company,
		Person,
		
		ShipmentsManagement,
		ReservationManagement,
		PaymentsManagement,
		VehiclesManagement,
		HistoryManagement,
		GoOut,
	}
	
	final short EXIT_SUCCESS = 0;
	final short EXIT_FAILURE = 1;
	
	final short SLOW = 1;
	final short FAST = 2;
	
	final long MAX = 8223372036854775807L;
	final long MIN = 1000000000000000000L;
	
	/*=( Data.java )
	 **************************************************************************/
	
	final short WRITE_OBJECT = 1;
	final short READ_OBJECT = 2;
	final short WRITE_STRING = 3;
	final short READ_STRING = 4;
	final short DELETE_FILE = 5;
	
	final String OBJECT_PATH = "data/object.bin";
	final String BOX_PATH = "data/box.txt";
	final String COMPANY_PATH = "data/user_company.txt";
	final String PERSON_PATH = "data/users_person.txt";
	final String VEHICLE_PATH = "data/vehicles.txt";
	
	final String PAYMENTS_PATH = "data/payments.txt";
	final String SHIPMENTS_PATH = "data/shipmets.txt";
	final String RESERVATION_PATH = "data/reservation.txt";
	final String VEHICLES_MNG_PATH = "data/vehicles_mng.txt";
	final String HISTORY_MNG_PATH = "data/history.txt";
	
	final String LOG_PATH = "data/log.txt";

	/*=( Box.java )
	 **************************************************************************/
	
	final short LETTER = 1;
	final double L_HEIGHT = 10;
	final double L_WIDTH = 15;
	final double L_DEPTH = 1;
	
	final short BOX = 2;
	final double B_HEIGHT = 20;
	final double B_WIDTH = 50;
	final double B_DEPTH = 20;
	
	final short BIG_BOX = 3;
	final double B_B_HEIGHT = 160;
	final double B_B_WIDTH = 140;
	final double B_B_DEPTH = 90;
	
	/*=( Vehicle.java )
	 **************************************************************************/
	final double DUCATO_HEIGHT = 2.10;
	final double DUCATO_WIDTH = 1.87;
	final double DUCATO_DEPTH = 4.07;
	final double DUCATO_LOAD = 3500;
	final short DUCATO_SPEED = FAST;
	final String DUCATO_NAME = "Fiat Ducato";
	
	final double FIORINO_HEIGHT = 1.80;
	final double FIORINO_WIDTH = 1.76;
	final double FIORINO_DEPTH = 3.52;
	final double FIORINO_LOAD = 2700;
	final short FIORINO_SPEED = SLOW;
	final String FIORINO_NAME = "Fiat Fiorino";
	
	final double SCUDO_HEIGHT = 1.50;
	final double SCUDO_WIDTH = 1.69;
	final double SCUDO_DEPTH = 3.29;
	final double SCUDO_LOAD = 2400;
	final short SCUDO_SPEED = SLOW;
	final String SCUDO_NAME = "Fiat Scudo";
}