package uspo.interfaces;

/** 
 * L'interfaccia Lang contiente tutti i messaggi di output inerenti ai menu' 
 * che devono essere mostrati all'utente durante l'uso abituale del software.
 * Io e Simone abbiamo pensato fosse utile avere
 * tutto il testo raggruppato in un singolo file per tre motivi:<p>
 * <ol><li> Non sporcare il codice con il testo.</li><li> Semplicita' e velocita'
 *  nella manutenzione dell'output.</li><li> Possibilita' di tradurre il software
 *  in altre lingue.</li></ol><p>
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see <a href="http://docs.oracle.com/javase/tutorial/i18n/intro/index.html"
 * title="i18n internationalization">http://docs.oracle.com/javase/tutorial/
 * i18n/intro/index.html</a>
 * 
 * @version 12.0904
 * @since 12.0129
 */
public interface Lang {
	
	/*=( Insert Messages )
	 **************************************************************************/
	
	final String main_menu = ("" +
	"*************************************************\n" +
	"*      United State Post Office Management      *\n" +
	"*************************************************\n" +
	" 1. Gestione spedizioni\n" +
	" 2. Gestione prenotazioni\n" +
	" 3. Gestione mezzi\n" +
	" 4. Gestione storici\n" +
	" 0. Esci \n" +
	"*************************************************");
	
	final String shipment_menu = ("" +
	"*************************************************\n" +
	"*              Gestione spedizioni              *\n" +
	"*************************************************\n" +
	" 1. Nuova spedizione\n" +
	" 2. Lancia spedizione\n" +
	" 3. Visualizza storico spedizioni\n" +
	" 4. Cancella storico spedizioni\n" +
	" 0. Indietro \n" +
	"*************************************************");
	
	final String reservation_menu = ("" +
	"*************************************************\n" +
	"*             Gestione prenotazioni             *\n" +
	"*************************************************\n" +
	" 1. Nuova prenotazione\n" +
	" 2. Visualizza prenotazioni\n" +
	" 3. Elimina prenotazione\n" +
	" 4. Cancella tutte le prenotazioni\n" +
	" 0. Indietro \n" +
	"*************************************************");
	
	final String vehicle_menu = ("" +
	"*************************************************\n" +
	"*                Gestione veicoli               *\n" +
	"*************************************************\n" +
	" 1. Nuovo veicolo\n" +
	" 2. Visualizza stato veicoli\n" +
	" 3. Elimina veicolo\n" +
	" 0. Indietro \n" +
	"*************************************************");
	
	final String history_menu = ("" +
	"*************************************************\n" +
	"*              Gestione storici                 *\n" +
	"*************************************************\n" +
	" 1. Visualizza storico pagamenti\n" +
	" 2. Visualizza storico spedizioni\n" +
	" 3. Elimina storico pagamenti\n" +
	" 4. Elimina storico spedizioni\n" +
	" 0. Indietro \n" +
	"*************************************************");
	
	final String newCustomerMenu = ("" +
	"1. Per aggiungere un'Azienda\n" +
	"2. Per aggiungere una Persona");
	
	final String shipmentPriority = ("" +
	"1. Per eseguire spedizione SLOW\n" +
	"2. Per eseguire spedizione FAST");
	
	final String insert_sender = ("" +
	"*************************************************\n" +
	"* INSERIRE I DATI DEL MITTENTE:                 *\n" +
	"*************************************************");
	
	final String insert_receiver = ("" +
	"*************************************************\n" +
	"* INSERIRE I DATI DEL DESTINATARIO:             *\n" +
	"*************************************************");
	
	final String insert_package = ("" +
	"*************************************************\n" +
	"* INSERIRE LE CARATTERISTICHE DEL PACCO:        *\n" +
	"*************************************************");
	
	final String insert_vehicle = ("" +
	"*************************************************\n" +
	"* INSERIRE I DATI DEL VEICOLO:                  *\n" +
	"*************************************************");
	
	final String delete_vehicle_menu = ("" +
	"*************************************************\n" +
	"* Scegli la coda di veicoli                      *\n" +
	"*************************************************\n" +
	" 1. Parcheggio\n" +
	" 2. In carico\n" +
	" 3. Pronto\n" +
	" 4. In viaggio\n" +
	" 5. Rotto\n" +
	"*************************************************");
	
	final String op_complete = ("" +
	"*************************************************\n" +
	"* OPERAZIONE COMPLETATA                         *\n" +
	"*************************************************\n\n");
}
