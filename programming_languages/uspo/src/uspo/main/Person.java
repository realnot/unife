package uspo.main;

import uspo.include.io.*;

/**
 * Lo stesso discorso effettuato per la classe Company vale per questa 
 * classe, solo che qui estendiamo le funzionalita' del comune utente per 
 * andare a definire una persona (non un'azienda in Company). Questa classe 
 * recupera le informazioni dalla superclass 'User', che accomuna tutti gli 
 * utenti e ne estende le funzionalita'.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see Company
 * 
 * @version 12.0906
 * @since 12.0403
 */
public class Person extends User {
	
	/**
	 * La superclass User implementa Serializable
	 * @see java.io.Serializable
	 */
	private static final long serialVersionUID = 3901520116479960056L;
	
	private String person;
	private String taxcode = "1002022992";

	/**
	 * Person() e' il costruttore di default che inizializza i campi 
	 * dell'oggetto richiamando il costruttore della superclasse e il metodo 
	 * setTaxcode()
	 * 
	 * @version 12.0906
	 * @since 12.0403
	 */
	public Person() {
		super();
		setTaxcode();
	}
	
	/**
	 * Person(Person) e' un costruttore che inizializza i campi dell'oggetto 
	 * copiando i valori dal Company passato come parametro
	 * 
	 * @param obj, l'oggetto di tipo Person da cui copiare i dati
	 * 
	 * @version 12.0906
	 * @since 12.0403
	 */
	public Person(Person obj) {
		super(obj);
		taxcode = obj.getTaxcode();
	}
	
	/** Il metodo setTaxcode() ha il compito di recuperare un valore di tipo 
	 * taxcode sullo standard input da tastiera.
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.String#length()
	 * @see java.lang.String#matches(String)
	 * 
	 * @version 12.0906
	 * @since 12.0405
	 */
	private void setTaxcode() {
		
		boolean error;
		
		do {
			System.out.print("Inserire il codice fiscale: ");
			this.taxcode = In.readString();
			
			if(taxcode.matches("^\\w{16}"))
				error = false;
			else {
				System.out.print("" +
					"Il codice fiscale non e' corretto:\n " +
					"- Il codice fiscale deve essere un numero di 16 cifre.\n"); 
				error = true;
			}
		} while(error);
	}
	
	/**
	 * Il metodo concatena le variabili di istanza della superclasse e di 
	 * questa classe per creare una stringa.
	 * 
	 * @retur la stringa contenente tutti i campi dell'oggetto
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0906
	 * @since 12.0405
	 */
	@Override
	public final String toString() {
	
		person = "l privato " + super.getName() + " con codice fiscale " + 
				taxcode + " CAP " + super.getZipcode() + " con sede in " + 
				super.getAddress() + " " + regions[super.getRegion()];
		
		return person;
	}
	
	/** 
	 * @return il metodo ritorna il codice fiscale
	 */
	public final String getTaxcode() { 
		return taxcode; 
	}
}