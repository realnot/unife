package uspo.main;

import java.util.ArrayList;

import uspo.include.algorithms.dijkstra.Edge;
import uspo.include.algorithms.dijkstra.Vertex;
import uspo.include.io.Data;
import uspo.include.io.In;
import uspo.include.shipment.Queue;
import uspo.include.shipment.Shipment;
import uspo.interfaces.Global;
import uspo.interfaces.Lang;

/**
 * La classe Init si occupa di implementare tutti i metodi necessari alla 
 * visualizzazione dei menu' e della gestione delle scelte chiamando i metodi 
 * delle altre classi al momento opportuno. Init inoltre inizializza le variabili 
 * e gli oggetti che verranno utilizzati per tutta l'esecuzione del programma.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0907
 * @since 12.0828
 */
public class Init implements Global, Lang {
	
	private Person the_person;
	private Company the_company;
	private Queue queue;
	private short paths[][] = new short[20][];
	private Shipment the_shipment;
	
	@SuppressWarnings("unused")
	private Data fileOperator;
	
	/**
	 * Init() e' il costruttore di default che si occupa di :
	 *  - chiedere all'utente se intende cancellare i file degli storici.
	 *  - inizializzare l'oggetto con le code
	 *  - inizializzare l'oggetto inerente alle spedizioni
	 *  - inizializzare la coda parcheggio inserendo i veicoli predefiniti
	 *  - inizializzare il grafo per il calcolo dei cammini minimi
	 *  
	 * @version 12.0907
	 * @since 12.0828
	 */
	public Init() {
		
		short chose;
		do {
			System.out.print("Vuoi pulire i file di storico dei pagamenti e " +
				"storico delle spedizioni? (1 = si, 2 = no) ");
			chose = (short) In.readInt();
			
			if(chose < 1 | chose > 2)
				System.out.println("Scelta non corretta");
		} while (chose < 1 | chose > 2);
		
		if(chose == 1) {
			shipmentsDelete();
			paymentsDelete();
		}
		
		this.queue = new Queue();
		this.the_shipment = new Shipment(queue);
		setParking();
		initGraph();
	}
	
	/**
	 * il metodo si occupa di visualizzare a video il menu' e di eseguire 
	 * l'operazione scelta
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	public void shipmentsManagement() {
		
		int chose;
		do {
			
			System.out.println(shipment_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: newShipment(); break;
				case 2: shipmentLauncher(); break;
				case 3: shipmentsRead(); break;
				case 4: shipmentsDelete(); break;
				case 0: break;
				default: System.out.println("Scelta non corretta\n"); break;
			}
			if(chose > 0 & chose < 5)
				System.out.println(op_complete);
			
		} while (chose != 0);
	}
	
	/**
	 * Il metodo si occupa di chiedere all'utente tutti i dati inerenti ad una 
	 * nuova spedizione, eseguendo il costruttore di Box, il metodo
	 * setNewCustomer() e scrivendo nel file dei pagamenti
	 * 
	 * @see Box#Box(short, short)
	 * @see Init#setNewCustomer()
	 * 
	 * @version 12.0906
	 * @since 12.0825
	 */
	private void newShipment() {
		
		String the_string;
		short source, dest, i , chose;
		double price, tot = 0;
		
		System.out.println(insert_sender);
		setNewCustomer();
		
		if(the_company != null) {
			source = the_company.getRegion();
			the_string = the_company.toString() + " ha pagato :\n";
		} else {
			source = the_person.getRegion();
			the_string = "i" + the_person.toString() + " ha pagato :\n";
		}
		
		System.out.println(insert_receiver);
		setNewCustomer();
		
		if(the_company != null)
			dest = the_company.getRegion();
		else
			dest = the_person.getRegion();
		
		do {
			System.out.print("Quanti pacchi si ha intezione di inviare al " +
					"destinatario appena inserito? ");
			chose = (short) In.readInt();
			
			if(chose < 0)
				System.out.println("Scelta non corretta");
		} while (chose < 0);
		
		for(i = 0; i < chose; i++) {
			System.out.println(insert_package); 
			Box the_box = new Box(source, dest);
			the_shipment.addBox(the_box);
			
			price = Math.rint(the_box.getPrice() * 100) / 100;
			tot += price;
		
			the_string += " - " + price + "€ per la spedizione" +
			" di " + the_box.toString() + "\n";
		}
		
		tot = Math.rint(tot * 100) / 100;
		the_string += " - in totale " + tot + "€\n";
		
		if(the_company != null)
			the_string += "al" + the_company.toString() + "\n";
		else
			the_string += "a" + the_person.toString() + "\n";
		
		fileOperator = new Data(the_string,PAYMENTS_PATH,WRITE_STRING);
	}
	
	/**
	 * Il metodo si occupa di lanciare il metodo Shipment#sequence() che si 
	 * occupa di simulare la spedizione
	 * 
	 * @see Shipment#sequence()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void shipmentLauncher() {
		
		short chose;
		
		do {
			
			System.out.println(shipmentPriority);
			chose = (short) In.readInt();
			
			if(chose < 1 | chose > 2)
				System.out.println("Scelta non valida");
			
		} while(chose < 1 | chose > 2);
		the_shipment.sequence(chose);
	}
	
	/**
	 * Il metodo si occupa di andare a leggere da file i dati di tutte le 
	 * spedizioni effettuate
	 * 
	 * @see Data#Data(String, String, short)
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 * */
	private void shipmentsRead() {
		
		System.out.println("Procedo alla lettura dello storico delle " +
				"spedizioni");
		fileOperator = new Data("",SHIPMENTS_PATH,READ_STRING);
		
	}
	
	/**
	 * Il metodo si occupa di cancellare il file dello storico delle spedizioni 
	 * tramite il costruttore di Data
	 * 
	 * @see Data#Data(String, String, short)
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 * */
	private void shipmentsDelete(){
		
		System.out.println("Procedo alla cancellazione dello storico delle " +
				" spedizioni");
		fileOperator = new Data("",SHIPMENTS_PATH,DELETE_FILE);
	}
	
	/**
	 * il metodo si occupa di visualizzare a video il menu' e di eseguire 
	 * l'operazione scelta
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	public void reservationManagement() {
	
		int chose;
		
		do {
			
			System.out.println(reservation_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: newShipment(); break;
				case 2: reservationWatch(); break;
				case 3: reservationDelete(); break;
				case 4: reservationsDelete(); break;
				case 0: break;
				default: System.out.println("Scelta non corretta\n"); break;
			}
			if(chose > 0 & chose < 5)
				System.out.println(op_complete);
			
		} while (chose != 0);
	}
	
	/**
	 * Il metodo si occupa di scorrere la coda delle richieste e visualizzare 
	 * i dati di tutti i pacchi che contiene
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void reservationWatch() {
		
		System.out.println("Procedo alla lettura della coda delle richieste");
		if(queue.getRequest().isEmpty())
			System.out.println("Non ci sono prenotazioni in attesa di essere" +
					" elaborate");
		else {
			
			int i = 0;
			for(Box the_box : queue.getRequest()) {
				System.out.println(i + ". " + the_box.toString() + " da " +
						regions[the_box.getSource()] + " a " + 
						regions[the_box.getDestination()]);
				i++;
			}
		}
	}
	
	/**
	 * il metodo si occupa di visualizzare i dati dei pacchi della coda delle 
	 * richieste, chiedendo in input all'utente l'indice del pacco da eliminare.
	 * 
	 * @see Init#reservationWatch()
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void reservationDelete() {
		
		System.out.println("Scegliere, dalla lista visualizzata, il numero" +
				" della prenotazione da cancellare e inserire tale numero");
		reservationWatch();
		
		if(!queue.getRequest().isEmpty()) {
			
			int chose;
			do {
				
				chose = In.readInt();
				
				if(chose < 0 | chose > queue.getRequest().size())
					System.out.println("Errore, il valore inserito non e' corretto");
				else
					queue.getRequest().remove(chose);
				
			} while(chose < 0 | chose >= queue.getRequest().size());
		}
	}
	
	/**
	 * Il metodo si occupa di svuotare interamente la coda delle richieste.
	 * 
	 * @see ArrayList#clear()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void reservationsDelete() {
		
		System.out.println("Procedo alla cancellazione di tutta la coda delle" +
				" richieste");
		if(queue.getRequest().isEmpty())
			System.out.println("Non ci sono prenotazioni in attesa di essere" +
					" elaborate");
		else
			queue.getRequest().clear();
	}
	
	/**
	 * il metodo si occupa di visualizzare a video il menu' e di eseguire 
	 * l'operazione scelta
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	public void vehicleManagement() {
	
		int chose;
		
		do {
			
			System.out.println(vehicle_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: addVehicle(); break;
				case 2: vehicleWatch(); break;
				case 3: vehicleDelete(); break;
				case 0: break;
				default: System.out.println("Scelta non corretta\n"); break;
			}
			if(chose > 0 & chose < 4)
				System.out.println(op_complete);
			
		} while (chose != 0);
	}
	
	/**
	 * Il metodo si occupa di aggiungere un veicolo nelle code dei veicoli 
	 * chiedendo all'utente tutti i dati
	 * 
	 * @see Vehicle#Vehicle()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void addVehicle() {
		
		System.out.println(insert_vehicle);
		Vehicle the_vehicle = new Vehicle();
		queue.getParking().add(the_vehicle);
	}
	
	/**
	 * il metodo ha il compito di scorrere tutta le code dei veicoli e 
	 * visualizzarne i dati
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	private void vehicleWatch() {
		
		System.out.println("Procedo alla visualizzazione dello stato dei" +
				" veicoli");
		int i;
		
		if(queue.getParking().isEmpty())
			System.out.println("Non ci sono veicoli nel parcheggio");
		else {
			
			i = 0;
			System.out.println("Veicoli nel parcheggio: ");
			for(Vehicle the_vehicle : queue.getParking()) {
				System.out.println(i + ". " + the_vehicle.toString());
				i++;
			}
		}
		
		if(queue.getLoading().isEmpty())
			System.out.println("Non ci sono veicoli che stanno caricando");
		else {
			
			i = 0;
			System.out.println("Veicoli che stanno caricando: ");
			for(Vehicle the_vehicle : queue.getLoading()) {
				System.out.println(i + ". " + the_vehicle.toString() + " che " +
						"contiene " + the_vehicle.getTheLoad().size() + " pacchi");
				i++;
			}
		}
		
		if(queue.getReady().isEmpty())
			System.out.println("Non ci sono veicoli pronti a partire");
		else {
			
			i = 0;
			System.out.println("Veicoli pronti a partire: ");
			for(Vehicle the_vehicle : queue.getReady()) {
				System.out.println(i + ". " + the_vehicle.toString() + " che " +
						"contiene " + the_vehicle.getTheLoad().size() + " pacchi");
				i++;
			}
		}
		
		if(queue.getMoving().isEmpty())
			System.out.println("Non ci sono veicoli in viaggio");
		else {
			
			i = 0;
			System.out.println("Veicoli in viaggio: ");
			for(Vehicle the_vehicle : queue.getReady()) {
				System.out.println(i + ". " + the_vehicle.toString() + " che " +
						"contiene " + the_vehicle.getTheLoad().size() + " pacchi");
				i++;
			}
		}
		
		if(queue.getBroken().isEmpty())
			System.out.println("Non ci sono veicoli rotti");
		else {
			
			i = 0;
			System.out.println("Veicoli rotti: ");
			for(Vehicle the_vehicle : queue.getLoading()) {
				System.out.println(i + ". " + the_vehicle.toString() + " che " +
						"contiene " + the_vehicle.getTheLoad().size() + " pacchi");
				i++;
			}
		}
	}
	
	/**
	 * Il metodo si occupa di visualizzare tutti i veicoli nelle code dei veicoli 
	 * , chiede di inserire il numero di una delle liste mostrate e di inserire 
	 * il veicolo da eliminare
	 * 
	 * @see Init#vehicleWatch()
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 * */
	private void vehicleDelete() {
		
		System.out.println("**ATTENZIONE** cancellando un veicolo verranno " +
				"spostati i pacchi che esso contiene nel deposito\n" +
				"Scegliere, dalla lista visualizzata, il numero" +
				" della coda da cui cancellare il veicolo e inserire tale numero");
		
		vehicleWatch();
		
		int chose;
		ArrayList<Vehicle> the_list = null;
		
		do {
			
			System.out.println(delete_vehicle_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: the_list = queue.getParking(); break;
				case 2: the_list = queue.getLoading(); break;
				case 3: the_list = queue.getReady(); break;
				case 4: the_list = queue.getMoving(); break;
				case 5: the_list = queue.getBroken(); break;
				default: System.out.println("Scelta non corretta\n"); break;
			}
			
		} while (chose < 0 | chose > 5);
		
		int i = 0;
		if(the_list.isEmpty())
			System.out.println("La coda scelta e' vuota, impossibile cancellare");
		else {
			System.out.println("La coda scelta contiene: ");
			for(Vehicle the_vehicle : the_list){
				System.out.println(i + ". " + the_vehicle.toString() + " che " +
						"contiene " + the_vehicle.getTheLoad().size() + " pacchi");
				i++;
			}
			
			do {
				
				System.out.println("Fai la tua scelta ");
				chose = In.readInt();
				
			} while (chose < 0 | chose >= the_list.size());
			
			Box the_box;
			
			Vehicle the_vehicle = the_list.get(chose);
			for(i = 0; i < the_vehicle.getTheLoad().size(); i++){
				the_box = the_vehicle.getTheLoad().get(i);
				queue.getDeposit().add(the_box);
				the_vehicle.getTheLoad().remove(the_box);
				i--;
			}
			the_list.remove(the_vehicle);
		}
	}
	
	/**
	 * il metodo si occupa di visualizzare a video il menu' e di eseguire 
	 * l'operazione scelta
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 */
	public void historyManagement() {
		
		int chose;
		
		do {
			
			System.out.println(history_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: paymentsRead(); break;
				case 2: shipmentsRead(); break;
				case 3: paymentsDelete(); break;
				case 4: shipmentsDelete(); break;
				case 0: break;
				default: System.out.println("Scelta non corretta\n"); break;
			}
			if(chose > 0 & chose < 5)
				System.out.println(op_complete);
			
		} while (chose != 0);
	}
	
	/**
	 * Il metodo si occupa di andare a leggere da file i dati di tutti i 
	 * pagamenti effettuati
	 * 
	 * @see Data#Data(String, String, short)
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 * */
	private void paymentsRead() {
		
		System.out.println("Procedo alla lettura dello storico dei pagamenti");
		fileOperator = new Data("",PAYMENTS_PATH,READ_STRING);
		
	}
	
	/**
	 * Il metodo si occupa di cancellare il file dello storico dei pagamenti 
	 * tramite il costruttore di Data
	 * 
	 * @see Data#Data(String, String, short)
	 * 
	 * @version 12.0906
	 * @since 12.0906
	 * */
	private void paymentsDelete() {
		System.out.println("Procedo all'eliminazione dello storico dei pagamenti");
		fileOperator = new Data("",PAYMENTS_PATH,DELETE_FILE);
	}
	
	/**
	 * Il metodo si occupa di selezionare quale tipo di mittente o destinatario 
	 * creare, chiedendo all'utente tutti i dati
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0906
	 * @since 12.0805
	 */
	private void setNewCustomer() {
		
		int chose;
		the_person = null;
		the_company = null;
		
		do {
			System.out.println(newCustomerMenu);
			chose = In.readInt();
			
			switch(chose) {
				case 1:
					the_company = new Company();
					break;
				case 2:
					the_person = new Person();
					break;
				default:
					System.out.println("Scelta non corretta");
					break;
			}
		} while(chose < 1 | chose > 2);
	}
	
	/**
	 * Il tipo ed il numero dei veicoli deve essere conosciuto a priori, cosi' 
	 * creiamo un parcheggio che contiene tutti i tipi di veicoli, i veicoli 
	 * vengono messi in una struttura ArrayList
	 * 
	 * @exception se il numero dei veicoli e' diverso dalla grandezza della 
	 * coda parcheggio
	 * 
	 * @version 12.0827
	 * @since 12.0827
	 */
	private void setParking() {
		
		short ducato = 3;
		short fiorino = 5;
		short scudo = 2;
		
		System.out.println("Inserisco i veicoli predeterminati nel parcheggio");
		
		for (short i = 0; i < ducato; i++) {
			Vehicle the_vehicle = new Vehicle(DUCATO_WIDTH, DUCATO_HEIGHT, 
					DUCATO_DEPTH, DUCATO_LOAD, DUCATO_NAME, DUCATO_SPEED);
			queue.getParking().add(the_vehicle);
		}
		
		for (short i = 0; i < fiorino; i++) {
			Vehicle the_vehicle = new Vehicle(FIORINO_WIDTH, FIORINO_HEIGHT, 
					FIORINO_DEPTH, FIORINO_LOAD, FIORINO_NAME, FIORINO_SPEED);
			queue.getParking().add(the_vehicle);
		}
		
		for (short i = 0; i < scudo; i++) {
			Vehicle the_vehicle = new Vehicle(SCUDO_WIDTH, SCUDO_HEIGHT, 
					SCUDO_DEPTH, SCUDO_LOAD, SCUDO_NAME, SCUDO_SPEED);
			queue.getParking().add(the_vehicle);
		}
		
		if((ducato + fiorino + scudo) != queue.getParking().size())
			throw new RuntimeException("Parcheggio inconsistente!!!\n");
	}
	
	/** 
	 * Il metodo e' usato per caricare il percorso in "edges" (ArrayList)
	 * 
	 * @param pathId, nome che descrive il percorso
	 * @param source, il nodo di partenza
	 * @param dest, il nodo di fine
	 * @param cost, costo per andare dalla sorgente alla destinazione
	 * 
	 * @version 12.0904
	 * @since 12.0826
	 **/
	private void addPath(String pathId, int source, int dest, int cost) {
		
		Edge path = new Edge(pathId, queue.getNodes().get(source), 
				queue.getNodes().get(dest), cost);
		queue.getEdges().add(path);
	}
	
	/**
	 * Inizializza il grafo con tutti i nodi e tutti i lati, con i costi tra i 
	 * vari nodi
	 * 
	 * @version 12.0831
	 * @since 12.0825
	 */
	private void initGraph() {
		
		// The paths from Piedmont to adjacent regions with relative cost
		paths[PIEMONTE] = new short[] {
			 V_DAOSTA, 119, LOMBARDIA, 149, E_ROMAGNA, 332, LIGURIA, 172
		};
		
		// The paths from Aosta Valley to adjacent regions with relative cost
		paths[V_DAOSTA] = new short[] {
			PIEMONTE, 119,
		};
		
		// The paths from Lombardy to adjacent regions with relative cost
		paths[LOMBARDIA] = new short[] {
			TRENTINO, 225, VENETO, 270, E_ROMAGNA, 216, PIEMONTE, 149
		};
		
		// The paths from Trentino to adjacent regions with relative cost
		paths[TRENTINO] = new short[] {
			VENETO, 215, LOMBARDIA, 225
		};
		
		// The paths from Veneto to adjacent regions with relative cost
		paths[VENETO] = new short[] {
			FRIULI, 158, E_ROMAGNA, 153, LOMBARDIA, 273, TRENTINO, 215
		};
		
		// The paths from Friuli to adjacent regions with relative cost
		paths[FRIULI] = new short[] {
			VENETO, 158
		};
		
		// The paths from Liguria to adjacent regions with relative cost
		paths[LIGURIA] = new short[] {
			PIEMONTE, 145, E_ROMAGNA, 293, TOSCANA, 230
		};
		
		// The paths from Emilia-Romagna to adjacent regions with relative cost
		paths[E_ROMAGNA] = new short[] { 
			MARCHE, 227, TOSCANA, 106, LIGURIA, 293, PIEMONTE, 332, 
			LOMBARDIA, 216, VENETO, 153
		};
		
		// The paths from Tuscany to adjacent regions with relative cost
		paths[TOSCANA] = new short[] { 
			E_ROMAGNA, 106, MARCHE, 336, UMBRIA, 156, LAZIO, 279,
			LIGURIA, 230
		};
		
		// The paths from Marche to adjacent regions with relative cost
		paths[UMBRIA] = new short[] { 
			MARCHE, 136, LAZIO, 173, TOSCANA, 156
		};
		
		// The paths from Umbria to adjacent regions with relative cost
		paths[MARCHE] = new short[] { 
			ABRUZZO, 194, LAZIO, 306, UMBRIA, 136, TOSCANA, 336, 
			E_ROMAGNA, 227
		};
		
		// The paths from Lazio to adjacent regions with relative cost
		paths[LAZIO] = new short[] { 
			ABRUZZO, 121, MOLISE, 233, CAMPANIA, 229, TOSCANA, 279, 
			UMBRIA, 173, MARCHE, 306
		};
		
		// The paths from Abruzzo to adjacent regions with relative cost
		paths[ABRUZZO] = new short[] {
			MOLISE, 192, LAZIO, 121, MARCHE, 194
		};
		
		// The paths from Molise to adjacent regions with relative cost
		paths[MOLISE] = new short[] { 
			PUGLIA, 221, CAMPANIA, 160, LAZIO, 223, ABRUZZO, 192
		};
		
		// The paths from Campania to adjacent regions with relative cost
		paths[CAMPANIA] = new short[] { 
			PUGLIA, 263, BASILICATA, 157, LAZIO, 229, MOLISE, 160
		};
	
		// The paths from Apulia to adjacent regions with relative cost
		paths[PUGLIA] = new short[] {
			BASILICATA, 133, CAMPANIA, 263, MOLISE, 221
		};

		// The paths from Basilicata to adjacent regions with relative cost
		paths[BASILICATA] = new short[] {
			PUGLIA, 133, CALABRIA, 322, CAMPANIA, 157
		};
		
		// The paths from Calabria to adjacent regions with relative cost
		paths[CALABRIA] = new short[] {
			BASILICATA, 322, SICILIA, 709
		};
		
		// The paths from Sicily to adjacent regions with relative cost
		paths[SICILIA] = new short[] { 
				CALABRIA, 709
		};
		
		// The paths from Sardinia to adjacent regions with relative cost
		paths[SARDEGNA] = new short[] { 
			LIGURIA, 863, TOSCANA, 662, LAZIO, 556, CAMPANIA, 791,
			CALABRIA, 1179, SICILIA, 467
		};
		
		// load the Vertex on ArrayList<Vertex>
		for (int i = 0; i < paths.length; i++) {
			Vertex location = new Vertex(regions[i], regions[i]);
			queue.getNodes().add(location);
		}
		
		// load the Edge on ArrayList<Edge> using the addPath method
		for (int i = 0; i < paths.length; i++) {
			for (int j = 0; j < paths[i].length; j++) {
				addPath("Edge_" + i, i, paths[i][j], paths[i][++j]);
			}
		}
		
		queue.setGraph();
	}
}
