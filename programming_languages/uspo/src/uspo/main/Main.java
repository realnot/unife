package uspo.main;

import uspo.include.io.In;
import uspo.interfaces.Global;
import uspo.interfaces.Lang;

/**
 * La classe main si occupa semplicemente di avviare il programma visualizzando 
 * un menu' e gestendolo
 * 
 *  @link <a href="http://code.google.com/p/uspo/" title="USPO Project>
 *  USPO - United State Post Office on http://code.google.com/p/uspo/</a>
 *  
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0403
 * @since 12.0219
 */
public class Main implements Global, Lang{
	
	/**
	 * il metodo main istanzia un oggetto di tipo Init e visualizza il menu' 
	 * principale
	 * 
	 * @version 12.0403
	 * @since 12.0219
	 */
	public static void main(String args[]) {
	
		int chose;
		Init the_init = new Init();
		
		do {
			System.out.println(main_menu);
			chose = In.readInt();
			
			switch(chose) {
				case 1: the_init.shipmentsManagement(); break;
				case 2: the_init.reservationManagement(); break;
				case 3: the_init.vehicleManagement(); break;
				case 4: the_init.historyManagement(); break;
				case 0:
					System.out.println("Uscita...");
					System.exit(EXIT_SUCCESS); 
					break;
			}			
		} while(chose != 0);
	}
}