package uspo.main;

import java.io.Serializable;
import uspo.include.io.In;
import uspo.interfaces.Global;

/**
 * User getta le basi per la creazione di un utente, sfruttando i costruttori 
 * per inizializzare l'oggetto che vogliamo creare a seconda delle esigenze.
 * 
 * Gli utenti che questo software trattera' saranno principalmente di due tipi: 
 * <ul>
 * 		<li>Le aziende (Company)</li>
 * 		<li>Le persone (Person)</li>
 * </ul>
 * 
 * Di conseguenza, all'interno di questo package e' possibile trovare le classi:
 * 
 * <ul>
 * 		<li><em>Company.java</em></li>
 * 		<li><em>Person.java</em></li>
 * </ul>
 * 
 * Questa classe non e' stata designata per essere istanziata direttamente, 
 * ma solo per definire il concetto di 'Utente' e di quali campi  esso e' 
 * composto.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see java.io.Serializable
 * @see Company
 * @see Person
 * 
 * @version 12.0904
 * @since 12.0403
 */
public class User implements Serializable, Global {

	/**
	 * La classe User implementa Serializable
	 * @see java.io.Serializable
	 */
	private static final long serialVersionUID = -3402826742683524295L;
	
	private String name;
	private String address;
	private short region;
	private String zipcode;
	
	/**
	 * User() e' il costruttore di default che inizializza i vari campi 
	 * dell'oggetto richiamando i vari setter
	 * 
	 * @version 12.0831
	 * @since 12.0403
	 */
	public User() {
		setName();
		setAddress();
		setRegion();
		setZipcode();
	}
	
	/**
	 * User(User) e' un costruttore che inizializza i campi dell'oggetto 
	 * copiando il valore dei campi del veicolo in ingresso
	 * 
	 * @param obj, il veicolo da cui copiare i campi
	 * 
	 * @version 12.0831
	 * @since 12.0403
	 */
	public User(User obj) {
		name = obj.getName();
		address = obj.getAddress();
		region = obj.getRegion();
		zipcode = obj.getZipcode();
	}
	
	/**
	 * @return il nome.
	 */
	public String getName() { 
		return name; 
	}
	
	/**
	 * @return l'indirizzo.
	 */
	public String getAddress() { 
		return address; 
	}
	
	/**
	 * @return la citta'.
	 */
	public short getRegion() {
		return region; 
	}
	
	/**
	 * @return il CAP.
	 */
	public String getZipcode() { 
		return zipcode; 
	}
	
	/** 
	 * Il metodo setName() ha il compito di recuperare un valore di tipo nome 
	 * sullo standard input da tastiera e controllare che sia un valore 
	 * accettabile.
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.String#matches(String)
	 * 
	 * @version 12.0831
	 * @since 12.0405
	 */
	public void setName() {
		
		boolean error;
		
		do {
			System.out.print("Inserire il nome: ");
			this.name = In.readString();
			
			if(this.name.matches("^[a-zA-Z ]{2,15}"))
				error = false;
			else {
				System.out.println("" +
					"Il nome inserito non e' corretto:\n" +
					"- Il nome inizia con una lettera (a-z) oppure (A-Z).\n" +
					"- Il nome deve essere lungo da 2 a 15 caratteri.\n"); 
				error = true;
			}
		} while(error);
	}
	
	/** 
	 * Il metodo setAddress() ha il compito di recuperare un valore di tipo
	 * indirizzo sullo standard input da tastiera e controllare che sia un valore 
	 * accettabile.
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.String#matches(String)
	 * 
	 * @version 12.0831
	 * @since 12.0405
	 */
	public void setAddress() {
		
		boolean error;
		
		do {
			System.out.print("Inserire l'indirizzo: ");
			this.address = In.readString();
			
			if(this.address.matches("^[a-zA-Z. ]{2,36} [0-9]{1,4}$"))
				error = false;
			else {
				System.out.println("" +
					"L'indirizzo inserito non e' corretto:\n " +
					"- L'indirizzo inizia con una lettera (a-z) oppure (A-Z).\n " +
					"- Il nome della via deve essere lungo da 2 a 36 caratteri.\n " +
					"- Il numero civico deve essere lungo da 1 a 4 caratteri.\n " +
					"- L'indirizzo deve terminare con il numero civico.\n " +
					"- Sono consentiti gli spazi tra le lettere.\n");
				error = true;
			}
		} while(error);
	}
	
	/** 
	 * Il metodo setRegion() ha il compito di recuperare un valore di tipo regione 
	 * sullo standard input da tastiera e controllare che sia un valore 
	 * accettabile.
	 * 
	 * @see uspo.include.io.In#readInt()
	 * 
	 * @version 12.0831
	 * @since 12.0405
	 */
	public void setRegion() {
		
		int i;
		
		do {
			i = 0;
			System.out.println("Scegliere la regione ");
			for(String the_string : regions)
				System.out.println(i++ + ". " + the_string);
			this.region = (short) In.readInt();
			
			if(region < 0 | region > 19)
				System.out.println("La scelta non e' corretta");
			
		} while(region < 0 | region > 20);
	}
	
	/** 
	 * Il metodo setZipcode() ha il compito di recuperare un valore di tipo 
	 * zipcode sullo standard input da tastiera e controllare che sia un valore 
	 * accettabile.
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.String#matches(String)
	 * 
	 * @version 12.0831
	 * @since 12.0405
	 */
	public void setZipcode() {
		
		boolean error;
		
		do {	
			System.out.print("Inserire il CAP: ");
			this.zipcode = In.readString();
			
			if(this.zipcode.matches("^[0-9]{5}")) 
				error = false;
			else {
				System.out.println("" +
					"Il codice postale non e' corretto:\n - " +
					"Il codice postale deve essere un numero di 5 cifre.\n");
				error = true;
			}
		} while(error);
	}
}