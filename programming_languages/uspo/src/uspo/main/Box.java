package uspo.main;

import uspo.include.io.In;
import uspo.interfaces.Global;

/**
 * Box getta le basi per la creazione di un pacco, sfruttando i costruttori per 
 * inizializzare l'oggetto che si vuole creare a seconda delle esigenze, e la 
 * sua gestione tramite vari metodi.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0904
 * @since 12.0423
 */
public class Box implements Global {

	private double width;
	private double height;
	private double depth;
	private double weight;
	private double volume;
	private double price;
	
	private short type;
	private short dest;
	private short source;
	private short priority;
	
	private double min_weight = 0;
	private double max_weight = 200;
	private double min_dimensions = 5;
	private double max_dimensions = 160;
	private double min_volume = (short) (min_dimensions * min_dimensions * 
			min_dimensions);
	private double max_volume = (short) (max_dimensions * max_dimensions * 
			max_dimensions);
	
	/**
	 * Box() e' il costruttore di default che inizializza i vari campi 
	 * dell'oggetto richiamando i vari setter e getter
	 * 
	 * @version 12.0904
	 * @since 12.0423
	 */
	public Box() {
		setWidth();
		setHeight();
		setDepth();
		setWeight();
		setPriority();
		setVolume(width, height, depth);
		type = checkBoxType(volume);
		setThePrice();
		source = -1;
		dest = -1;
	}
	
	/**
	 * Box(short, short) e' un costruttore che inizializza i vari campi 
	 * dell'oggetto richiamando i vari setter e setta le variabili di istanza 
	 * source e destination copiando il valore dai parametri
	 * 
	 * @param source, la regione di partenza
	 * @param destination, la regione di destinazione
	 * 
	 * @version 12.0904
	 * @since 12.0423
	 */
	public Box(short source, short destination) {
		setWidth();
		setHeight();
		setDepth();
		setWeight();
		setPriority();
		setVolume(width, height, depth);
		type = checkBoxType(volume);
		setThePrice();
		
		this.source = source;
		this.dest = destination;
	}
	
	/**
	 * Box(Box) e' un costruttore che inizializza i campi dell'oggetto copiando 
	 * il valore dei campi del pacco in ingresso
	 * 
	 * @param obj, il veicolo da cui copiare i campi
	 * 
	 * @version 12.0904
	 * @since 12.0423
	 */
	public Box(Box obj) {
		width = obj.width;
		height = obj.height;
		depth = obj.depth;
		weight = obj.weight;
		priority = obj.priority;
		volume = obj.volume;
		price = obj.price;
		type = obj.type;
		dest = obj.dest;
		source = obj.source;
	}	
	
	/** 
	 * @return la larghezza (la variabile di istanza).
	 */
	public double getWidth() { 
		return width; 
	}
	
	/** 
	 * @return l'altezza (la variabile di istanza).
	 */
	public double getHeight() { 
		return height; 
	}
	
	/** 
	 * @return la profondita' (la variabile di istanza).
	 */
	public double getDepth() { 
		return depth; 
	}
	
	/** 
	 * @return il peso (la variabile di istanza).
	 */
	public double getWeight() { 
		return weight; 
	}
	
	/** 
	 * @return la priorita' (la variabile di istanza).
	 */
	public short getPriority() { 
		return priority; 
	}
	
	/** 
	 * @return volume (la variabile di istanza).
	 */
	public double getVolume() { 
		return volume; 
	}

	/** 
	 * @return Il metodo ritorna il tipo di pacco (la variabile di istanza).
	 */
	public short getType() { 
		return type; 
	}
	
	/** 
	 * @return il prezzo (la variabile di istanza).
	 */
	public double getPrice() { 
		return price; 
	}
	
	/** 
	 * @return luogo di partenza del pacco (la variabile di istanza).
	 */
	public short getSource() { 
		return source; 
	}
	
	/** 
	 * @return la destinazione del pacco (la variabile di istanza).
	 */
	public short getDestination() { 
		return dest; 
	}
	
	/**
	 * Il metodo ha il compito di recuperare la larghezza del pacco 
	 * (di tipo double) dallo standard input da tastiera.
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0825
	 * @since 12.0404
	 */
	public void setWidth() {
		
		boolean error;
		
		do {
			System.out.print("Inserire la larghezza del pacco: ");
			this.width = In.readDouble();
			
			if(width < min_dimensions | width > max_dimensions) {
				System.out.println("Larghezza del pacco non corretta");
				error = true;
			} else
				error = false;
		} while(error);
	}
	
	/**
	 * Dato in ingresso la larghezza di un pacco, il metodo controlla se e' 
	 * corretta, ed in caso positivo la larghezza diventa la nuova variabile 
	 * d'istanza (width)
	 * 
	 * @param width, la larghezza data in ingresso.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public void setWidth(double width) {
		
		if(width < min_dimensions | width > max_dimensions) {
			System.out.println("Larghezza del pacco non corretta");
		} else {
			this.width = width;
		}
	}
	
	/**
	 * Il metodo setHeight() ha il computo di recuperare l'altezza del pacco 
	 * (di tipo double) dallo standard input da tastiera.
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0825
	 * @since 12.0404
	 */
	public void setHeight() {
		
		boolean error;
		
		do {
			System.out.print("Inserire l'altezza del pacco: ");
			this.height = In.readDouble();
			
			if(height < min_dimensions | height > max_dimensions) {
				System.out.println("Altezza del pacco non corretta");
				error = true;
			} else
				error = false;
		} while(error);
	}
	
	/**
	 * Data la larghezza del pacco in ingresso, il metodo controlla se la 
	 * larghezza e' correta, in caso positivo, diventera' la nuova variabile 
	 * d'istanza (height).
	 *
	 * @param height, l'altezza data in ingresso.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public void setHeight(double height) {
		
		if(height < min_dimensions | height > max_dimensions) {
			System.out.println("Altezza del pacco non corretta");
		} else {
			this.height = height;
		}
	}
	
	/**
	 * Il metodo ha il compito di recuperare la profondita' della scatola 
	 * (di tipo double) dallo standard input da tastiera.
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0825
	 * @since 12.0404
	 */
	public void setDepth() {
		
		boolean error;
		
		do {
			System.out.print("Inserire la profondita' del pacco: ");
			this.depth = In.readDouble();
			
			if(depth < min_dimensions | depth > max_dimensions) {
				System.out.println("Profondita' non valida");
				error = true;
			} else
				error = false;
		} while (error);
	}
	
	/**
	 * Data la profondita' del pacco in ingresso il metodo controlla che sia 
	 * valida, in caso positivo setta la profondita' come nuova variabile 
	 * d'istanza.
	 * 
	 * @param depth, la possibile nuova profondita'.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public void setDepth(double depth) {
		
		if(depth < min_dimensions | depth > max_dimensions) {
			System.out.println("Profontia' del pacco non corretta");
		} else {
			this.depth = depth;
		}
	}
	
	/**
	 * Il metodo setWeight() ha il compito di recuperare il peso' della 
	 * scatola (di tipo double) dallo standard input da tastiera.
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.2605
	 * @since 12.0404
	 */
	public void setWeight() {
		
		boolean error;
		
		do {
			System.out.print("Inserire il peso del pacco: ");
			this.weight = In.readDouble();
			
			if(weight < min_weight | weight > max_weight) {
				System.out.println("Peso del pacco non valido");
				error = true;
			} else
				error = false;
		} while (error);
	}
	
	
	/**
	 * Dato in ingresso il peso del pacco, il metodo controlla se e' 
	 * valido, in caso positivo il peso diventera' la nuova variabile 
	 * d'istanza weight.
	 * 
	 * @param weight, il possibile peso.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public void setWeight(double weight) {
		
		if(weight < min_weight | weight > max_weight) {
			System.out.println("Peso del pacco non valido");
		} else {
			this.weight = weight;
		}
	}
	
	/**
	 * setPriority setta la velocita' del veicolo dallo standard input e 
	 * controlla che tale valore sia esatto
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0904
	 * @since 12.0827
	 */
	public void setPriority() {
		
		boolean error;
		
		do {
			System.out.print("inserire la velocita' del veicolo: (fast = " + 
					FAST + ") (slow = " + SLOW + ") ");
			this.priority = (short)In.readInt();
			
			if(! (priority == FAST | priority == SLOW)) {
				System.out.println("La velocita' inserita non e' corretta");
				error = true;
			} else
				error = false;
			
		} while(error);
	}
	
	/**
	 * Il metodo riceve la priorita' in ingrsso e controlla se e' valida, 
	 * in caso positivo questa diventera' la nuova priorita'.
	 * 
	 * @param priority, la possibile priorita'.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public void setPriority(short priority) {
		
		if(priority != SLOW & priority != FAST) {
			System.out.println("La priorita' inserita non esiste!");
		} else {
			this.priority = priority;
		}
	}
	
	/**
	 * Dato in ingresso un volume (il volume del pacco), il meotodo controlla 
	 * che tale volume rispetti le dimesioni prestabilite.
	 * 
	 * @param the_volume
	 * @return "true", se il volume e' valido, altrimento "false".
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 */
	public boolean checkVolume(double the_volume) {
		
		if(the_volume < min_volume && the_volume > max_volume)
			return false;
		else
			return true;
	}
	
	/**
	 *  Dati in ingresso rispettivamente la larghezza, l'altezza e la 
	 *  profondita', il metodo controlla che sia accettabile e ne ritorna 
	 *  il valore in caso positivo.
	 *  
	 *  @param with, la larghezza del pacco
	 *  @param height, l'altezza del pacco
	 *  @param depth, la profondita' del pacco
	 *  
	 *  @version 12.0829
	 *  @since 12.0829
	 */
	public void setVolume(double width, double height, double depth) {
		
		double volume = (width * height * depth);
		
		if(checkVolume(volume))
			this.volume = volume;
		else 
			System.out.println("Volume non corretto.");
	}
	
	/** 
	 * Dato il volume, il metodo torna il tipo di pacco.
	 * 
	 * @param volume, il volume del pacco.
	 * @return il tipo di pacco (nel caso sia valido), altrimenti '-1'.
	 * 
	 * @version 12.0904
	 * @since 12.0829
	 */
	public short checkBoxType(double volume) {
		
		if(volume < L_WIDTH * L_HEIGHT * L_DEPTH) 
			return LETTER;
		else if(volume < B_WIDTH * B_HEIGHT * B_DEPTH)
			return BOX;
		else if(volume < B_B_WIDTH * B_B_HEIGHT * B_B_DEPTH)
			return BIG_BOX;
		else 
			return -1;
	}
	
	/**
	 * Il metodo, lanciando i vari setter, calcola il prezzo complessivo del 
	 * pacco (non della spedizione, infatti la spedizione potrebbe avere altri 
	 * costi, com l'assicurazione ecc...) richiamando gli altri setter che 
	 * lavorano con le variabili di istanza dell'oggetto
	 * 
	 * @version 12.0829
	 * @since 12.0816
	 */
	public void setThePrice() {
		
		setWeightPrice();
		setTypePrice();
		setPriorityPrice();
	}
	
	/** 
	 * In base al peso del pacco, il metodo calcola il relativo costo e lo 
	 * assegna alla variabile di istanza.
	 * 
	 * @version 12.0829
	 * @since 12.0815
	 */
	public void setWeightPrice() {
		
		if(weight < 1) price += 3.98;
		else if(weight < 3) price += 4.98;
		else if(weight < 5) price += 6.98;
		else if(weight < 10) price += 7.98;
		else if(weight < 15) price += 8.98;
		else if(weight < 30) price += 11.98;
		else if(weight < 40) price += 12.98;
		else if(weight < 50) price += 14.98;
		else if(weight < 60) price += 20.98; 
		else if(weight < 70) price += 25.98;
		else if(weight < 80) price += 29.98;
		else if(weight < 90) price += 33.98;
		else if(weight < 100) price += 36.98;
		else if(weight < 200) price += 60.98;
		else {
			System.out.println("Peso troppo elevato");
		}
	}
	
	/** 
	 * In base al tipo di pacco, il metodo calcola il relativo costo e lo 
	 * assegna alla variabile di istanza.
	 * 
	 * @version 12.0829
	 * @since 12.0816
	 */
	public void setTypePrice() {
		
		switch(type) {
			case LETTER: 
				price += 0.28; break;
			case BOX: 
				price += 2.08; break;
			case BIG_BOX: 
				price += 3.38; break;
			default: 
				System.out.println("Tipo di pacco inesistente!");
		}
	}
	
	/** 
	 * In base alla priorita' del pacco, il metodo calcola il relativo costo 
	 * e lo assegna alla variabile di istanza.
	 * 
	 * @version 12.0829
	 * @since 12.0829
	 * */
	public void setPriorityPrice() {
		
		switch(priority) {
			case SLOW: 
				price += 0.30; break;
			case FAST: 
				price += 3.00; break;
		}
	}
	
	/**
	 * Il metodo e' un Override del metodo toString() per semplificare la 
	 * scrittura su file dello storico
	 * 
	 * @return una stringa contenente i dati dell'oggetto
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0904
	 * @since 12.0904
	 */
	@Override
	public String toString() {
		
		String priority;
		
		if(this.priority == FAST)
			priority = "Fast";
		else
			priority = "Slow";
		
		switch(checkBoxType(this.volume)) {
			case LETTER:
				return "Lettera con priorita' " + priority;
			case BOX:
				return "Pacco di medie dimensioni con priorita' " + priority;
			case BIG_BOX:
				return "Pacco grande con priorita' " + priority;
			default:
				return "Pacco generico con priorita' " + priority;
		}
	}
}