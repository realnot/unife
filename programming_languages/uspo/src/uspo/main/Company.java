package uspo.main;

import uspo.include.io.In;

/** 
 * Questa classe recupera le informazioni dalla superclasse 'User', 
 * che accomuna tutti gli utenti e ne estende le funzionalita'.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @see Person
 * 
 * @version 12.0906
 * @since 12.0403
 */
public final class Company extends User {

	/**
	 * La superclasse User implementa Serializable
	 * @see java.io.Serializable
	 */
	private static final long serialVersionUID = 2147829025378642937L;
	
	private String company;
	private String vat;
	
	/**
	 * Company() e' il costruttore di default che inizializza i campi 
	 * dell'oggetto richiamando il costruttore della superclasse e il metodo 
	 * setVat()
	 * 
	 * @version 12.0906
	 * @since 12.0403
	 */
	public Company() {
		super();
		setVat();
	}
	
	/**
	 * Company(Company) e' un costruttore che inizializza i campi dell'oggetto 
	 * copiando i valori dal Company passato come parametro
	 * 
	 * @param obj, l'oggetto di tipo Company da cui copiare i dati
	 * 
	 * @version 12.0906
	 * @since 12.0403
	 */
	protected Company(Company obj) {
		super(obj);
		vat = obj.getVat();
	}
	
	/** 
	 * Il metodo setVat() ha il compito di recuperare un valore di tipo vat 
	 * sullo standard input da tastiera.
	 * 
	 * @see uspo.include.io.In#readString()
	 * @see java.lang.String#matches(String) 
	 * 
	 * @version 12.0906
	 * @since 12.0405
	 */
	private final void setVat() {
		
		boolean error = true;
		
		do {
			
			System.out.print("Inserire la partita IVA: ");
			vat = In.readString();
		
			if(vat.matches("^[0-9]{11}"))
				error = false;
			else 
				System.out.println("" +
					"La partita IVA non e' corretta:\n " +
					"- La partita IVA deve essere un numero di 11 cifre.\n");
			
		} while(error);
	}
	
	/**
	 * Il metodo concatena le variabili di istanza della superclasse e di 
	 * questa classe per creare una stringa.
	 * 
	 * @return una stringa che conriene tutti i campi dell'oggetto
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0906
	 * @since 12.0405
	 */
	@Override
	public final String toString() {
	
		company = "l'azienda " + super.getName() + " con partita iva " + vat + 
				" CAP " + super.getZipcode() + " con sede in " + 
				super.getAddress() + " " + regions[super.getRegion()];
		
		return company;
	}

	/**
	 * @return la partita iva
	 */
	public final String getVat() { 
		return vat; 
	}
}
