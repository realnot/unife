package uspo.main;

import java.util.ArrayList;
import uspo.include.io.In;
import uspo.interfaces.Global;

/**
 * Vehicle getta le basi per la creazione di un veicolo, sfruttando i 
 * costruttori per inizializzare l'oggetto che si vuole creare a seconda delle 
 * esigenze, e la sua gestione tramite vari metodi.
 * 
 * @author Mauro Crociara
 * @author Simone Armari
 * 
 * @version 12.0904
 * @since 12.0223
 */
public class Vehicle implements Global {

	private double width;
	private double height;
	private double depth;
	private double volume;
	private double load;
	private short speed;
	private String name = "";
	private ArrayList<Box> the_load;
	
	/**
	 * Vehicle() e' il costruttore di default che inizializza i vari campi 
	 * dell'oggetto richiamando i vari setter
	 * 
	 * @version 12.0904
	 * @since 12.0403
	 */
	public Vehicle() {
		setWidth();
		setHeight();
		setDepth();
		setLoad();
		setName();
		setSpeed();
		setVolume(width, height, depth);
		
		the_load = new ArrayList<Box>(0);
	}
	
	/**
	 * Vehicle(double,double,double,double) e' un costruttore che inizializza i 
	 * campi dell'oggetto copiando il valore dei campi passati come parametro
	 * 
	 * @param la lunghezza in metri
	 * @param l'altezza in metri
	 * @param la profondita' in metri
	 * @param la capacita' in chili
	 * 
	 * @version 12.0904
	 * @since 12.0403
	 */
	public Vehicle(double width, double height, double depth, double load, 
			String name, short speed) {
		setWidth(width);
		setHeight(height);
		setDepth(depth);
		setLoad(load);
		setName(name);
		setSpeed(speed);
		setVolume(width, height, depth);
		
		the_load = new ArrayList<Box>(0);
	}
	
	/**
	 * Vehicle(Vehicle) e' un costruttore che inizializza i campi dell'oggetto 
	 * copiando il valore dei campi del veicolo in ingresso
	 * 
	 * @param obj, il veicolo da cui copiare i campi
	 * 
	 * @version 12.0904
	 * @since 12.0403
	 */
	public Vehicle(Vehicle obj) {
		width = obj.getWidth();
		height = obj.getHeight();
		depth = obj.getDepth();
		load = obj.getLoad();
		name = obj.getName();
		speed = obj.getSpeed();
		volume = obj.getVolume();
		
		the_load = new ArrayList<Box>(0);
	}
	
	/**
	 * getCurrentLoad calcola il peso del carico attuale.
	 * 
	 * @return ritorna il carico attuale del veicolo in chili
	 * 
	 * @version 12.0904
	 * @since 12.08.29
	 */
	public double getCurrentLoad() {

		double boxes_weight = 0;
		
		for(Box box : the_load)
			boxes_weight += box.getWeight();
	
		return boxes_weight;
	}
	
	/**
	 * getCurrentVolume calcola il volume occupato.
	 * 
	 * @return ritorna il volume occupato del veicolo in metri cubici
	 * 
	 * @version 12.0904
	 * @since 12.0828
	 */
	public double getCurrentVolume() {

		double boxes_volume = 0;
		
		for(Box box : the_load)
			boxes_volume += box.getVolume();
	
		return boxes_volume;
	}
	
	/**
	 * @return la lunghezza in metri
	 */
	public double getWidth() { 
		return width; 
	}
	
	/**
	 * @return l'altezza in metri
	 */
	public double getHeight() { 
		return height; 
	}
	
	/**
	 * @return la profondita' in metri
	 */
	public double getDepth() { 
		return depth; 
	}
	
	/**
	 * @return la capacita' in chili
	 */
	public double getLoad() { 
		return load; 
	}
	
	/**
	 * @return il nome
	 */
	public String getName() { 
		return name; 
	}
	
	/**
	 * @return la velocita'
	 */
	public short getSpeed() {
		return speed;
	}
	
	/**
	 * @return il carico (i pacchi attualmente contenuti nel veicolo)
	 */
	public ArrayList<Box> getTheLoad() {
		return the_load;
	}
	
	/**
	 * @return il volume in metri cubi
	 */
	public double getVolume() { 
		return volume; 
	}
	
	/**
	 * setWidth setta la larghezza del veicolo dallo standard input da tastiera 
	 * e controlla che sia esatta
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0904
	 * @since 12.0404
	 */
	public void setWidth() {
		
		boolean error;
		
		do {
			System.out.print("Inserire lunghezza del veicolo (mt) ");
			this.width = In.readDouble();
			
			if(width < 1 | width > 3.5) {
				System.out.println("Lungezza del veicolo non corretta");
				error = true;
			} else
				error = false;
		} while(error);
	}
	
	/** 
	 * @Overloading setWidth() per passare la lunghezza come parametro 
	 * @see Vehicle#setWidth()
	 * */
	public void setWidth(double width) {
		
		if(width < 1 | width > 3.5)
			System.out.println("Lungezza del veicolo non corretta");
		else
			this.width = width;
	}
	
	/**
	 * setHeight setta l'altezza del veicolo dallo standard input da tastiera e 
	 * controlla che sia esatta
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0904
	 * @since 12.0404
	 */
	public void setHeight() {
		
		boolean error;
		
		do {
			System.out.print("Inserire altezza del veicolo (mt) ");
			this.height = In.readDouble();
			
			if(height < 1 | height > 4) {
				System.out.println("Altezza veicolo non corretta");
				error = true;
			} else {
				error = false;
			}
		} while(error);
	}
	
	/**
	 * @Overloading setHeight() per passare l'altezza come parametro
	 * @see Vehicle#setHeight()
	 */
	public void setHeight(double height) {
		
		if(height < 1 | height > 4)
			System.out.println("Altezza veicolo non corretta");
		else
			this.height = height;
	}
	
	/**
	 * setDepth setta la profondita' del veicolo dallo standard input da 
	 * tastiera e controlla che sia esatta
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0904
	 * @since 12.0404
	 */
	public void setDepth() {
		
		boolean error;
		
		do {
			System.out.print("Inserire profondita' del veicolo (mt) ");
			this.depth = In.readDouble();
			
			if(depth < 0.5 | depth > 20.0) {
				System.out.println("Profondita' veicolo non corretta");
				error = true;
			} else 
				error = false;
			
		} while (error);
	}
	
	/** 
	 * @Overloading setDepth() per passare la profondita' come parametro
	 * @see Vehicle#setDepth()
	 */
	public void setDepth(double depth) {
		
		if(depth < 0.5 | depth > 20.0)
			System.out.println("Profondita' veicolo non corretta");
		else 
			this.depth = depth;
	}
	
	/**
	 * setLoad setta la capacita' del veicolo dallo standard input da tastiera e 
	 * controlla che sia esatta
	 * 
	 * @see In#readDouble()
	 * 
	 * @version 12.0904
	 * @since 12.0404
	 */
	public void setLoad() {
		
		boolean error;
		
		do {
			System.out.print("Inserire la capacita' del veicolo (kg) ");
			this.load = In.readDouble();
			
			if(load > 3500) {
				System.out.println("Capacita' di carico eccessiva");
				error = true;
			} else
				error = false;
		} while(error);
	}
	
	/** 
	 * @Overloading setLoad() per passare la capacita' come parametro
	 * @see Vehicle#setLoad()
	 */
	public void setLoad(double load) {
		
		if(load > 3500)
			System.out.println("Capacita' di carico eccessiva");
		else
			this.load = load;
	}
	/**
	 * setName setta il nome del veicolo dallo standard input da tastiera e 
	 * controlla che sia esatto
	 * 
	 * @see In#readString()
	 * 
	 * @version 12.0904
	 * @since 12.0725
	 */
	public void setName() {
		
		boolean error;
		
		do {
			System.out.print("Inserire il nome del veicolo ");
			this.name = In.readString();
			
			if(name.length() > 30) {
				System.out.println("Il nome del veicolo e' troppo lungo");
				error = true;
			} else 
				error = false;
		} while(error);
	}
	
	/**
	 * @Overloading setName() per poter passare il nome come parametro
	 * @see Vehicle#setName()
	 */
	public void setName(String name) {
		
		if(name.length() > 30)
			System.out.println("Il nome del veicolo e' troppo lungo");
		else 
			this.name = name;
	}
	
	/**
	 * setSpeed setta la velocita' del veicolo dallo standard input e 
	 * controlla che tale valore sia esatto
	 * 
	 * @see In#readInt()
	 * 
	 * @version 12.0904
	 * @since 12.0827
	 */
	public void setSpeed() {
		
		boolean error;
		
		do {
			System.out.print("inserire la velocita' del veicolo: (fast = " + 
					FAST + ") (slow = " + SLOW + ") ");
			this.speed = (short)In.readInt();
			
			if(! (speed == FAST | speed == SLOW)) {
				System.out.println("La velocita' inserita non e' corretta");
				error = true;
			} else
				error = false;
			
		} while(error);
	}
	
	/**
	 * @Overloading setSpeed() per poter passare la velocita' come parametro
	 * @see Vehicle#setSpeed()
	 */
	public void setSpeed(short speed) {
		
		if(speed != FAST & speed != SLOW)
			System.out.println("La velocita' inserita non e' corretta");
		else
			this.speed = speed;
	}
	
	/**
	 * setVolume(double,double,double) setta il valore del campo volume come 
	 * risultato della moltiplicazione dei parametri in ingresso e controlla che 
	 * i valori inseriti siano esatti, se si setta la variabile di istanza 
	 * volume come il risultato della moltiplicazione dei tre parametri
	 * 
	 * @param la lunghezza in metri
	 * @param l'altezza in metri
	 * @param la profondita' in metri
	 * 
	 * @version 12.0904
	 * @since 12.0403
	 */
	public void setVolume(double width, double height, double depth) {
		
		if(width < 1 | width > 3.5)
			System.out.println("Lungezza del veicolo non corretta");
		else if(height < 1 | height > 4)
			System.out.println("Altezza veicolo non corretta");
		else if(depth < 0.5 | depth > 20.0)
			System.out.println("Profondita' veicolo non corretta");
		else
			this.volume = (width * height * depth);
	}
	
	/**
	 * isFull controlla se il veicolo ha raggiunto la capacita' e il volume 
	 * massimi di carico.
	 * 
	 * @return true se il veicolo e' pieno, false se puo' ancora contenere dei 
	 * pacchi.
	 * 
	 * @version 12.0831
	 * @since 12.0825
	 */
	public boolean isFull() {
		
		short boxes_weight = 0;
		double boxes_volume = 0;
		
		for(Box box : the_load) {
			boxes_weight += box.getWeight();
			boxes_volume += box.getVolume(); 
			
			if( ! (boxes_weight < load) | ( ! (boxes_volume < volume)))
				return true;
		}
		return false;
	}
	
	/**
	 * Il metodo e' un Override del metodo toString() per semplificare la 
	 * scrittura su file dello storico
	 * 
	 * @see java.lang.Object#toString()
	 * 
	 * @version 12.0904
	 * @since 12.0904
	 */
	@Override
	public String toString() {
		
		String speed;
		if(this.speed == FAST)
			speed = "FAST";
		else
			speed = "SLOW";
		
		return this.name + " " + speed;
	}
}