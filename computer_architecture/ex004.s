# Prologue

.text
.align 2

# Functions

.ent sum        
sum:

    add $v0, $a0, $a1           # $v0 = i + j
    jr $ra                      # return to next istruction after jal

.end sum

.ent sum_rec
sum_rec:

    addi $sp, $sp, -12          # update the stack to get space for 3 elements

    sw $ra, 8($sp)              # push $ra (address of return)
    sw $a1, 4($sp)              # push $a1 (parameter size)
    sw $a0, 0($sp)              # push $a0 (parameter A[0])

    bne $a1, $zero, L1          # if (size != 0) goto label L1

    addi $v0, $zero, 0          # return 0

    addi $sp, $sp, 12           # delete 3 elements from stack, updating the
                                # stack pointer

    jr  $ra                     # return to next istruction of jal

L1:

    addi $a0, $a0, -1           # size = size - 1

    jal sum_rec                 # call sum_rec with (size - 1)
    
    lw $a0, 0($sp)              # pop $a0 (parameter A[0])
    lw $a1, 4($sp)              # pop $a1 (parameter size)
    lw $ra, 8($sp)              # pop $ra (address of return)

    addi $sp, $sp, 12           # delete 3 element from stack, updating the
                                # stack pointer
    
    addi $t1, $a1, -1           # $t1 save the original value of size from
                                # lw $a1 and decrease it of one.

    sll $t1, $t1, 2             # [size -1] * 4 (alignment constraint)

    lw  $t2, 16($a0)            # date[size-1]        

    add $v0, $v0, $t2           # $v0 = $v0 + data[size - 1]

    jr $ra                      # return to next istruction after jal

.end sum_rec

# Main

.globl main
main:
    
    li  $t0, 0                  # i = 0
    li  $t1, 2                  # j = 2
    li  $t2, 4                  # NUM = 4

for:    
        
    bge $t0, $t2, L2            # if i >= NUM goto L2

    addi $a0, $t0, 0            # $a0 = i = 0
    addi $a1, $t1, 0            # $a1 = j = 2

    addi $sp, $sp, -4           # update the stack to get space for 1 element
    sw $ra, 0($sp)              # push $ra (register of return)

    jal sum                     # call sum

    la  $s0, aa                 # get A[0]
    sll $t2, $t0, 2             # i * 4 (alignment constraint)
    lw  $s1, aa($t2)            # load word A[i]

    addi $s1, $v0, 0            # A[i] = $v0

    sw  $s1, aa($t2)            # store word A[i]

    addi $t1, $t1, 1            # j++
    addi $t0, $t0, 1            # i++    

    j   for                     # jump to "for"
                
L2:

    addi $a0, $s0, 0            # $a0 = A[0]
    addi $a1, $t2, 0            # $a1 = size

    jal sum_rec                 # call the function sum_rec

    lw $ra, 0($sp)              # pop $ra (address of return)
    addi $sp, $sp, 4            # delete 1 element from stack with update
                                # lo stack pointer
    
# Epilogue

$FINE:

    j   $ra
    .end main

# Data
    
    .align 2
    aa: .space 16
