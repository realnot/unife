/*
 * File: ex006.c
 * Author: Simone Armari (102721) && Mauro Crociara (105760)
 * Created on: 2011 May 19, 8:39
 * Project: LAB1-N6-#40 (Computer Architecture)
 * Contacts: simone.armari@student.unife.it
 * Contacts: mauro.crociara@student.unife.it
 * GCC Version: gcc (Ubuntu/Linaro 4.5.2-8ubuntu4) 4.5.2
 * Standard in use: C99 - http://www.open-std.org/JTC1/SC22/WG14/www/projects#9899
 */

// Libraries
#include <stdlib.h>
#include <stdio.h>

// Function prototypes
void init_cache(void);
void init_mem(void);
void cache_print(void);
int fetch_mem(unsigned int);
int write_mem(unsigned int, int);
void stopwatch(char);

#define T_MEM 10
#define mask(i) ((1 << i) - 1)
#define MAX_T(a, b) ((a) > (b) ? (a) : (b))

struct {
	unsigned int v;
	unsigned int tag;
	int elem[4];
} cache[4096];

// Globals
int mem[1048576];
unsigned int rhit;
unsigned int rtot;
unsigned int whit;
unsigned int wtot;
unsigned int hit_flag;
unsigned int t_now;
unsigned int t_busy;

int main(void) {

	unsigned short i;

	init_cache();

	init_mem();

    // Generate 30 cases
	for(i = 0; i < 30; i++) {

		// prints the addresses and the return values
		fetch_mem(i * 10); 
		printf("access %08d (r)\n", i*2); 
		stopwatch('r');
		
		write_mem(i * 10 + 8, i* 10 + 3); 
		printf("access %08d (w)\n", i*2+1); 
		stopwatch('w');
	}

    // Prints the cache stats
   	cache_print();

   	double time;

   	time = MAX_T(t_now, t_busy);
   	time /= (wtot + rtot);

    printf("-------------------------------------------------------\n");
    printf("Stats = rhit: %d, rtot: %d, whit: %d, wtot: %d\n"
    	   "Average access time: %2.15f\n", rhit, rtot, whit, wtot,time);
    printf("-------------------------------------------------------\n");

    return;
}

void init_cache(void) {

	unsigned short i, j;

	for(i = 0; i < 4096; i++) {
		cache[i].v = 0;
		cache[i].tag = 0;
		for(j = 0; j < 3; j++) cache[i].elem[j] = 0;
	}

    rhit = 0; rtot = 0; whit = 0; wtot = 0; t_now = 0; t_busy = 0;

	return;
}

void init_mem(void) {

	int i;

	for(i = 0; i < 1048576; i++) mem[i] = 1;

	return;
}

void cache_print(void) {

	unsigned short i, j;
	
	printf("-------------------------------------------------------\n");
	printf("IDX     V     TAG    WORD 0   WORD 1   WORD 2   WORD 3 \n");
	printf("-------------------------------------------------------\n");
	
	for(i = 0; i < 4096; i++) {

		if(cache[i].v) {
			printf("%03hu  |  %d  |  %02hd  | ", i, 1, cache[i].tag);
			for(j = 0; j < 4; j++)
				printf("%08d ", cache[i].elem[j]);

		printf("\n");
		}
	}

	return;
}

int fetch_mem(unsigned int address) {

	unsigned int i = 0, word;

	// get the bit for wsel, idx, tag
	int wsel = (address >>  2) & mask(2); // bits_02_03
	int idx = (address >>  4) & mask(12); // bits_04_15
	int tag = (address >> 16) & mask(6); // bits_16_21

	rtot++;

	// byte addressing for the cache
	address >>= 2;

	word = mem[address];

    // Check if the data is inside the cache	
	if(cache[idx].v && cache[idx].tag == tag) {
		printf("cache hit\n");
		rhit++;
		hit_flag = 1;
		return cache[idx].elem[wsel];
	} else {
		printf("cache miss\n");
		cache[idx].v = 1;
		cache[idx].tag = tag;
		hit_flag = 0;

		// Get the words of the block indicated by address to store into cache
		for(; i < wsel; i++) cache[idx].elem[i] = mem[address + (i-wsel)];
        for(i = wsel; i < 4; i++) cache[idx].elem[i] = mem[address + (i-wsel)];
	}

	return word;
}

int write_mem(unsigned int address, int val) {

	unsigned int i = 0, word = 0;

	// get the bit for wsel, idx, tag
	int wsel = (address >>  2) & mask(2); // bits_02_03
	int idx = (address >>  4) & mask(12); // bits_04_15
	int tag = (address >> 16) & mask(6); // bits_16_21

	wtot++;

	// byte addressing for the cache
	address >>= 2;

    word = mem[address];

	if(cache[idx].v && cache[idx].tag == tag) {
		printf("write on hit\n");
		cache[idx].v = 1;
		cache[idx].tag = tag;
		cache[idx].elem[wsel] = val;
		mem[address] = val;

		whit++;

		hit_flag = 1;

	} else {
		printf("write on miss\n");
		mem[address] = val;

		hit_flag = 0;
	}
	return;
}

void stopwatch(char c) {

    unsigned int busy_flag;

    busy_flag = t_now < t_busy;

    printf("t_now = %d, t_busy = %d, busy_flag = %d |", t_now, t_busy, busy_flag);

    // management of c, busy_flag e hit_flag
    if(c == 'w') {
        if(busy_flag) {
            t_now = t_busy + 1; // da verificare
            t_busy += T_MEM;
        } else {
            t_busy += T_MEM;
            t_now++;
        }
    }
    else if(c == 'r') {
        if(hit_flag) t_now++;
        else {
            if(busy_flag) {
                t_now = T_MEM + t_busy + 1;
                t_busy += T_MEM;
            } else {
                t_busy += T_MEM;
                t_now++;
            }
        }
    }
    else exit(EXIT_FAILURE);

    printf(" next_time = %d, t_busy = %d\n", t_now, t_busy);
}
