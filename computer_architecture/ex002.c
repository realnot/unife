/*
 * File: ex002.c
 * Author: Mauro Crociara (105760)
 * Created on: 2011 April 05, 13:27
 * Project: Computer Architecture
 * Contacts: mauro.crociara@student.unife.it
 * GCC Version: gcc (Ubuntu/Linaro 4.4.4-14ubuntu5) 4.4.5
 * Standard in use: C99 - http://www.open-std.org/JTC1/SC22/WG14/www/projects#9899
 */

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// Function prototypes
int wordAdder(int, int, int *);
void fullAdder(int, int, int, int *);
int mult(int, int);

#define N 10 
#define MAX_RAND 20

int main(void) {
 
    int out[2], check[2] = {0, 0}, X, Y, i, result;

    srand(time(NULL));

    printf("\nSum of %d random numbers\n\n",N);
    for(i = 0; i < N; i++) {
        
        // Generates two random numbers
        X = (rand() % MAX_RAND) - (MAX_RAND/2); 
        Y = (rand() % MAX_RAND) - (MAX_RAND/2);
    
        // Make the sum
        result = wordAdder(X, Y, out);

        // Print the result
        printf("%d + %d = %d\n",X,Y,result);    
    }
        
    printf("\nMultiplication of %d random numbers\n\n",N);
    for(i = 0; i < N; i++) {
        
        // Generates two random numbers
        X = (rand() % MAX_RAND) - (MAX_RAND/2); 
        Y = (rand() % MAX_RAND) - (MAX_RAND/2);
    
        /* Check if the generated numbers are positive or negative, for a 
        change of sign for the return result */
        if((X < 0) && (Y < 0)) { check[0] = 0; check[1] = 0; }        
        else if((X < 0) && (Y > 0)) check[0] = 1;
        else if((X > 0) && (Y < 0)) check[1] = 1;
        else { check[0] = 0; check[1] = 0; }

        // Make the sum
        result = mult(X, Y);

        // Print the result
        if((check[0] && check[1])) 
            printf("%d * %d = %d\n",X,Y,result);
        else if(check[0] || check[1]) { 
            result = ~(result++); 
            printf("%d * %d = %d\n",X,Y,result); 
        } else 
            printf("%d * %d = %d\n",X,Y,result);
    }
    return EXIT_SUCCESS;
}

/* 
 * wordAdder(): This function adds two numbers signed int, saves the result of 
 * the sum in out [0], the "carry out" in out[1], and Cin for the "Carry In".
 * The function uses fullAdder (); to perform the bitwise sum. 
 */
int wordAdder(int X, int Y, int *out) {

    int binValue[32], A, B, i, Cin = 0, decValue = 0;

    for(i = 0; i < 32; i++) {        

        // Conversion of A, B, Cin in binary
        A = !!(X & (1 << i)); B = !!(Y & (1 << i)); Cin &= 1;

        // Make the bitwise sum
        fullAdder(A, B, Cin, out);

        // Save the result
        binValue[i] = out[0];

        /* Take the "Carry Out" from fullAdder and assign it into Cin for the 
        next fullAdder */
        Cin = out[1] & 1;
    }    

    // Conversion from bin to dec
    for(i = 31; i+1 > 0; i--) decValue += binValue[i] << i;

    return decValue;
}

/* fullAdder(): The function makes the bitwise sum */
void fullAdder(int A, int B, int Cin, int *out) {
        
    out[0] = A ^ B ^ Cin;
    out[1] = (((A ^ B) & Cin) | (A & B));

    return;    
}

/* 
 * mult(): This function takes two signed int parameters and takes care 
 * of adding X to 0, Y times using the function wordAdder();
 */
int mult(int X, int Y) {

    int out[2], decValue = 0, i;

    for(i = 0; i < abs(Y); i++) decValue += wordAdder(abs(X),0,out);
    
    return decValue;
}
