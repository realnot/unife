/*
 * File: es001.c
 * Author: Mauro Crociara (105760)
 * Created on: 2011 February 02, 01:53
 * Project: Computer Architecture
 * Contacts: mauro.crociara@student.unife.it
 * Contacts: noemi.trasforini@student.unife.it
 * Standard in use: C99 - http://www.open-std.org/JTC1/SC22/WG14/www/projects#9899
 */

// Libraries 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Function prototypes
void dec2bin( unsigned int, short int*); 
void bin2dec( unsigned int *, short int *);
void dec2oct( unsigned int, short int *);
void oct2dec( unsigned int *, short int *);
unsigned int calcPow(long int, long int);

// Number of conversions
#define N 5 

int main(void) {
        
    unsigned int data, i, j;
    short bitSeq[32];
    short octSeq[11];

    // Pseudo-random numbers generator
    srand(time(NULL));

    printf("\n******************************\n"
            "Conversion of %d numbers from:\n
            "Dec to Bin\nBin to Dec\n"
            "Dec to Oct\nOct to Dec\n"
            "******************************\n\n",N);    

    for(i = 0; i < N; i++) { 
        data = rand() % 65535;
        
        printf("Conversion number [ #%u ]:\n\n",i+1);
        dec2bin(data, bitSeq);

        printf("(%d) Base 10 = (",data);
        for(j = 31; j+1 > 0; j-- ) printf("%hd",bitSeq[j]);
        printf(") Base 2\n");

        bin2dec(&data, bitSeq);
        
        printf("(");
        for(j = 31; j+1 > 0; j-- ) printf("%hd",bitSeq[j]);
        printf(") Base 2 = (%d) Base 10\n",data);

        dec2oct(data, octSeq);
        
        printf("(%d) Base 10 = (",data);
        for(j = 10; j+1 > 0; j-- ) printf("%hd",octSeq[j]);
        printf(") Base 8\n");

        bin2dec(&data, bitSeq);
        
        printf("(");
        for(j = 10; j+1 > 0; j-- ) printf("%hd",octSeq[j]);
        printf(") Base 8 = (%d) Base 10\n\n",data);
    }
}

/* 
 * dec2bin(): This function takes two input parameters, the first by value, 
 * the second one by address, with the aim of converting a decimal number into 
 * a binary number.
 */
void dec2bin( unsigned int data, short int *bitSeq) {

    unsigned short i = 0;

    for(; data > 0; i++ ) {
        bitSeq[i] = ( data % 2 );
        data = (int)(data / 2);
    }
    
    for(; i < 32; i++ ) bitSeq[i] = 0;
}

/*
 * bin2dec(): This function converts a binary value to decimal, accepts as 
 * input two values ​​passed by address.
 */
void bin2dec( unsigned int *data, short int *bitSeq) {

    unsigned int i;

    for(i = 31; i > 0; i--) data += calcPow(2,i) * bitSeq[i];
}

/* 
 * dec2oct(): This function accepts as input two integer parameters, returns 
 * the conversion from decimal number to octal number 
 */
void dec2oct( unsigned int data, short int *octSeq) {
    
    unsigned short i = 0;
    
    for(; data > 0; i++ ) {
        octSeq[i] = ( data % 8 );
        data = (int)(data / 8);
    }
    
    for(; i < 11; i++ ) octSeq[i] = 0;
}

/* 
 * oct2dec(): This function accepts two input value passed by address, and 
 * converts an octal value into a decimal value
 */
void oct2dec( unsigned int *data, short int *octSeq) {
    
    unsigned int i;

    for(i = 10; i > 0; i--) data += calcPow(8,i) * octSeq[i];
}

/* 
 * calcPow(): This function takes as parameters the current two unsigned 
 * integers, b = base, e = exponent, so the multiplication is performed (b * b) 
 * and ^ (exponentiation).
 */
unsigned int calcPow(long int b, long int e) {

    unsigned int i = 0,temp = 1;

    for(; i < e; i++) temp *= b;
    
    return temp; 
}
