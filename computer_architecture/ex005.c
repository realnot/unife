/*
 * File: ex005.c
 * Author: Simone Armari (102721) && Mauro Crociara (105760)
 * Created on: 2011 May 13, 14:27
 * Project: LAB1-N5-#40 (Computer Architecture, cache simulator)
 * Contacts: simone.armari@student.unife.it
 * Contacts: mauro.crociara@student.unife.it
 * GCC Version: gcc (Ubuntu/Linaro 4.5.2-8ubuntu4) 4.5.2
 * Standard in use: C99 - http://www.open-std.org/JTC1/SC22/WG14/www/projects#9899
 */

// Libraries
#include <stdlib.h>
#include <stdio.h>

// Function prototypes
void init_cache(void);
void init_mem(void);
void cache_print(void);
int fetch_mem(unsigned int);
int write_mem(unsigned int, int);

struct {
	unsigned int v;
	unsigned int tag;
	int elem[4];
} cache[4096];

#define mask(i) ((1 << i) - 1)

// Globals
int mem[1048576];
unsigned int rhit;
unsigned int rtot;
unsigned int whit;
unsigned int wtot;

void main(void) {
	
	unsigned short i;

	init_cache();
	init_mem();
	
	// Generate 30 cases
	for(i = 0; i < 5; i++) {
	
		// prints the addresses and the return values
		printf("fetch_mem(000%hu00)\n", i); 
		printf("val = %hu\n", fetch_mem(i * 100));
		printf("fetch_mem(000%hu04)\n", i); 
		printf("val = %hu\n", fetch_mem(i * 100 + 4));
		printf("fetch_mem(000%hu08)\n", i); 
		printf("val = %hu\n", fetch_mem(i * 100 + 8));
		
		printf("write_mem(000%hu08,%hu3)\n", i, i); 
		write_mem(i * 100 + 8, i* 10 + 3);
		printf("write_mem(0400%hu04,%hu3)\n", i, i); 
		write_mem(i * 100 + 400004, i* 10 + 3);
		printf("write_mem(0400%hu04,%hu3)\n", i, i); 
		write_mem(i * 100 + 400004, i * 10 + 3);
	}
    
    // Prints the cache stats    
   	cache_print();
    
    printf("-------------------------------------------------------\n");
    printf("Stats = rhit: %d, rtot: %d, whit: %d, wtot: %d\n", 
        rhit, rtot, whit, wtot);
    printf("-------------------------------------------------------\n");
    
    return;
}

void init_cache(void) {

	unsigned short i, j;

	for(i = 0; i < 4096; i++) {
		cache[i].v = 0;
		cache[i].tag = 0;
		for(j = 0; j < 3; j++) cache[i].elem[j] = 0;
	}

	rhit = 0;	rtot = 0;	whit = 0;	wtot = 0;
	
	return;
}

void init_mem(void) {

	int i;

	for(i = 0; i < 1048576; i++) mem[i] = 1;

	return;
}

void cache_print(void) {

	unsigned short i, j;
	
	printf("-------------------------------------------------------\n");
	printf("IDX     V     TAG    WORD 0   WORD 1   WORD 2   WORD 3\n");
	printf("-------------------------------------------------------\n");
	for(i = 0; i < 4096; i++) {
	
		if(cache[i].v) {
			printf("%03hu  |  %d  |  %02hd  | ", i, 1, cache[i].tag);
			for(j = 0; j < 4; j++)
				printf("%08d ", cache[i].elem[j]);
				
		    printf("\n");
		}
	}
	return;
}

int fetch_mem(unsigned int address) {

	unsigned int i = 0, word;
	
	// get the bit for wsel, idx, tag
	int wsel = (address >>  2) & mask(2); // bits_02_03
	int idx = (address >>  4) & mask(12); // bits_04_15
	int tag = (address >> 16) & mask(6); // bits_16_21
	 
	rtot++;

	// byte addressing for the cache
	address >>= 2;

	word = mem[address];

	// Checks if the data is inside the cache
	if(cache[idx].v && cache[idx].tag == tag) {
		printf("cache hit\n");
		
		rhit++;
		
		return cache[idx].elem[wsel];

	} else {
		printf("cache miss\n");
		cache[idx].v = 1;
		cache[idx].tag = tag;
		
		// Get the words of the block indicated by address to store into cache
		for(; i < wsel; i++) cache[idx].elem[i] = mem[address + (i-wsel)];
        for(i = wsel; i < 4; i++) cache[idx].elem[i] = mem[address + (i-wsel)];
	}
	return word;
}

int write_mem(unsigned int address, int val) {

	unsigned int i = 0, word = 0;
	
	// get the bit for wsel, idx, tag
	int wsel = (address >>  2) & mask(2); // bits_02_03
	int idx = (address >>  4) & mask(12); // bits_04_15
	int tag = (address >> 16) & mask(6); // bits_16_21
	
	wtot++;
	
	// byte addressing for the cache
	address >>= 2;
    
    word = mem[address];
	
	if(cache[idx].v && cache[idx].tag == tag) {
		printf("write on hit\n");
		cache[idx].v = 1;
		cache[idx].tag = tag;
		cache[idx].elem[wsel] = val;
		mem[address] = val;
		
		whit++;
		
	} else {
		printf("write on miss\n");
		mem[address] = val;	
	}
	return;
}
