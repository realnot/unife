/*
 * File: ex007.c
 * Author: Simone Armari (102721) && Mauro Crociara (105760)
 * Created on: 2011 May 23, 16:12
 * Project: LAB1-N7-#40 (Computer Architecture)
 * Contacts: simone.armari@student.unife.it
 * Contacts: mauro.crociara@student.unife.it
 * GCC Version: gcc (Ubuntu/Linaro 4.5.2-8ubuntu4) 4.5.2
 * Standard in use: C99 - http://www.open-std.org/JTC1/SC22/WG14/www/projects#9899
 */

// Libraries
#include <stdio.h>
#include <stdlib.h>

// Function prototypes
void fp_print(float);
void fp_mul(float, float, float *);

#define mask(i) ((1 << i) -1)

// Globals
unsigned int Ma, Mb, Mc, Sa, Sb, Sc, Ea, Eb, Ec;
unsigned long long int temp;
unsigned int h;

int main(void) {

    float a, b, c;

    a = 6.0f; b = 5.0f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c); printf("\n");

    a = 1.0f; b = 1.1754942106924e-38f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");
    
    a = 1.0f; b = 2.0f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");

    a = 1.76f; b = 1.9675e-12f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");
    
    a = 2.14f; b = 6.1024f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");

    a = 19.00001f; b = 1.1324386924e-31f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");
    
    a = 2.0f; b = 9.0f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");

    a = 1.0f; b = 3.14159;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");
    
    a = 1.555566621f; b = 11.11f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");

    a = 2.28368731e-4f; b = 1.1210333224e-2f;
    fp_mul(a, b, &c);
    printf("fp_mul(%e, %e, &c) = %e\n", a, b, c);
    fp_print(a * b) ; fp_print(c);  printf("\n");
    
}

void fp_print(float a) {

    unsigned int h, y;
    int i;

    Ma = 0; Sa = 0; Ea = 0;

    // Get the bits of a
    y = * ((unsigned int *) &a);

    // Get the bits from Sa, Ea, Ma 
    Ma = (y >> 0) & mask(23); // bits_00_22
    Ea = (y >> 23) & mask(8); // bits_23_30
    Sa = (y >> 31) & mask(1); // bit_31

    printf("%+2.13e -> 0x%X\n", a, y);

    printf("S = %d  E = ", Sa);

    for(i = 7; i >= 0; i--) printf("%d", ((Ea & (1 << i)) >> i));

    h = Ea != 0;

    printf("  (%03d)M=[%d.]", Ea, h);

    for(i = 22; i >= 0; i--) printf("%d", ((Ma & (1 << i)) >> i));

    printf("\n");
}

void fp_mul(float a, float b, float * c) {

    unsigned int x,y, result = 0;
    int i;

    /* Reset globals */
    Ma = 0; Mb = 0; Mc = 0;
    Sa = 0; Sb = 0; Sc = 0;
    Ea = 0; Eb = 0; Ec = 0;

    // Get the bits of a and b
    x = *((unsigned int *) &a);
    y = *((unsigned int *) &b);

    // Get the bits of Sa, Ea, Ma
    Ma = (x >> 0) & mask(23); // bits_00_22
    Ea = (x >> 23) & mask(8); // bits_23_30
    Sa = (x >> 31) & mask(1); // bits_31_32

    // Get the bits of Sb, Eb, Mb
    Mb = (y >> 0) & mask(23); // bits_00_22
    Eb = (y >> 23) & mask(8); // bits_23_30
    Sb = (y >> 31) & mask(1); // bit_31

    if(Ea == 255 || Eb == 255) {  *c = 0.0; return; }

    // Reintegrate the mantissas
    if(Ea != 0) Ma = Ma | (1 << 23);
    if(Eb != 0) Mb = Mb | (1 << 23);

    // Calculates the sign of the result
    Sc = Sa ^ Sb;

    // Calculate the temp value of Ec
    Ec = (Ea + Eb) - 127;

    // Multiplication of the mantissas
    temp = (unsigned long long int)Ma * (unsigned long long int)Mb;

    // Normalize Mc and edit Ec
    Mc = (temp >> 24) & mask(24); // bits_24_47

    h = (Mc >> 23) & mask(1); // bit_23

    if(h) {
        Mc = Mc & ( ~(1 << 23));
        Ec++;

    } else {
        Mc = Mc << 1;
        Mc = Mc & ( ~(1 << 23));
    }

    /* Compose the bit string fp in the positions indicated by the standard, 
    assign the value to c and exit */
    
    result = result | Mc;

    result = result | (Ec << 23);

    result = result | (Sc << 31);

    *c = *((float *) &result);
}

