# University Repository

##  Who I am

Hello, I'm Mauro, a student of computer science at university of Ferrara (Italy). This is my university repository where you may find some useful stuff that I made during university courses. Some of this are still "work in progress", others, as the databases, are older.

I'm also a Gentoo Linux enthusiast, may you find other useful stuff here:

<https://bitbucket.org/realnot/dotfiles>

## Clone

You can clone the repository wherever you want

```bash
git clone https://realnot@bitbucket.org/realnot/unife.git && cd unife
```
## Feedback

Please, feel free to report any bugs, comments, opinions, and so on here:

<https://bitbucket.org/realnot/unife/issues>

## License

You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

All sources here, are distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

Please, see <http://www.gnu.org/licenses/>.
