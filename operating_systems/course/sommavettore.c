#include <stdlib.h>
#include <unistd.h>     /* fork, getpid, getppid */
#include <stdio.h>
#include <sys/wait.h>   /* wait */

#define NUMFIGLI 4
#define DIMVETT  100

///////////////////////////////////////////////////////////////////////
// Questo programma genera una gerarchia di 5 processi, un padre e 
// quattro figli. Il padre passa a ciascuno dei figli una porzione 
// di un vettore di interi. Ogni figlio fa la somma parziale della propria 
// porzione e ritorna al padre il risultato attraverso una pipe. Il padre 
// somma i risultati parziali calcolati dai figli e stampa la somma totale.
///////////////////////////////////////////////////////////////////////

int main ( int argc, char *argv[] ) {
 
  int i, status;
  
  pid_t childpid, pid;
  
  int pipefd[2];
  
  int ris, sommaparz=0, somma=0;
  
  int vett[DIMVETT];
  int dimproc=DIMVETT/NUMFIGLI;
  int inizio, fine;

  for ( i=0; i < DIMVETT; i++ )
    vett[i] = i+1;
  
  if ( pipe(pipefd) == -1 ) {
    perror ("ERRORE nella creazione della pipe");
    return(-1);
  }
  
  childpid = 1; // valore fittizio
  
  for ( i = 0; i < NUMFIGLI; i++ ) {
    if ( childpid > 0 ) {
      inizio   = i*dimproc;       // che succede se lo metto fuori dall'if ???
      fine     = (i+1)*dimproc-1; // che succede se lo metto fuori dall'if ???
      childpid = fork();
      if ( childpid < 0 )
	      perror("[parent   ]: fork FAILED");
    }
  }
    

  if ( childpid == 0 ) {    
    // this is the child code
    
    printf("[child:%u]: i: %d, inizio: %d, fine: %d, ppid: %d\n", 
	  
    getpid(), i, inizio, fine, getppid());
    
    close (pipefd[0]);                     // close the reading side of pipe 
    
    for ( i = inizio; i <= fine; i++)
      sommaparz+=vett[i];
    
    printf("[child:%u]: somma parziale: %d\n", getpid(), sommaparz); 
	
    write (pipefd[1], &sommaparz, sizeof(int));
    
  } else if ( childpid > 0 ) {   
    // this is the parent code
    
    for ( i=0; i<NUMFIGLI; i++ ) {
      
      read (pipefd[0], &ris, sizeof(int)); 
      
      somma += ris; 
      
      pid = wait (&status);
      
      if ( pid == -1 ) 
	      perror("ERROR: wait failed");
      else  
      	printf ("[parent    ]: Il figlio %d ha terminato l'esecuzione\n", pid );
      
    }
    
    printf ("[parent    ]: la somma totale e' %d\n", somma);
    
  } else {
    
    perror("[parent    ]: ERROR: fork failed");
      
  }
        
  close (pipefd[0]);
  close (pipefd[1]);
  
  exit(0);

}

