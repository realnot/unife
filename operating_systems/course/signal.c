#include <signal.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>     /* fork, getpid, getppid */

void print_date() {
  time_t t = time(NULL);
  char* now = ctime(&t);
  printf("ORA: %s", now);
}

void my_handler(int sig) {
  switch(sig) {
    case SIGINT:
      printf("you typed CTRL-C \n");
      break;
    case SIGTSTP:
      printf("you typed CTRL-Z\n");
      break;
    case SIGALRM:
      print_date();
      // arranges for SIGALRM signal to be delivered in 5 seconds.
      alarm(5);
      break;
    }
}

int main() {
  
  printf("I am  pid %u\n", getpid());
  
  if ( signal(SIGALRM, my_handler) == SIG_ERR ) 
    perror("signal failed\n");
  
  if ( signal(SIGINT,  my_handler) == SIG_ERR )
    perror("signal failed\n");
    
  if ( signal(SIGTSTP, my_handler) == SIG_ERR )
    perror("signal failed\n");
  
  print_date();
  
  // arranges for SIGALRM signal to be delivered in 2 seconds.
  alarm(2);
  
  while ( 1 )
    pause();
  
  return 0;
  
}
