////////////////////////////////////////////////////////////////////////
//
// In un sistema operativo in cui i processi cooperano 
// mediante semafori, la risorsa R e` condivisa tra due classi 
// A e B di processi, e consiste di un insieme di 
// K dispositivi tutti uguali, (es. stampanti o CDROM o nastri) 
// numerati con indici 0 \ldots K-1, su cui i processi possono operare 
// per tempi arbitrariamente lunghi ma finiti. 
//
// Lo stato dei dispositivi e` definito da un array booleano D, in cui 
// l'elemento D[i] indica se il dispositivo di indice i e` libero,  
// ovvero occupato da qualche processo. Ogni processo che ha accesso 
// alla risorsa R ricerca nell'array D un dispositivo libero, lo utilizza, 
// e poi lo rilascia.
//
// La politica di accesso alla risorsa R e` descritta dalle seguenti regole:
//
//    - processi della medesima classe possono essere ammessi all'uso della 
//      risorsa in concorrenza sino ad un massimo di K unita` consecutive; 
//      i successivi devono essere sospesi;
//      
//    - la risorsa ha uno stato rappresentato dalla variabile condivisa statoR  
//      che puo` assumere i valori CLASSEA e CLASSEB. Quando 
//      statoR==CLASSEA possono accedere solo i processi di classe A; 
//      analogamente, quando la variabile statoR==CLASSEB possono accedere 
//      solo i processi di classe B;
//      
//    - un processo di una classe puo` accedere la risorsa se e` il primo 
//      processo a richiedere l'accesso, o se la risorsa e` utilizzata 
//      da processi della medesima classe (sino ad un massino di K);
//      
//    - quando l'ultimo processo di una classe rilascia la risorsa, 
//      sveglia in prima priorita` eventuali processi dell'altra classe, 
//    	altrimenti eventuali processi della medesima classe.      
//
// Scrivere la procedura che deve essere eseguita dai processi di classe A 
// (o di classe B) per accedere alla risorsa R, e gestire le variabili 
// condivise, specificando tutte le dichiarazioni di variabili utilizzate 
// e la loro inizializzazione.
//
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>  /* Semaphore */

#define N 16
#define K 10

#define CLASSEA 0
#define CLASSEB 1

#define FREE 0
#define BUSY 1

int D[K];         // stato dei dispositivi 

sem_t mux;        // mux per le variabili condivise
sem_t muxD;       // mux per l'accesso all'array D
sem_t classeAsem; // semaforo di attesa per i processi di classe A
sem_t classeBsem; // semaforo di attesa per i processi di classe B

int nProcInR            = 0;       // numero di processi che utilizzano R
int statoR              = CLASSEA; // stato della risorsa

int nProcClasseAwaiting = 0;       // numero di processi di classe A in attesa
int nProcClasseBwaiting = 0;       // numero di processi di classe B in attesa

int nProcClasseAammessi  = 0;      // numero di processi di classe A ammessi in R
int nProcClasseBammessi  = 0;      // numero di processi di classe B ammessi in R

int running = 1;

int tharg[N];

pthread_t pth[N];

pthread_barrier_t bar; 

/////////////////////////////////////////////////////////////////////

void * utenteClasseA (void * arg) {

  int pid = *((int *)arg);

  int go = 0, idx, found;
  
  // questa e' una barriera per far partire tutti i thread insieme
  pthread_barrier_wait(&bar);

  while ( running ) {
    // questa serve per de-sincronizzare i threads  
    usleep(100+(rand()%100)); 
  
    sem_wait(&mux);
    if ( (nProcClasseAammessi == K) || ((statoR == CLASSEB) && (nProcInR > 0)) ) {
      nProcClasseAwaiting++;
      go = 0;
    } else {
      nProcInR++;
      nProcClasseAammessi++;
      statoR = CLASSEA; // necessario nel caso in cui sono il primo processore ad essere ammesso  
      go = 1;
    }    
    sem_post(&mux);
 
    if ( go == 0 ) {
      printf("[utenteClasseA]: T%02d is waiting ... statoR: %d, nProcInR: %d, nProcClasseAammessi: %d\n", pid, statoR, nProcInR, nProcClasseAammessi);
      sem_wait(&classeAsem);
      printf("[utenteClasseA]: T%02d awaken ...\n");
    }
    
    printf("[utenteClasseA]: T%02d is accessing ... statoR: %d, nProcInR: %d\n", pid, statoR, nProcInR);
    
    // ricerca del dispositivo libero
    sem_wait(&muxD);
    found = 0;
    idx   = 0;
    while (!found) {
      if (D[idx] == FREE) {
	      D[idx] = BUSY;
	      found  = 1;
      } else 
	      idx++;
    }
    sem_post(&muxD);
    
    printf("[utenteClasseA]: T%02d is using resource idx: %d\n", pid, idx);
    
    // simulazione del tempo di accesso  
    usleep(17+pid+(rand()%100)); 
  
    sem_wait(&mux);
    D[idx] = FREE;
    nProcInR--;
    printf("[utenteClasseA]: T%02d is leaving  ... statoR: %d, nProcInR: %d\n", pid, statoR, nProcInR);
    if ( nProcInR == 0 ) {
      if ( nProcClasseBwaiting > 0 ) {
      	printf("[utenteClasseA]: T%02d awaking %d users of classB\n", pid, nProcClasseBwaiting);	
      	statoR = CLASSEB;
	      nProcClasseBammessi = 0;
      	while ((nProcClasseBwaiting > 0) && ( nProcClasseBammessi < K)){
      	  sem_post(&classeBsem);
	        nProcClasseBwaiting--;
	        nProcClasseBammessi++;
      	  nProcInR++;
      	}
      } else {
	      nProcClasseAammessi = 0;
      	while ((nProcClasseAwaiting > 0) && (nProcClasseAammessi < K)){
      	  sem_post(&classeAsem);
	        nProcClasseAwaiting--;
	        nProcClasseAammessi++;
      	  nProcInR++;
      	}
      } 
    }
    sem_post(&mux);  
    
  }
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

void * utenteClasseB (void * arg) {

  int pid = *((int *)arg);

  int go = 0, idx, found;
  
  // questa e' una barriera per far partire tutti i thread insieme
  pthread_barrier_wait(&bar);

  while ( running ) {
    // questa serve per de-sincronizzare i threads
    usleep(17+(rand()%100)); 
  
    sem_wait(&mux);
    // se il numero di processi e' K, oppure e' > 0 ma occupato dall'altra classe
    if ( (nProcClasseBammessi == K) || ((statoR == CLASSEA) && (nProcInR > 0)) ) {
      nProcClasseBwaiting++;
      go = 0;
    } else {
      nProcInR++;
      nProcClasseBammessi++;
      statoR = CLASSEB; // necessario nel caso in cui non ci sono processi 
      go = 1;
    }    
    sem_post(&mux);
 
    if ( go == 0 ) {
      //printf("[utenteClasseB]: T%02d is waiting ... statoR: %d, nProcInR: %d\n", pid, statoR, nProcInR);
      printf("[utenteClasseB]: T%02d is waiting ... statoR: %d, nProcInR: %d, nProcClasseBammessi: %d\n", pid, statoR, nProcInR, nProcClasseBammessi);
      sem_wait(&classeBsem);
    }
    
    printf("[utenteClasseB]: T%02d is accessing ... statoR: %d, nProcInR: %d\n", pid, statoR, nProcInR);
    
    // ricerca del disposotivo
    sem_wait(&muxD);
    found = 0;
    idx   = 0;
    while (!found) {
      if (D[idx] == FREE) {
	      D[idx] = BUSY;
	      found  = 1;
      } else 
	      idx++;
    }
    sem_post(&muxD);
    
    // simulazione del tempo di accesso  
    usleep(23+pid+(rand()%100)); 
  
    sem_wait(&mux);
    D[idx] = FREE;
    nProcInR--;
    printf("[utenteClasseB]: T%02d is leaving  ... statoR: %d, nProcInR: %d\n", pid, statoR, nProcInR);
    if ( nProcInR == 0 ) {
      if ( nProcClasseAwaiting > 0 ) {
      	printf("[utenteClasseB]: T%02d awaking %d users of classA\n", pid, nProcClasseAwaiting);	
      	statoR = CLASSEA;
      	nProcClasseAammessi = 0;
      	while ((nProcClasseAwaiting > 0) && (nProcClasseAammessi < K)) {
      	  sem_post(&classeAsem);
	        nProcClasseAwaiting--;
	        nProcClasseAammessi++;
      	  nProcInR++;
      	}
      } else {
        nProcClasseBammessi = 0;
	      while ((nProcClasseBwaiting > 0) && (nProcClasseBammessi < K)) {
      	  sem_post(&classeBsem);
	        nProcClasseBwaiting--;
	        nProcClasseBammessi++;
      	  nProcInR++;
      	}
      }	  
    }
    sem_post(&mux);  
  
  }
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

int main () {

  int tid, i;
  
  sem_init(&mux,        0, 1);
  sem_init(&muxD,       0, 1);
  sem_init(&classeAsem, 0, 0);
  sem_init(&classeBsem, 0, 0);
  
  for (i=0; i<K; i++)
    D[i] = FREE;
  
  pthread_barrier_init(&bar, NULL, N);
  
  running = 1;
  
  srand(getpid());
  
  // creazione di N/2 threads di classe A
  for ( tid=0; tid<N/2; tid++ ) {
    tharg[tid] = tid;
    pthread_create( &pth[tid], NULL, utenteClasseA, &tharg[tid] );
  }
  
  // creazione di N/2 threads di classe B
  for ( tid=N/2; tid<N; tid++ ) {
    tharg[tid] = tid;
    pthread_create( &pth[tid], NULL, utenteClasseB, &tharg[tid] );
  
  }

  // tempo di esecuzione della simulazione
  sleep(10);
 
  running = 0;
   
  // wait end of threads
  for ( tid=0; tid < N; tid++ )
     pthread_join(pth[tid], NULL);   
  
  printf("statoR: %d, nProcInR: %d\n", statoR, nProcInR);
  
  sem_destroy(&mux);
  sem_destroy(&classeAsem);
  sem_destroy(&classeBsem);
  
  return 0; 

}
