///////////////////////////////////////////////////////////////////////
// Una strada a senso unico permette il passaggio a senso unico alternato
// di K auto al massimo. L'accesso alla strada e' regolato da un semaforo 
// (stradale) che agisce secono la seguente politica:
//
// - il numero massimo di auto che possono percorrere la strada e' K;
//
// - un auto e' ammessa al transito se:
//   1. se non ci sono auto che la percorrono
//   2. se ci sono auto che la percorrono nella medesima direzione
//
// - un auto NON e' ammessa al transito se:
//   1. ci sono auto che la percorrono in senso inverso
//   2. se ci sono K auro che la percorrono (indipendente dalla direzione)
//   3. se ci sono auto in attesa di percorrerla nella medesima direzione
//
// - quando l'ultimo auto ammesso ad attraversare il ponte in una 
//   direzione e' uscito, da la priorita' alle auto in attesa 
//   nella direzione opposta, se esistono.
//
// Possibili problemi di questa soluzione:
//
// 1. fissata una direzione, se un'auto impegna la strada prima che 
//    l'ultima auto che la percorre nella medesima direzione sia uscita, 
//    la precedenza NON viene mai data alle auto della direzione opposta.
//
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>  /* Semaphore */

#define N 128
#define K 10

#define DIR1 0
#define DIR2 1

sem_t mux;
sem_t dir1_sem;
sem_t dir2_sem;

char * direzioneStrada[2] = {"direzione 1", "direzione 2"};

int nAutoInTransito   = 0;
int nAutoInAttesaDir1 = 0;
int nAutoInAttesaDir2 = 0;

int direzione;            // direzione corrente

int tharg[2*N];

pthread_t pth[2*N];

pthread_barrier_t bar;

/////////////////////////////////////////////////////////////////////

void * autoDir1 ( void * arg ) {
  
  int pid = *((int *)arg);

  int go = 0;

  pthread_barrier_wait(&bar);
  
  usleep(100+rand()%100); 
  
  // protocollo di ingresso
  sem_wait(&mux);
  if ( (nAutoInTransito == K)                        || // auto in transito e' K
      ((nAutoInTransito > 0) && (direzione == DIR2)) || // ci auto in transito in direzione opposta
      (nAutoInAttesaDir1 > 0) ) {                       // ci sono auto in attesa nella medesima direzione
    go = 0;
    nAutoInAttesaDir1++;
  } else {
    go = 1;
    nAutoInTransito++;
    direzione = DIR1; // necessario se non ci sono auto in transito e la direzione e' orientata nel senso opposto
  }
  sem_post(&mux);
  
  if (go == 0) {  
    printf("[autoDir1]: auto %03d aspetta nAutoInTransito: %03d %s nAutoInAttesaDir1: %03d\n", 
	pid, nAutoInTransito, direzioneStrada[direzione], nAutoInAttesaDir1);
    sem_wait(&dir1_sem); 
  }
  
  printf("[autoDir1]: auto %03d percorre la strada in %s\n", pid, direzioneStrada[direzione]);
  usleep(1+pid+rand()%100); 
  
  // protocollo di uscita
  sem_wait(&mux);
  nAutoInTransito--;
  printf("[autoDir1]: auto %03d ha percorso la strada nAutoInTransito: %03d\n", pid, nAutoInTransito);
  if ( nAutoInTransito == 0 ) {
    if ( nAutoInAttesaDir2 > 0 ) { // se ci sono auto in attesa in direzione opposta 
      direzione = DIR2; 
      while ((nAutoInAttesaDir2 > 0) && (nAutoInTransito < K)) {
	nAutoInAttesaDir2--;
	nAutoInTransito++;
	sem_post(&dir2_sem);
      }
    } else { // se NON ci sono auto in attesa in direzione opposta
      while ((nAutoInAttesaDir1 >0) && (nAutoInTransito < K)) {
	nAutoInAttesaDir1--;
	nAutoInTransito++;
	sem_post(&dir1_sem);	
      }
    }
  }
  sem_post(&mux);
  
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

void * autoDir2 ( void * arg ) {
  
  int pid = *((int *)arg);

  int go = 0;

  pthread_barrier_wait(&bar);
  
  usleep(100+rand()%100); 
  
  // protocollo di ingresso
  sem_wait(&mux);
  if ( (nAutoInTransito == K)                        || // auto in transito e' K
      ((nAutoInTransito > 0) && (direzione == DIR1)) || // ci auto in transito in direzione opposta
      (nAutoInAttesaDir2 > 0) ) {                       // ci sono auto in attesa nella medesima direzione
    go = 0;
    nAutoInAttesaDir2++;
  } else {
    go = 1;
    nAutoInTransito++;
    direzione = DIR2; // necessario se non ci sono auto in transito e la direzione e' orientata nel senso opposto
  }
  sem_post(&mux);
  
  if (go == 0) {  
    printf("[autoDir2]: auto %03d aspetta nAutoInTransito: %03d %s nAutoInAttesaDir2: %03d\n", 
	pid, nAutoInTransito, direzioneStrada[direzione], nAutoInAttesaDir2);
    sem_wait(&dir2_sem); 
  }
  
  printf("[autoDir2]: auto %03d percorre la strada in  %s\n", pid, direzioneStrada[direzione]);
  usleep(1+pid+rand()%100); 
  
  // protocollo di uscita
  sem_wait(&mux);
  nAutoInTransito--;
  printf("[autoDir2]: auto %03d ha percorso la strada nAutoInTransito: %03d\n", pid, nAutoInTransito);
  if ( nAutoInTransito == 0 ) {
    if ( nAutoInAttesaDir1 > 0 ) { // se ci sono auto in attesa in direzione opposta 
      direzione = DIR1; 
      while ((nAutoInAttesaDir1 > 0) && (nAutoInTransito < K)) {
	nAutoInAttesaDir1--;
	nAutoInTransito++;
	sem_post(&dir1_sem);
      }
    } else { // se NON ci sono auto in attesa in direzione opposta
      while ((nAutoInAttesaDir2 >0) && (nAutoInTransito < K)) {
	nAutoInAttesaDir2--;
	nAutoInTransito++;
	sem_post(&dir2_sem);	
      }
    }
  }
  sem_post(&mux);
  
  pthread_exit(NULL);
}


/////////////////////////////////////////////////////////////////////

int main () {

  int tid, ret;
  
  sem_init(&mux,      0, 1);
  sem_init(&dir1_sem, 0, 0);
  sem_init(&dir2_sem, 0, 0);
  
  pthread_barrier_init(&bar, NULL, 2*N);

  direzione = DIR1; // inizializzazione arbitraria
    
  srand(getpid());
  
  // create threads
  for ( tid=0; tid<N; tid++ ) {
    tharg[tid]   = tid;
    ret = pthread_create( &pth[tid], NULL, autoDir1, &tharg[tid] );
    if (ret != 0)
      perror("pthread_create:");
    tharg[N+tid] = N+tid;
    ret = pthread_create( &pth[N+tid], NULL, autoDir2, &tharg[N+tid] );
    if (ret != 0)
      perror("pthread_create:");
  }
  
  // wait end of threads
  for ( tid=0; tid < (2*N-1); tid++ )
     pthread_join(pth[tid], NULL);   
  
  sem_destroy(&mux);
  sem_destroy(&dir1_sem);
  sem_destroy(&dir2_sem);
  
  printf("nAutoInTransito: %d, nAutpInAttesaDir1: %d, nAutoInAttesaDir2: %d\n",
    nAutoInTransito, nAutoInAttesaDir1, nAutoInAttesaDir2);
  
  return 0; 

}
