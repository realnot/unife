///////////////////////////////////////////////////////////////////////
// Un parcheggio e' dotato di una uscita e di un ingresso. La capacita' 
// del parcheggio e' di K auto. Supponendo che le auto sono threads di 
// uno stesso processo Linux, descrivere il protocollo di ingresso e 
// uscita che ciascuna auto deve osservare per utilizzare il parcheggio.
///////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h> // to use usleep

#define FREE 1
#define BUSY 0

// numero di auto
#define NAUTO 256

// capacita' del parcheggio
#define K 17

// variabili condivise
sem_t postiLiberi;

// structure to pass arguments to threads 
typedef struct {
  int tid;    // thread identifier
  int * park; // pointer to park array
  int * tret; // thread return value
} targv_t;

// barriera di sincronizzazione per far partire tutti i thread insieme
pthread_barrier_t bar;

// mutex utilizzato per la ricerca del posto nel parcheggio
pthread_mutex_t mux2;

void * autoFunc ( void * targv ) {
  int tid;
  int * park;
  int found, idx;
  void * tret;

  tid  = ((targv_t *)targv)->tid;
  park = ((targv_t *)targv)->park;
  tret = ((targv_t *)targv)->tret;

  // questa e' una barriera per far partire tutti i thread insieme
  pthread_barrier_wait(&bar);

  // desincronizzo i thread
  usleep( 1110 * ( rand() % NAUTO ) );

  // protocollo di ingresso
  sem_wait(&postiLiberi);
  
  // ricerca del posto libero
  pthread_mutex_lock(&mux2);
  fprintf(stderr, "AUTO%04d cerca lo slot dove parcheggiare ...\n", tid);
  found = 0;
  idx   = 0;
  while ( ! found ) {
    if ( park[idx] == FREE ) {
      fprintf(stderr, "AUTO%04d parcheggia nel posto di indice %d\n", tid, idx);
      park[idx] = BUSY;
      found = 1;
    } else {
      idx++;
    }
  }
  pthread_mutex_unlock(&mux2);
  
  // simulazione del tempo di parcheggio
  usleep(317+rand()%NAUTO);
  
    
  // libero il posto
  pthread_mutex_lock(&mux2);
  fprintf(stderr, "AUTO%04d libera il posto %d\n", tid, idx); 
  park[idx] = FREE;
  pthread_mutex_unlock(&mux2);

  // esco dal parcheggio  
  sem_post(&postiLiberi);
  
  *((int *)tret) = 0;

  pthread_exit(tret);  
}

///////////////////////////////////////////////////////////////////////

int main ( int argc, char *argv[] ) {
  int i, t, r;

  int park[K]; 

  pthread_t thauto[NAUTO];
  targv_t targv[NAUTO];
  int tret[NAUTO];

  // inizializzazioni
  sem_init(&postiLiberi, 0, K);
  pthread_mutex_init(&mux2, NULL);

  for (i=0; i<K; i++)
    park[i] = FREE;

  srand(17);

  pthread_barrier_init(&bar, NULL, NAUTO);

  /////////////////////////////////////////////////////////////////////

  for (t=0; t<NAUTO; t++) {

    targv[t].tid  = t;
    targv[t].park = park;
    targv[t].tret = (void*) &tret[t];   

    r = pthread_create(&thauto[t], NULL, autoFunc, (void*) &targv[t]);

    if ( r != 0 )
      perror("ERROR creation of thread FAILED");
      // aggiungere gestione errore
  }

  for (t=0; t<NAUTO; t++) {
    r = pthread_join(thauto[t], (void*)&tret[t]);
    if ( r != 0 ) 
      perror("ERROR thread join FAILED");
      // aggiungere gestione errore
  }

  fprintf(stderr, "simulation ENDED.\n");

  return 0;
}
