#include <stdio.h>      /* printf, etc */
#include <sys/types.h>  /* almost always need this with system programming */
#include <unistd.h>     /* fork, getpid, getppid */
#include <string.h>     /* strcmp */
#include <stdlib.h>     /* exit */
#include <sys/wait.h>   /* wait */

/* Another fork test program - this one includes a command line option that 
   specifies which process should call sleep

   if "c" is specified the child sleeps, otherwise the parent sleeps

*/

int main(int argc, char **argv) {
  
  pid_t childpid;
  
  int sleeptime = 0;
  int sleepflag = 0; /* 0=child; 1=parent */
  int status = 0;
  int ret = 0;

  if ( argc != 3 ) {
    printf("usage: %s (p|c) sleep-time\n",argv[0]);
    exit(0);
  }
  
  sleepflag = ( strcmp(argv[1], "c") ) ? 1 : 0;
  
  sleeptime = atoi(argv[2]);
  
  childpid = fork();
  
  if ( childpid != 0 ) {

    /* fork returns child's pid in the parent - this is parent only code */
    if ( sleepflag == 1 ) {
      fprintf(stderr, "[parent]: I'm gonna to sleep for %d secs\n", sleeptime);
      /* parent sleeps */
      sleep(sleeptime);
    }
    
    printf("[parent]: Hello, I am the parent, my pid is %u\n", getpid());
    printf("[parent]: My child's pid is (or was) %u\n", childpid);
    
    fprintf(stderr, "[parent]: I'm gonna to wait my child ...\n");
    
    if ( wait(&status) != -1 ) {
      printf("[parent]: status: %d\n", status);
    } else {
      fprintf(stderr, "[parent]: wait failed\n"); 
    }
    
    printf("[parent]: Bye Bye\n");
    
  } else if ( childpid == 0 ) {

    /* fork returned 0 - this is child only code */
    if ( sleepflag == 0 ) {
      fprintf(stderr, "[child ]: I'm gonna to sleep for %d secs\n", sleeptime);
      /* child sleeps */
      sleep(sleeptime);
    }
    
    printf("[child ]: I am the child, my pid is %u\n", getpid());
    printf("[child ]: My current parent's pid is %u\n", getppid());

    ret = execl("/bin/ls", "ls", "-ltr", (char *) 0);
    
    if ( ret ) {
      fprintf(stderr,"[child ]: ERROR execl failed: %d\n", ret);
    } else {
      printf("[child ]: Bye Bye\n"); // will this instruction be printed ???
    }
    
  } else {
   
    fprintf(stderr, "[ERROR]: fork failed return value: %d\n", childpid); 

  }
        
  return(0);
}
