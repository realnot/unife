#include <stdlib.h>
#include <unistd.h>     /* fork, getpid, getppid */
#include <stdio.h>
#include <sys/wait.h>   /* wait */

int main () {
  pid_t pid;
  int ret;
  // P
  printf("uno\n"); 
  pid = fork();
  if ( pid > 0 ) {
    // P
    wait(&ret); 
    printf("ret: %d %d\n", ret, WEXITSTATUS(ret));
    exit(1);
  } else {
    //  P1
    printf("due\n"); 
    pid = fork();
    if ( pid > 0 ) {
      // P1
      wait(&ret); 
      printf("ret: %d %d\n", ret, WEXITSTATUS(ret));
    } else {
      // P2
      execlp("/bin/date", "date", NULL);
      exit(2);
    }
    // P1
    exit(3);
  }
}
