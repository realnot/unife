////////////////////////////////////////////////////////////////////////
// Esempio di produttore consumatore con buffer circolare. 
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define OVER (-1)
#define max 2000
#define BUFFER_SIZE 100

typedef struct {
  int buffer[BUFFER_SIZE];
  pthread_mutex_t M;
  int readpos, writepos;
  int cont;
  pthread_cond_t PIENO;
  pthread_cond_t VUOTO;
} prodcons;


prodcons B;

////////////////////////////////////////////////////////////////////////

void init (prodcons * b) {
  pthread_mutex_init(&b->M,    NULL);
  pthread_cond_init(&b->PIENO, NULL);
  pthread_cond_init(&b->VUOTO, NULL);
  b->cont     = 0;
  b->readpos  = 0;
  b->writepos = 0;
}

////////////////////////////////////////////////////////////////////////

void inserisci (prodcons *b, int MSG) {
  pthread_mutex_lock(&b->M);
  
  while ( b->cont == BUFFER_SIZE )       // buffer vpieno
    pthread_cond_wait(&b->PIENO, &b->M);
    
  // scrivi MSG e aggiornato stato buffer
  b->buffer[b->writepos] = MSG;
  b->cont++;
  b->writepos++;
  
  // gestione circolare
  if ( b->writepos >= BUFFER_SIZE )
    b->writepos = 0;
    
  // risveglia un eventuale thread consumatore sospeso
  pthread_cond_signal(&b->VUOTO);
  pthread_mutex_unlock(&b->M);
}

////////////////////////////////////////////////////////////////////////

int estrai (prodcons *b) {
  int MSG;
  pthread_mutex_lock(&b->M);
  while (b->cont == 0) // buffer vuoto
    pthread_cond_wait(&b->VUOTO, &b->M);
  
  // leggi il messaggio e aggiorna lo stato
  MSG = b->buffer[b->readpos];
  
  b->cont--;
  b->readpos++;
  
  // gestione circolare
  if ( b->readpos >= BUFFER_SIZE )
    b->readpos = 0;
    
  // risveglia eventuale thread produttore
  pthread_cond_signal(&b->PIENO);
  pthread_mutex_unlock(&b->M);
  
  return MSG;
}

////////////////////////////////////////////////////////////////////////

void * produttore (void * arg){
  int n;
  for (n=0; n < max; n++) {
    printf("Thread produttore %d --> \n", n);
    inserisci(&B,n);
  }
  inserisci(&B, OVER);
  return NULL;
}

////////////////////////////////////////////////////////////////////////

void * consumatore (void * arg){
  int d;
  while (1) {
    d = estrai(&B);
    if (d==OVER)
      break;
    printf("Thread consumatore: --> %d\n", d);
  }
  return NULL;
}
////////////////////////////////////////////////////////////////////////

int main() {
  pthread_t th_a, th_b;
  
  init(&B);
  
  // Creazione Threads
  if ( pthread_create(&th_a, NULL, produttore,  0) != 0 ) {
    fprintf(stderr, "ERROR: failed to create thread\n");
    return -1; 
  }
  
  if ( pthread_create(&th_b, NULL, consumatore, 0) != 0 ) {
    fprintf(stderr, "ERROR: failed to create thread\n");
    return -1; 
  }

  // Attesa terminazione threads
  pthread_join(th_a, NULL);
  pthread_join(th_b, NULL);
  
  return 0;
}
