///////////////////////////////////////////////////////////////////////
// Una strada apermette il passaggio di auto a senso unico 
// alternato. L'accesso alla strada e' regolato da un semaforo 
// stradale che agisce secono la seguente politica:
//
// - fissata una direzione il numero massimo di auto consecutive che 
//   possono essere ammesse e' K; tutte le auto successive 
//   sono eventualmente bloccate all'ingresso.
//
// - un auto e' ammessa al transito se:
//   1. se non ci sono auto che transitano o
//   2. se ci sono auto che la percorrono nella medesima direzione
//
// - un auto NON e' ammessa al transito se:
//   1. ci sono auto che la percorrono in senso inverso
//   2. se sono state ammesse K auto a percorrere la strada nella medesima 
//      direzione 
//
// - quando l'ultimo auto ammesso ad attraversare il ponte in una 
//   direzione e' uscito, da la priorita' alle auto in attesa 
//   nella direzione opposta, se esistono.
//
// In questa soluzione:
// 
// 1. fissata una direzione vengono ammesse al piu' K auto in una direzione, 
//    e tutte le successive sono bloccate. 
//
// 2. L'ultima che lascia la strada da la precedenza alle auto in attesa nella 
//    direzione opposta se esistono, altrimenti sveglia le auto in attesa 
//    nella medesima direzione. In ogni caso vengono sveglaite TUTTE le auto, 
//    ma le auto risvegliate ri-controllano la condizione che le auto ammesse 
//    NON siano piu' di K, e in caso negativo si ri-sospendono. 
//    (variante: si potrebbero svegliare al piu' K auto).
//
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define N 128
#define K 10

#define DIR1 0
#define DIR2 1

pthread_mutex_t mux;

pthread_cond_t dir1_cond;
pthread_cond_t dir2_cond;

char * direzioneStrada[2] = {"direzione 1", "direzione 2"};

int nAutoInTransito   = 0;

int nAutoAmmesseDir1  = 0; // numero auto ammesse nella direzione 1
int nAutoAmmesseDir2  = 0; // numero auto ammesse nella direzione 2

int nAutoInAttesaDir1 = 0; // numero auto in attesa di accedere dalla direzione 1
int nAutoInAttesaDir2 = 0; // numero auto in attesa di accedere dalla direzione 2

int direzione;             // direzione corrente della strada 

int tharg[2*N];

pthread_t pth[2*N];

pthread_barrier_t bar;

/////////////////////////////////////////////////////////////////////

void * autoDir1 ( void * arg ) {
  
  int pid = *((int *)arg);

  pthread_barrier_wait(&bar);
  
  usleep(100+rand()%100); 
  
  // protocollo di ingresso
  pthread_mutex_lock(&mux);
  while ( (nAutoAmmesseDir1 == K) || ((direzione == DIR2) && (nAutoInTransito > 0)) ) {
    // semaforo rosso
    nAutoInAttesaDir1++;
    printf("[autoDir1]: auto %03d aspetta nAutoInTransito: %03d %s nAutoInAttesaDir1: %03d\n", 
      pid, nAutoInTransito, direzioneStrada[direzione], nAutoInAttesaDir1);
    pthread_cond_wait(&dir1_cond, &mux);
  }
  nAutoAmmesseDir1++;
  nAutoInTransito++;
  direzione = DIR1; // necessario se non ci sono auto in transito e la direzione e' orientata nel senso opposto 
  pthread_mutex_unlock(&mux);
  
  printf("[autoDir1]: auto %03d percorre la strada in %s\n", pid, direzioneStrada[direzione]);
  usleep(1+pid+(rand()%100)); 
   
  // protocollo di uscita
  pthread_mutex_lock(&mux);
  nAutoInTransito--;
  printf("[autoDir1]: auto %03d ha percorso la strada nAutoInTransito: %03d, nAutoInAttesaDir2: %d, nAutoInAttesaDir1: %d\n", 
      pid, nAutoInTransito, nAutoInAttesaDir2, nAutoInAttesaDir1);
  if ( nAutoInTransito == 0 ) {
    if ( nAutoInAttesaDir2 > 0 ) { 
      // se ci sono auto in attesa in direzione opposta le sveglio TUTTE
      direzione = DIR2;
      nAutoAmmesseDir2 = 0;
      while (nAutoInAttesaDir2 > 0) {
	nAutoInAttesaDir2--;
	pthread_cond_signal(&dir2_cond);
      }
    } else { 
      // sveglio TUTTE quelle in attesa di passae nella medesima direzione
      nAutoAmmesseDir1 = 0;
      while (nAutoInAttesaDir1 >0) {
	nAutoInAttesaDir1--;
	pthread_cond_signal(&dir1_cond);	
      }
    }
  }
  pthread_mutex_unlock(&mux);
  printf("[autoDir2]: auto %03d Bye Bye\n", pid);
  
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

void * autoDir2 ( void * arg ) {
  
  int pid = *((int *)arg);

  pthread_barrier_wait(&bar);
  
  usleep(100+rand()%100); 
  
  // protocollo di ingresso
  pthread_mutex_lock(&mux);
  while ( (nAutoAmmesseDir2 == K) || ((direzione == DIR1) && (nAutoInTransito > 0)) ) {
    nAutoInAttesaDir2++;
    // semaforo rosso
    printf("[autoDir2]: auto %03d aspetta nAutoInTransito: %03d %s nAutoInAttesaDir1: %03d\n", 
      pid, nAutoInTransito, direzioneStrada[direzione], nAutoInAttesaDir1);
    pthread_cond_wait(&dir2_cond, &mux);
  }
  nAutoAmmesseDir2++;
  nAutoInTransito++;
  direzione = DIR2; // necessario se non ci sono auto in transito e la direzione e' orientata nel senso opposto 
  pthread_mutex_unlock(&mux);
  
  printf("[autoDir2]: auto %03d percorre la strada in  %s\n", pid, direzioneStrada[direzione]);
  usleep(1+pid+(rand()%100)); 
  
  // protocollo di uscita
  pthread_mutex_lock(&mux);
  nAutoInTransito--;
  printf("[autoDir2]: auto %03d ha percorso la strada nAutoInTransito: %03d, nAutoInAttesaDir1: %d, nAutoInAttesaDir2, %d\n", 
      pid, nAutoInTransito, nAutoInAttesaDir1, nAutoInAttesaDir2);
  if ( nAutoInTransito == 0 ) {
    if ( nAutoInAttesaDir1 > 0 ) { // se ci sono auto in attesa in direzione opposta 
      direzione = DIR1; 
      nAutoAmmesseDir1 = 0;  
      while (nAutoInAttesaDir1 > 0) {
	nAutoInAttesaDir1--;
	pthread_cond_signal(&dir1_cond);
      }
    } else { // se NON ci sono auto in attesa in direzione opposta
      nAutoAmmesseDir1 = 0;
      while (nAutoInAttesaDir2 >0) {
	nAutoInAttesaDir2--;
	pthread_cond_signal(&dir2_cond);	
      }
    }
  }
  pthread_mutex_unlock(&mux);
  printf("[autoDir2]: auto %03d Bye Bye\n", pid);  
  pthread_exit(NULL);
}


/////////////////////////////////////////////////////////////////////

int main () {

  int tid, ret;
  
  pthread_mutex_init(&mux, NULL);
  
  pthread_cond_init(&dir1_cond, NULL);
  pthread_cond_init(&dir2_cond, NULL);
  
  pthread_barrier_init(&bar, NULL, 2*N);
  
  direzione = DIR1; // inizializzazione arbitraria
    
  srand(getpid());
  
  // create threads
  for ( tid=0; tid<N; tid++ ) {
    tharg[tid]   = tid;
    ret = pthread_create( &pth[tid], NULL, autoDir1, &tharg[tid] );
    if (ret != 0)
      perror("pthread_create:");
    tharg[N+tid] = N+tid;
    ret = pthread_create( &pth[N+tid], NULL, autoDir2, &tharg[N+tid] );
    if (ret != 0)
      perror("pthread_create:");
  }
  
  // wait end of threads
  for ( tid=0; tid < (2*N-1); tid++ )
     pthread_join(pth[tid], NULL);   
  
  pthread_mutex_destroy(&mux);
  
  pthread_cond_destroy(&dir1_cond);
  pthread_cond_destroy(&dir2_cond);
  
  printf("nAutoInTransito: %d, nAutpInAttesaDir1: %d, nAutoInAttesaDir2: %d\n",
    nAutoInTransito, nAutoInAttesaDir1, nAutoInAttesaDir2);
  
  return 0; 

}
