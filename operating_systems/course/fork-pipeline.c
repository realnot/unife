#include <stdio.h>      /* printf, etc */
#include <sys/types.h>  /* almost always need this with system programming */
#include <unistd.h>     /* fork, getpid, getppid */
#include <string.h>     /* strcmp */
#include <stdlib.h>     /* exit */
#include <sys/wait.h>   /* wait */

////////////////////////////////////////////////////////////////////////
// questo programma genera una gerarchia di processi padre, figlio, 
// nipote, ... Il numero di processi generati e' pari al numero di 
// argomenti del programma.  
// Ogni processo attende prima la terminazione del 
// processo figlio e successivamente esegue un comando.
//
// Es: fork-pipeline ls date  
////////////////////////////////////////////////////////////////////////

void foo ( int i, int argc, char * argv[] ) {

  pid_t childpid, pid;
  int   status;
  char  cmd[64]; 

  pid = getpid();
  
  if ( i < argc ) {
    
    childpid = fork();   
   
    if ( childpid == 0 ) {
      // codice del figlio     
      i = i + 1;
      printf ("  P%d: invoco foo(%d, ...)\n", getpid(), i);
      foo(i, argc, argv );
      
    } else {
      // codice del padre
      printf ("P%d: ho creato il processo %d\n", pid, childpid);
      
      pid = getpid();
      
      wait(&status);
      
      printf ("  P%d: processo %d terminato, eseguo il comando %s\n", 
	      pid, childpid, argv[i]);
      
      sprintf(cmd, "/bin/%s", argv[i]);
      
      execl(cmd, argv[i], NULL);
       
      fprintf(stderr,"ERROR: exec %s fallita\n", argv[i]); 
    }
    
  }
    
  return;

}

////////////////////////////////////////////////////////////////////////

int main ( int argc, char * argv[] ) {
  
  int i = 1;
  
  printf ("  P%d: invoco foo(%d, ...)\n", getpid(), i);
  
  foo( i, argc, argv );  
  
  exit(0);
}
