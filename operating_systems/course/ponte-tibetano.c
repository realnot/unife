///////////////////////////////////////////////////////////////////////
// Un ponte tibetano (orientato da nord a sud), permette il passaggio a 
// senso unico alternato di 10 persone al massimo. L'accesso al ponte e' 
// regolato da un semaforo intelligente che opera secondo la seguente 
// politica:
//
// - il numero massimo di passanti che possono attraversare il ponte 
//   in una direzione e' K. I passanti possono presentarsi all'accesso 
//   del ponte anche distanziati nel tempo;
//
// - se il ponte e' impegnato da passanti in una direzione, non 
//   consente l'accesso di passanti in direzione opposta;
//
// - un passante che si presenta all'accesso del ponte per percorrerlo 
//   in una direzione, puo' essere ammesso se il ponte e' impegnato 
//   da passanti che lo percorrono nella medesima direzione;
//
// - quando l'ultimo passante ammesso ad attraversare il ponte in una 
//   direzione e' uscito, da la priorita' ai passanti in attesa 
//   nella direzione opposta, se esistono.
//
//
// Possibili problemi di questa soluzione:
//
//   1. fissata una direzione, un utente che deve attraversare il ponte 
//      e trova gia' K utenti sul ponte, si sospende.
//      Un successivo utente puo' essere ammesso prima di quelli sospesi
//      se nel frattempo qualche utente ha liberato il ponte.
//
//   2. fissata una direzione, se un utente impegna il ponte prima che 
//      l'ultimo utente che lo percorre nella medesima direzione sia uscito, 
//      la precedenza NON viene mai data agli utenti della direzione opposta.
//
///////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>  /* Semaphore */

#define N 128
#define K 10

#define NORD2SUD 0
#define SUD2NORD 1

sem_t mux;
sem_t Nord2Sud;
sem_t Sud2Nord;

char * direzionePonte[2] = {"nordToSud", "sudToNord"};

int nPInTransito    = 0;
int nPattesaNordSud = 0;
int nPattesaSudNord = 0;

int direzione       = NORD2SUD;

int tharg[2*N];

pthread_t pth[2*N];

pthread_barrier_t bar;

/////////////////////////////////////////////////////////////////////

void * P_Nord2Sud (void * arg) {

  int pid = *((int *)arg);

  int go = 0;

  pthread_barrier_wait(&bar);
  
  usleep(100+rand()%100); 
  
  sem_wait(&mux);
  // protocollo di ingresso: se il ponte e' impegnato da K utenti, oppure ci sono 
  // utenti (>0) che lo percorrono in direzione opposta, allora l'utente deve aspettare. 
  // Quindi:
  //   - finche' ci sono K utenti della medesima direzione che impegnano il ponte, i successivi aspettano
  //   - ma un utente puo' ammesso anche se ci sono utenti della medesima direzione in attesa (un-fair)
  if (  (nPInTransito == K) || ((nPInTransito >0) && (direzione == SUD2NORD)) ) { 
    nPattesaNordSud++;
    go = 0;
  } else {
    nPInTransito++;
    direzione = NORD2SUD; // necessario nel caso in cui non si sono passanti
    go = 1;
  }
  sem_post(&mux);
 
  if ( go == 0 ) {
    printf("[P_Nord2Sud]: pedestrian %02d is waiting ... %02d %s\n", 
      pid, nPInTransito, direzionePonte[direzione]);
    sem_wait(&Nord2Sud);
  }
  
  printf("[P_Nord2Sud]: pedestrian %02d is crossing the bridge %s\n", 
    pid, direzionePonte[direzione]);
  usleep(1+pid+rand()%100); 
  
  sem_wait(&mux);
  // protocollo di uscita: chi lascia il ponte decrementa il numero di utenti in 
  // transito ma NON effettua una sem_post sul semaforo Nord2Sud; quindi possono 
  // esserci utenti in attesa nella medesima direzione, quelli che sono arrivati 
  // quando il ponte era impegnato da K utenti. 
  nPInTransito--;
  printf("[P_Nord2Sud]: pedestrian %02d left the bridge, nPInTransito: %02d %s\n", 
    pid, nPInTransito, direzionePonte[direzione]);
  if (  nPInTransito == 0 ) { 
    if ( nPattesaSudNord > 0 ) {
      direzione = SUD2NORD; // cambia direzione
      while ((nPattesaSudNord > 0 ) && (nPInTransito < K)) {
  	nPattesaSudNord--;
  	nPInTransito++;
  	sem_post(&Sud2Nord);
      }
    } else {
      while ((nPattesaNordSud > 0 ) && (nPInTransito < K)) {
  	nPattesaNordSud--;
  	nPInTransito++;
  	sem_post(&Nord2Sud);
      }
    }
  }    
  sem_post(&mux);
  
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

void * P_Sud2Nord (void * arg) {

  int go = 0;
  
  int pid = *((int *)arg);

  pthread_barrier_wait(&bar);
  
  usleep(1+rand()%100); 
  
  sem_wait(&mux);
  // protocollo di ingresso: se il ponte e' impegnato da K utenti, oppure ci sono 
  // utenti che lo percorrono in direzione opposta, allora l'utente deve aspettare. 
  // Quindi:
  //   - finche' ci sono K utenti della medesima direzione che impegnano il ponte, i successivi aspettano
  //   - ma un utente puo' essere ammesso anche se ci sono utenti della medesima direzione in attesa (un-fair)
  if (  (nPInTransito == K) || ((nPInTransito > 0) && (direzione == NORD2SUD)) ) { 
    nPattesaSudNord++;
    go = 0;
  } else {
    nPInTransito++;
    direzione = SUD2NORD; // necessario nel caso in cui non si sono passanti
    go = 1;
  }
  sem_post(&mux);
  
  if ( go == 0 ) {
    printf("[P_Sud2Nord]: pedestrian %02d is waiting ... nPInTransito: %02d  dir: %s\n", 
      pid, nPInTransito, direzionePonte[direzione]);
    sem_wait(&Sud2Nord);
  }
  
  printf("[P_Sud2Nord]: pedestrian %02d is crossing the bridge %s\n", pid, direzionePonte[direzione]);
  usleep(1+pid+rand()%100); 
  
  // protocollo di uscita: chi lascia il ponte decrementa il numero di utenti in 
  // transito ma NON effettua una sem_post sul semaforo Sud2Nord; quindi possono 
  // esserci utenti in attesa nella medesima direzione in attesa, quelli che sono 
  // arrivati quando il ponte era impegnato da K utenti.  
  sem_wait(&mux);
  nPInTransito--; 
  printf("[P_Sud2Nord]: pedestrian %02d left the bridge, nPInTransito: %d\n", pid, nPInTransito);
  if ( nPInTransito == 0 ) {
    if ( nPattesaNordSud > 0 ) {
      direzione = NORD2SUD;
      while ((nPattesaNordSud > 0 ) && (nPInTransito < K)) {
  	nPattesaNordSud--;
  	nPInTransito++;
  	sem_post(&Nord2Sud);
      }
    } else {
      while ((nPattesaSudNord > 0 ) && (nPInTransito < K)) {
  	nPattesaSudNord--;
  	nPInTransito++;
  	sem_post(&Sud2Nord);
      }
    }
  }  
  sem_post(&mux);
    
  pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////

int main () {

  int tid, ret;
  
  sem_init(&mux,      0, 1);
  sem_init(&Nord2Sud, 0, 0);
  sem_init(&Sud2Nord, 0, 0);
  
  pthread_barrier_init(&bar, NULL, 2*N);
  
  srand(getpid());
  
  // create threads
  for ( tid=0; tid<N; tid++ ) {
    tharg[tid]   = tid;
    ret = pthread_create( &pth[tid], NULL, P_Nord2Sud, &tharg[tid] );
    if (ret != 0)
      perror("pthread_create:");
    tharg[N+tid] = N+tid;
    ret = pthread_create( &pth[N+tid], NULL, P_Sud2Nord, &tharg[N+tid] );
    if (ret != 0)
      perror("pthread_create:");
  }
  
  // wait end of threads
  for ( tid=0; tid < (2*N-1); tid++ )
     pthread_join(pth[tid], NULL);   
  
  sem_destroy(&mux);
  sem_destroy(&Nord2Sud);
  sem_destroy(&Sud2Nord);
  
  printf("nPInTransito: %d, nPattesaNordSud: %d, nPattesaSudNord: %d\n",
    nPInTransito,nPattesaNordSud,nPattesaSudNord);
  
  return 0; 

}
