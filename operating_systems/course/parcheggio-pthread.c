///////////////////////////////////////////////////////////////////////
// Un parcheggio e' dotato di una uscita e di un ingresso. La capacita' 
// del parcheggio e' di K auto. Supponendo che le auto sono threads di 
// uno stesso processo Linux, descrivere il protocollo di ingresso e 
// uscita che ciascuna auto deve osservare per utilizzare il parcheggio.
///////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h> // to use usleep

#define FREE 1
#define BUSY 0

// numero di auto
#define NAUTO 256

// capacita' del parcheggio
#define K 17

// variabili condivise
int postiLiberi = K; // numero di posti liberi
int nauto=0;         // numero di auto ammesse al parcheggio (usata solo per monitorare lo stato del parcheggio)

// mutex per l'accesso alle variabili condivise
pthread_mutex_t mux1, mux2;

// structure to pass arguments to threads 
typedef struct {
  int tid;    // thread identifier
  int * park; // pointer to park array
  int * tret; // thread return value
} targv_t;

pthread_barrier_t bar;

// variabile di condizione su cui si sospendono le auto in attesa di 
// un posto libero
pthread_cond_t attesaPostoLibero;

void * autoFunc ( void * targv ) {
  int tid;
  int * park;
  int found, idx;
  void * tret;

  tid  = ((targv_t *)targv)->tid;
  park = ((targv_t *)targv)->park;
  tret = ((targv_t *)targv)->tret;

  // questa e' una barriera per far partire tutti i thread insieme
  pthread_barrier_wait(&bar);

  // desincronizzo i thread
  usleep( 1110 * ( rand() % NAUTO ) );

  // protocollo di ingresso
  pthread_mutex_lock(&mux1);
  nauto++;
  fprintf(stderr, "AUTO%04d richiede accesso al parcheggio ... postiLiberi=%d nauto: %d\n", tid, postiLiberi, nauto); 
  while ( postiLiberi == 0 ) {
    pthread_cond_wait( &attesaPostoLibero, &mux1);
  }
  postiLiberi--;
  pthread_mutex_unlock(&mux1);

  // ricerca del posto libero
  pthread_mutex_lock(&mux2);
  fprintf(stderr, "AUTO%04d cerca lo slot dove parcheggiare ...\n", tid);
  found = 0;
  idx = 0;
  while ( ! found ) {
    if ( park[idx] == FREE ) {
      fprintf(stderr, "AUTO%04d parcheggia nel posto di indice %d\n", tid, idx);
      park[idx] = BUSY;
      found = 1;
    } else {
      idx++;
    }
  }
  pthread_mutex_unlock(&mux2);
  
  // simulazione del tempo di parcheggio
  usleep(317+rand()%NAUTO);      
  
  // libero il posto
  pthread_mutex_lock(&mux2);
  fprintf(stderr, "AUTO%04d libera il posto %d\n", tid, idx); 
  park[idx] = FREE;
  pthread_mutex_unlock(&mux2);
  
  // protocollo di uscita
  pthread_mutex_lock(&mux1);
  nauto--;
  postiLiberi++;
  pthread_cond_signal( &attesaPostoLibero );
  fprintf(stderr, "AUTO%04d lascia il parcheggio ... postiLiberi: %d, nauto: %d\n", tid, postiLiberi, nauto); 
  pthread_mutex_unlock(&mux1); 

  *((int *)tret) = 0;

  pthread_exit(tret);  
}

///////////////////////////////////////////////////////////////////////

int main ( int argc, char *argv[] ) {
  int i, t, r;

  int park[K]; 

  pthread_t thauto[NAUTO];
  targv_t targv[NAUTO];
  int tret[NAUTO];

  // inizializzazioni
  pthread_mutex_init(&mux1, NULL);
  pthread_mutex_init(&mux2, NULL);

  pthread_cond_init(&attesaPostoLibero, NULL);

  for (i=0; i<K; i++)
    park[i] = FREE;

  srand(17);

  pthread_barrier_init(&bar, NULL, NAUTO);

  /////////////////////////////////////////////////////////////////////

  for (t=0; t<NAUTO; t++) {

    targv[t].tid  = t;
    targv[t].park = park;
    targv[t].tret = (void*) &tret[t];   

    r = pthread_create(&thauto[t], NULL, autoFunc, (void*) &targv[t]);

    if ( r != 0 )
      perror("ERROR creation of thread FAILED");
      // aggiungere gestione errore
  }

  for (t=0; t<NAUTO; t++) {
    //fprintf(stderr, "waiting for thread %d ...\n", t);
    r = pthread_join(thauto[t], (void*)&tret[t]);
    if ( r != 0 ) 
      perror("ERROR thread join FAILED");
      // aggiungere gestione errore
  }

  fprintf(stderr, "simulation ENDED.\n");

  return 0;
}
