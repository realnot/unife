#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define N 5

#define LEFT(i)  (i+N-1)%N
#define RIGHT(i) (i+1)%N

#define THINKING 0
#define HUNGRY 1
#define EATING 2
 
char *names [N] = {
  "1. Socrates",
  "2. Plato",
  "3. Immanuel Kant",
  "4. Friedrich Hegel",
  "5. Karl Marx"
};
 
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t s[N] = {
    PTHREAD_MUTEX_INITIALIZER,
    PTHREAD_MUTEX_INITIALIZER,
    PTHREAD_MUTEX_INITIALIZER,
    PTHREAD_MUTEX_INITIALIZER,
    PTHREAD_MUTEX_INITIALIZER
  };

pthread_t philos[N];

int state[N];

int philid[N] = {0, 1, 2, 3, 4};

///////////////////////////////////////////////////////////////////////

void think (int pid) {
  int j;
  j = 1 + rand()%N; 
  printf("%s thinks\n", names[pid]);
  usleep(j);
}

///////////////////////////////////////////////////////////////////////

void eat (int pid) {
  int j;
  j = 2 + rand()%N;
  printf ("%s eats \n", names[pid]);
  usleep(j);
}

///////////////////////////////////////////////////////////////////////

void test ( int pid ) {
  if ( (state[pid] == HUNGRY) && 
       (state[LEFT(pid)] != EATING) && (state[RIGHT(pid)] != EATING) ) {
    state[pid] = EATING;
    pthread_mutex_unlock(&s[pid]);
  }
}

///////////////////////////////////////////////////////////////////////

void take_forks ( int pid ) {
  pthread_mutex_lock(&mutex);
  state[pid] = HUNGRY;
  test(pid);
  pthread_mutex_unlock(&mutex);
  pthread_mutex_lock(&s[pid]);
}

///////////////////////////////////////////////////////////////////////

void put_forks ( int pid ) {
  pthread_mutex_lock(&mutex);
  state[pid] = THINKING;
  test(LEFT(pid));
  test(RIGHT(pid));
  pthread_mutex_unlock(&mutex);
}

///////////////////////////////////////////////////////////////////////

void *philosopher (void *arg) {
  int pid;

  pid = *((int *)arg);
  
  while(1) {    
    think(pid);
    take_forks(pid);
    eat(pid);
    put_forks(pid);          
  }
}
   
///////////////////////////////////////////////////////////////////////

int main (int argc, char* argv[]) {
  int rc, i;
  int *arg;

  for (i = 0; i<N; i++) {
    arg = &philid[i];
    
    rc = pthread_create(&philos[i], NULL, philosopher, arg);
    
    if (rc) {
      printf("ERROR; return code from pthread_create() is %d\n",rc);
      exit(EXIT_FAILURE);
    }
  }
  
  printf("Thread creation finished\n");

  for (i=0; i<N; i++) {
    pthread_join(philos[i], NULL);
  } 

  exit(EXIT_SUCCESS);
}
