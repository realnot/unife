////////////////////////////////////////////////////////////////////////
// Esempio di produttore consumatore con buffer circolare. L'accesso alle 
// regioni critiche e' limitato soltanto al tempo necessario ad aggiornare 
// le variabili condivise. L'accesso al buffer avviene fuori dalle regioni 
// critiche, permettendo in caso di accessi lunghi un overlap temporale tra 
// accessi a locazioni diverse dello stesso.
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define OVER (-1)
#define max 2000
#define BUFFER_SIZE 10

typedef struct {
  int buffer[BUFFER_SIZE];
  pthread_mutex_t M;
  int readpos, writepos;
  int cont;
  pthread_cond_t PIENO;
  pthread_cond_t VUOTO;
} prodcons;


prodcons B;

////////////////////////////////////////////////////////////////////////

void init (prodcons * b) {
  pthread_mutex_init(&b->M,    NULL);
  pthread_cond_init(&b->PIENO, NULL);
  pthread_cond_init(&b->VUOTO, NULL);
  b->cont     = 0;
  b->readpos  = 0;
  b->writepos = 0;
}

////////////////////////////////////////////////////////////////////////

void inserisci (prodcons *b, int MSG) {
  int mywritepos;
  
  pthread_mutex_lock(&b->M);
  
  while ( b->cont == BUFFER_SIZE ) {       // buffer vpieno
    fprintf(stderr, "buffer PIENO!!!\n");
    pthread_cond_wait(&b->PIENO, &b->M);
  }
  
  mywritepos = b->writepos;  
  b->writepos++;
  
  pthread_mutex_unlock(&b->M);
    
  // scrivi MSG (si suppone essere un'operazione molto lunga
  b->buffer[mywritepos] = MSG;
  // simulazione del tempo di scrittura
  usleep(117+(rand()%1103));
  
  pthread_mutex_lock(&b->M);
  
  // incrementa numero di elementi
  b->cont++;
  
  // gestione circolare
  if ( b->writepos >= BUFFER_SIZE )
    b->writepos = 0;
    
  // risveglia un eventuale thread consumatore sospeso
  pthread_cond_signal(&b->VUOTO);
  
  pthread_mutex_unlock(&b->M);
}

////////////////////////////////////////////////////////////////////////

int estrai (prodcons *b) {
  int MSG;
  int myreadpos;
  
  pthread_mutex_lock(&b->M);
  
  while (b->cont == 0) { // buffer vuoto
    fprintf(stderr, "buffer VUOTO\n");
    pthread_cond_wait(&b->VUOTO, &b->M);
  }
  
  myreadpos = b->readpos;
  
  b->readpos++;
 
  pthread_mutex_unlock(&b->M);
  
  // leggi il messaggio (si suppone essere un'operazione molto lunga)
  MSG = b->buffer[myreadpos];
  // simulazione del tempo di scrittura
  usleep(1770+(rand()%2751));
  
  pthread_mutex_lock(&b->M);

  b->cont--;
    
  // gestione circolare
  if ( b->readpos >= BUFFER_SIZE )
    b->readpos = 0;
    
  // risveglia eventuale thread produttore
  pthread_cond_signal(&b->PIENO);
  
  pthread_mutex_unlock(&b->M);
  
  return MSG;
}

////////////////////////////////////////////////////////////////////////

void * produttore (void * arg){
  int n;
  for (n=0; n < max; n++) {
    printf("Thread  produttore MSG%d --> \n", n);
    inserisci(&B,n);
  }
  inserisci(&B, OVER);
  return NULL;
}

////////////////////////////////////////////////////////////////////////

void * consumatore (void * arg){
  int d;
  while (1) {
    d = estrai(&B);
    if (d==OVER)
      break;
    printf("Thread consumatore --> MSG%d\n", d);
  }
  return NULL;
}
////////////////////////////////////////////////////////////////////////

int main() {
  pthread_t th_a, th_b;
  
  srand(17);
  
  init(&B);
  
  // Creazione Threads
  if ( pthread_create(&th_a, NULL, produttore,  0) != 0 ) {
    fprintf(stderr, "ERROR: failed to create thread\n");
    return -1; 
  }
  
  if ( pthread_create(&th_b, NULL, consumatore, 0) != 0 ) {
    fprintf(stderr, "ERROR: failed to create thread\n");
    return -1; 
  }

  // Attesa terminazione threads
  pthread_join(th_a, NULL);
  pthread_join(th_b, NULL);
  
  return 0;
}
