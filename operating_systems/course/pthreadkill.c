#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <error.h>
#include <unistd.h>

#define NUMTHREADS 3

void sighand(int signo) {
  fprintf(stderr, "[Thread.%p] in signal handler ... \n", (void*)pthread_self());  
  return;
}

////////////////////////////////////////////////////////////////////////

void * threadfunc(void *parm) {
  pthread_t self;
  int rc;

  self = pthread_self();
  
  fprintf(stderr, "[Thread.%p]: start and going to sleep ....\n", (void*)self);
  
  rc = sleep(10);
  
  if ( rc != 0 ) {
    fprintf(stderr, "[Thread.%p]: got a signal waiking me up after %d sec.\n", 
	(void*)self, 10-rc);
    return NULL;
  }
  
  fprintf(stderr, "[Thread.%p]: did not get any signal\n", (void*)self);
  
  return NULL;
}

////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  int       rc;
  int       i;
  pthread_t threads[NUMTHREADS];
  
  if ( signal(SIGALRM, sighand) == SIG_ERR ) 
    perror("signal failed");
  
  fprintf(stderr, "[Thread.%p]: main started \n", (void*)pthread_self());  

  for(i=0; i<NUMTHREADS; ++i) {
    rc = pthread_create(&threads[i], NULL, threadfunc, NULL);
  }

  sleep(1);
  
  for(i=0; i<NUMTHREADS; ++i) {
    if ( i != 2 ) {
      fprintf(stderr, "[Thread.%p]: sending kill to thread: %p\n", 
	  (void*)pthread_self(), (void*)threads[i]);
      rc = pthread_kill(threads[i], SIGALRM);
    }
  }
      
  for(i=0; i<NUMTHREADS; ++i) {
    rc = pthread_join(threads[i], NULL);
  }
      
  fprintf(stderr, "[Thread.%p]: main completed \n", (void*)pthread_self());  
  
  return 0;
}
