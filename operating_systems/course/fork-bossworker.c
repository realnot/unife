#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>     /* fork, getpid, getppid */
#include <sys/types.h>
#include <sys/wait.h>

#define N 2

///////////////////////////////////////////////////////////
// Questo programma genera una gerachia di tipo boss-workers 
// ovvero,  un processo e N figli. Il porcesso padre legge 
// da standard input un carattere e lo passa ad uno dei 
// figli mediante una pipe.
//////////////////////////////////////////////////////////

char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

int go = 1;

void sighand (int sig) {
  if ( sig == SIGINT ) {
    printf("CTRL-C received\n");
    go = 0;
  }
  return;
}

int main ( int argc, char * argv[] ) {
  
  int fd[N][2];
  int childpid[N];
  int pidx;
  int i, isFather, k;
  char c;
  
  if ( signal(SIGINT,  sighand) == SIG_ERR )
    perror("signal failed\n");
    
  pidx = 0;
  isFather = 1;
  
  while (( isFather == 1 ) && ( pidx < N )) { 
    pipe(fd[pidx]);
    childpid[pidx] = fork();
    
    if ( childpid[pidx] == 0 ) {
      isFather = 0;
      //printf("P%d: closing write side of pipe %d\n", getpid(), pidx);
      close(fd[pidx][1]); // closing the write side
    } else {
      close(fd[pidx][0]); // closing the read side
      pidx++;
    }   
  }
  
  if ( isFather == 1 ) {
    pidx = 0;
    k    = 1;
    while ( k < 10  ) {
      fprintf(stderr, "P%d: writing %c on pipe[%d]\n", getpid(), digits[k], pidx);
      write (fd[pidx][1], &digits[k], sizeof(char));
      pidx = ( pidx + 1 ) % N;
      k++;
    }
    c = 'x';
    
    for(pidx=0; pidx < N; pidx++)
      write (fd[pidx][1], &c, sizeof(char));
    
    fprintf(stderr, "Father exit\n");
    
    for(i=0; i < N; i++)
      wait(NULL);
    
    for(i=0; i < N; i++)
      close(fd[i][1]); // closing the write side
    
  } else {
    
    // child code
    fprintf(stderr, "P%d: my pidx is %d\n", getpid(), pidx);
    while ( c != 'x' ) {
      read(fd[pidx][0], &c, sizeof(char));
      fprintf(stderr, "P%d: my father gives me the char: %c \n", getpid(), c);
    }
    fprintf(stderr, "P%d: exit\n", getpid());
    close(fd[pidx][0]); // closing the read side
  }
  
  exit(0);
  
}

