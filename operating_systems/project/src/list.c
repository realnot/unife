#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

/*
 * the function accept an input parameter (the value of the node) and then 
 * creates a node to store the value. A node is created by allocating memory 
 * to a structure. Once the node is created, we can assign the value "" and 
 * its "next" pointer is assigned the address of next node. If no next node 
 * exists (that's mean the last node) is assigned to NULL values.
 */
list_t * create_list() {
	
	list_t * list = malloc(sizeof(list_t));
	
	if (NULL == list) {
		fprintf(stderr, "List creation failed!\n");
		exit(EXIT_FAILURE);
	} else {
		list->head = malloc (sizeof(node_t));
		if (NULL == list->head) {
			fprintf(stderr, "Creation of first node failed!\n");
			exit(EXIT_FAILURE);
		}
		list->count = 0;
		list->tail = list->head;
	}
	
	return list;
}

/*
 * The function accept a data_t value and checks if the linked list is been
 * initialized. Then reserve the memory for a new node, the tail points to the
 * memory location just reserved. Subsequently the the data "data" is stored in
 * the node and the tail, which now points to the last node, will have the 
 * reference to the next node set to null. 
 */
int insert_list (list_t * list, data_t data) {
	
	if ( ! empty_list(list)) {
		list->head->data = data;
		list->count++;
	} else {
		list->tail->next = malloc (sizeof(node_t));
		if (NULL == list->tail) {
			fprintf(stderr, "Node creation failed!\n");
			exit(EXIT_FAILURE);
		} else {
		    // save the predecessor
			list->tail->next->prev = list->tail;
			// move one position forward
			list->tail = list->tail->next;
			// store the value
			list->tail->data = data;
			list->tail->next = NULL;
			list->count++;
		}
	}

	return 0;
}

/* 
 * The search start with the first node (the head of the list) and then compare 
 * the value which is being searched with the value contained in this node. 
 * If the value does't match then through the "next" pointer (which contains
 * the address of next node) the next node is accessed and same value 
 * comparison is done there. The search goes on until last node is accessed 
 * or node is found whose value is equal to the value being searched. 
 * The function returns NULL if no value is found, otherwise it returns the 
 * pointer to the node that contains the value.
 */
node_t * search_list(list_t * list, data_t data) {

	node_t * p_node = list->head;
	while (NULL != p_node) {
		if ( ! strcmp(p_node->data.uname, data.uname)) return p_node;
		else p_node = p_node->next;
	}
	
	fprintf(stderr, "Node not found!\n");
	return NULL;
}

/*
 * A node is deleted by first finding it in the linked list (using the search 
 * function) and then calling free() on the pointer containing its address.
 *
 * When we try to delete a node from a doubly list, there may be four cases:
 * - 1. The searched value refers to a node in the middle of the list. 
 *   Generally this is most often the case and is the first in the if-elseif 
 *   statement.
 * - 2. The searched value refers to the last node in the list (the tail).
 * - 3. The searched value refers to the first node in the list (the head).
 * - 4. The search value is the only node in the list.
 */
int delete_list(list_t * list, data_t data) {
    
    node_t * p_del = NULL;
    
    p_del = search_list(list, data);
    if (NULL == p_del) {
    	fprintf(stderr, "The searched value doesn\'t exist!\n");
    	return -1;
    } else {
        // deleting the only node in the list
        if (p_del == list->head && p_del->next == NULL) {
	        list->head = NULL;
        // deleting a common node
        } else if (NULL != p_del->prev && NULL != p_del->next) {
	        p_del->next->prev = p_del->prev;
	        p_del->prev->next = p_del->next;
	        p_del->next = NULL;
	        p_del->prev = NULL;
        // deleting the last node in the list (the tail)
        } else if (p_del == list->tail) {
	        p_del->prev->next = NULL;
	        p_del->prev = NULL;
        // deleting the first node in the list (the head)
        } else if (p_del == list->head) {
	        p_del->next->prev = NULL;
	        list->head = p_del->next;
	        p_del->next = NULL;
        }
	}
	
    free(p_del); 
    return 0;
}

/*
 * 
 */
int empty_list(list_t * list) {
	
	return (list->count == 0) ? 0 : -1;
}

/*
 *
 */
void print_list(list_t * list) {

	size_t count = 0;
	node_t * p_list = list->head;

    while(NULL != p_list) {
        printf(" |   |--[%03zu]-> %s:", count, p_list->data.uname);
        printf("%s:", p_list->data.name);
        printf("%d:", p_list->data.sockid);
        printf("%s", p_list->data.email);
        
        p_list = p_list->next;
        
        count++;
    }
    printf(" |\n");
}
