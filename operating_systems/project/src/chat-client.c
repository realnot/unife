#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <pthread.h>
#include <curses.h>

#include "config.h"

#define PORT 1111
#define HOST "127.0.0.1"
#define PROG "chat-client"
#define PACKAGE "chat-Fe"
#define VERSION "22.062013"

// prototypes
int option_parser(int, char **, msg_t *);
void * reader_thread(void *);
void * writer_thread(void *);
int reglog_handler (msg_t *);
void shutdown_sck(int);
void print_help(int);

// Globals
int go = 1;
int * p_csd;

/*
 *
 */
int main (int argc, char ** argv) {
        
    int attempts;
    pthread_t reader_tid;
    pthread_t writer_tid;   
    p_csd = malloc(sizeof(int));  
    msg_t * p_msg = malloc(sizeof(msg_t));
    p_msg->msg = malloc(1024); 
    struct sockaddr_in sa;

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(PORT);
    sa.sin_addr.s_addr = inet_addr(HOST);
    
    // Parse command-line options
    if (option_parser(argc, argv, p_msg)) {
        fprintf(stderr, "Failed to parse command-line options. ");
        fprintf(stderr, "Cannot continue, exiting now!\n");
        shutdown_sck(-1);
    }
    
    // Create client socket descriptor    
    *p_csd = socket (AF_INET, SOCK_STREAM, 0 );
    if ( -1 == *p_csd) {
        perror("[#] Error while creating socket");
        shutdown_sck(-1);
    }
    
    // Connection request
    if ( -1 == connect(*p_csd, (struct sockaddr *)&sa, sizeof sa)) {
        perror("[#] Error on connect"); shutdown_sck(-1);
    } else 
        printf("Connection established\n");
    
    attempts = 1;
    printf("[#] attempts: %d\n", attempts);
    while ( 10 > attempts ) {
        // Registration/Login request
        if ( FALSE == reglog_handler(p_msg)) {
            attempts++; sleep(1);
            printf("[#] attempts: %d\n", attempts);
            DEBUG_PRINT("%s", "Here\n");
            free(p_msg->msg); free(p_msg);
            DEBUG_PRINT("%s", "Here\n");
            p_msg = malloc(sizeof(msg_t));
            p_msg->msg = malloc(1024);
            
            printf("server: please, provide the correct username!\n");
            if ( -1 == io_read_string(p_msg->msg, 1024)) {
                printf("Error on read input\n");
            }
        } else 
            break;
    }
    
    DEBUG_PRINT("%s", "Here\n");
    
    // Shutdown connection after 10 attempts
    if ( 10 == attempts) {
        // Shutdown connection
        printf("Connection lost. Max number of attempts reached\n");
        shutdown_sck(-1);
    } else {
        if ( 0 != pthread_create(&writer_tid, NULL, writer_thread, 0)) {
            fprintf(stderr, "[#] ERROR: failed to create thread\n");
            shutdown_sck(-1);
        }
        if ( 0 != pthread_create(&reader_tid, NULL, reader_thread, 0)) {
            fprintf(stderr, "[#] ERROR: failed to create thread\n");
            shutdown_sck(-1);
        }
	    pthread_join(writer_tid, NULL);
	    pthread_join(reader_tid, NULL);
    }
    
    return 0;
}

/*
 * The Reader
 *
 * The thread once running takes care of reading the messages from the server 
 * and write to STDOUT. The following code should be clear.
 *
 * Return value: 
 *  The function doesn't return any value. The thread remains in execution 
 *  until the global variable "go" isn't set to 0, then the socket ends by 
 *  the procedure shutdown_sck().
 */
void * reader_thread (void * args) {
    
    #ifdef DEBUG
        DEBUG_PRINT("reader_thread() %zu reached!\n", pthread_self());
    #endif
    
    msg_t * p_msg;
    char * time = malloc (128);

    while ( go ) {
        // Reserve space for the struct
        p_msg = malloc(sizeof(msg_t));
        // Read the msg_t struct
        if ( -1 != read (*p_csd, p_msg, sizeof(msg_t))) {
            // Get the msglen and reserve the space
            p_msg->msg = malloc(p_msg->msglen);
            // Read the msg
            if ( -1 != read (*p_csd, p_msg->msg, p_msg->msglen)) {
                if ( p_msg->type == MSG_LOGOUT ) 
                    go = 0;
                
                memset(time, '0', 128);
                set_timestamp(time, TIME_F);
                if (MSG_SINGLE == p_msg->type) {
                    printf("[%s] %s:%s:", time, p_msg->sender, p_msg->receiver);
                    printf("%s\n", p_msg->msg);
                    fflush(stdout);
                } else if (MSG_BRDCAST == p_msg->type) {
                    printf("[%s] %s:*: %s\n", time, p_msg->sender, p_msg->msg);
                    fflush(stdout);
                } else if (MSG_ERROR == p_msg->type) {
                    printf("[%s] %s %s\n", time, FROM_SERVER, p_msg->msg);
                    fflush(stdout);
                } else {
                    printf("%s\n", p_msg->msg);
                    fflush(stdout);
                }
                
                free(p_msg->msg); 
                free(p_msg);
                
            } else {
                perror("[#] Error on read"); 
                shutdown_sck(-1); 
            }
        } else { 
            perror("[#] Error on read"); 
            shutdown_sck(-1); 
        }   
    }
    
    free(time);
    // Shutdown connection
    shutdown_sck(0); 
    // Never reached
    return (void *) 0;
}

/*
 * The Writer
 *
 * This thread once running takes care of reading the messages from STDIN and 
 * send them to the server. The code should be clear and it's explained below.
 *
 * Return value: 
 *  The function doesn't return any value. The thread remains in execution 
 *  until the global variable "go" isn't set to 0, then the socket ends by 
 *  the procedure shutdown_sck().
 */
void * writer_thread (void * args) {

    #ifdef DEBUG
        DEBUG_PRINT("writer_thread() %zu reached!\n", pthread_self());
    #endif

    msg_t * p_msg;

    while ( go ) {
        // Reserve space for the msg_t struct and the message
        p_msg = malloc(sizeof(msg_t)); 
        p_msg->msg = malloc(1024);
        // Read a string from console
        if ( -1 != io_read_string(p_msg->msg, 1024)) {
            p_msg->msglen = strlen(p_msg->msg) + 1;
            if (-1 == write (*p_csd, p_msg, sizeof(msg_t)) ||
                -1 == write (*p_csd, p_msg->msg, p_msg->msglen)) {
                perror("[#] Error on write"); shutdown_sck(-1);
            } else { 
                free(p_msg->msg); free(p_msg);
            }
        } else {    
            fprintf(stderr, "[#] Error on read string\n");
            shutdown_sck(-1); 
        }
    }
    // Shutdown connection
    shutdown_sck(0);
    // Never reached
    return (void *) 0;
}

/* 
 * Registration/Login handler
 *
 * The registration/login handler writes the message (passed as an argument to 
 * the function) on fd (p_csd). The procedure is implemented in such a way that 
 * the first system call write(),  sends the msg_t struct, which contains, 
 * among other things, the field "msglen" (the length of the message), and the 
 * field "type" (message type), while the second call to the system call 
 * "write()" writes the message.
 *
 * Once you submit your registration request (contained in the msg field of the 
 * structure msg_t) we must read the server's response to the request referred 
 * just forwarded. To do so, we use two system call read(). The steps are as 
 * follows:
 * 
 *   1) We reserve the space for receiving the struct msg_t.
 *   2) We read the struct "p_msg" via the first system call read().
 *   3) We recover the length of the message and we allocate the right amount 
 *      of space.
 *   4) We read the message (the field msg).
 *   5) We check if the user is'"LOGGED"
 *
 * Return value: 
 *   the function returns "false" if the login is unsuccessful, otherwise it 
 *   returns "true".
 */
int reglog_handler (msg_t * p_msg) {

    int logged = FALSE;
    
    p_msg->type = MSG_LOGIN;
    // send a login/registration request 
    p_msg->msglen = strlen(p_msg->msg) + 1;
    // Write the struct msg_t (with msglen)
    if ( -1 != write(*p_csd, p_msg, sizeof(msg_t))) {
        // Write the msg field
        if ( -1 != write(*p_csd, p_msg->msg, p_msg->msglen)) {
            free(p_msg->msg); 
            free(p_msg);
        } else { 
            perror("[#]  Error on write"); 
            shutdown_sck(-1); 
        }
    } else { 
        perror("[#]  Error on write"); 
        shutdown_sck(-1); 
    }
    
    // read the answer from server
    p_msg = malloc(sizeof(msg_t));
    // read the msg_t struct (with the msglen field)
    if ( -1 != read(*p_csd, p_msg, sizeof(msg_t))) {
        // reserve space for the msg (by msglen)
        p_msg->msg = malloc(p_msg->msglen);
        // read the "real" message
        if ( -1 != read(*p_csd, p_msg->msg, p_msg->msglen)) {
            if ( LOGGED == p_msg->type) logged = TRUE;
            puts(p_msg->msg);
        } else { 
            perror("[#] Error on read"); 
            shutdown_sck(-1); 
        }
    } else { 
        perror("[#] Error on read"); 
        shutdown_sck(-1); 
    }
        
    return logged;
}

/*
 * The option parser
 *
 * The function parses the parameters passed from the command line and run 
 * their own procedures.
 *
 * Return value:
 *   0 on success
 *  -1 on failure
 *
 * Please, see http://www.gnu.org/software/libc/manual/html_node/Getopt.html
 * for further informations. (thanks to Frodo Looijaard)
 */
int option_parser(int argc, char ** argv, msg_t * p_msg) {
    
    #ifdef VERBOSE
        printf("%s:%d: option_parser() reached\n",__FILE__,__LINE__);
    #endif
    
    int ii, opt;

    if (argc < 2) {
        fprintf(stderr, "This program needs arguments...\n\n");
        print_help(1);
    }
    
    opterr = 0;
    
    while ( -1 != (opt = getopt (argc, argv, "hl:r:"))) {
        switch (opt) {
            case 'h': 
                print_help(0); 
                break;
            case 'l':
                //printf("optarg: %s\n", optarg);
                strncpy (p_msg->msg, optarg, 1 + strlen(optarg));
                p_msg->type = MSG_LOGIN;
                break;
            case 'r':
                //printf("optarg: %s\n", optarg);
                strncpy (p_msg->msg, optarg, 1 + strlen(optarg));
                p_msg->type = MSG_REGLOG;
                break;
            case '?':
                if (optopt == 'l' || optopt == 'r')
                    fprintf(stderr,"Option -%c requires an argument.\n",optopt);
                else if (isprint (optopt))
                    fprintf(stderr,"Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,"Unknown option chr `\\x%x'.\n", optopt);
                return -1;
            default:
                abort ();
        }

        for (ii = optind; ii < argc; ii++)
            printf ("Non-option argument %s\n", argv[ii]);
    }
    
    return 0;
}

/*
 * The signals handler
 *
 * The function handles some standard signals
 *
 * Return value:
 *  None.
 */
void sig_handler (int signo) {

    // Interrupt
    if (signo == SIGINT) {
        printf("received signal SIGINT (ctrl+c)\n");
        shutdown_sck(0);
    }
    /* Signal sent to parent process whenever one of its child 
    processes terminates or stops. */
    else if (signo == SIGCHLD) {
        printf("received signal SIGCHLD\n");
    }
}

/* 
 * The shutdown
 *
 * The function terminates the socket
 *
 * Return value:
 *  None
 */
void shutdown_sck (int exit_value) {
    
    go = 0;
    
    printf("Initializing shutdown sequence...");
    if ( -1 == shutdown (*p_csd, SHUT_RDWR))
        perror("[#] Can not shutdown socket");
    
    // end connection
    //free(p_msg->msg); 
    //free(p_msg);
    free(p_csd);
    
    if (-1 == exit_value)
        fprintf(stderr, "An error occurred!!!\n");
    else
        printf("Done\n");
    printf("Exiting now...\n");
    
    -1 == exit_value ? exit(EXIT_FAILURE) : exit(EXIT_SUCCESS);
}

/*
 * The helper
 *
 * Shows the functions provided by the client
 *
 * Return value:
 *  None
 */
void print_help (int exit_value) {

    printf("%s %s %s\n", PACKAGE, PROG, VERSION); 
    
    printf("%s [-h] [-l username] [-r username:fullname:email]\n\n", PROG);
    printf("  -h    print this help and exit\n");
    printf("  -l    send a login request\n");
    printf("  -r    send a registration request\n\n");
    
    exit(exit_value);
}
