#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <curses.h>
#include <string.h>

#include "config.h"

WINDOW * top;
WINDOW * bottom;

int size_x, size_y;
int line=1; // line position of top window
int input=1; // line position of bottom

int go = 1;
int msgready;

char * buffer;

pthread_mutex_t buf_mux;
pthread_cond_t buf_full;
pthread_cond_t buf_empty;

/*
 *
 *
 */
void * interface () {

  pthread_t threads[2];
  
  pthread_mutex_init(&buf_mux, NULL);
  pthread_cond_init(&buf_full, NULL);
  pthread_cond_init(&buf_empty, NULL);
  
  msgready = 0;
  
  // Set up windows 
  initscr();  
  
  // get screen dimension
  getmaxyx(stdscr, size_y, size_x);

  buffer = malloc(size_x);
  
  top    = newwin(size_y/2, size_x, 0,      0);
  bottom = newwin(size_y/2, size_x, size_y/2, 0);

  scrollok(top, TRUE);
  scrollok(bottom, TRUE);
  
  // draw a box around the edges of a window.
  box(top,    ' ', '-');
  box(bottom, ' ', '-');

  wsetscrreg(top,   1, size_y/2-2);
  wsetscrreg(bottom,1, size_y/2-2);

  // Spawn the listen/receive threads
  pthread_create(&threads[0], NULL, sendmessage, NULL);
  pthread_create(&threads[1], NULL, listener,    NULL);

  // Keep alive until finish condition is done
  while(go);  

  exit(0);
}

/*
 *
 *
 */
void * listener() {
  
    while (go) {
    
        wrefresh(top);

        pthread_mutex_lock(&buf_mux);

        while ( msgready == 0 ) 
            pthread_cond_wait(&buf_full, &buf_mux);

        // Print on own terminal
        mvwprintw(top, line, 1, buffer);

        msgready = 0;

        // scroll the top if the line number exceed height   
        if ( line != size_y/2-2 )
            line++;
        else
            scroll(top);

        pthread_cond_signal(&buf_empty);
        pthread_mutex_unlock(&buf_mux);
    }
    
    pthread_exit(NULL);
}

/*
 *
 *
 */
void * sendmessage () {

    char str[size_x];

    while(go) {

        bzero(str,size_x);

        wrefresh(bottom);

        // reads message from bottom window into string str
        mvwgetstr(bottom, input, 1, str);

        pthread_mutex_lock(&buf_mux);

        while(msgready == 1)
            pthread_cond_wait(&buf_empty, &buf_mux);

        // prepare buffer
        strcpy(buffer, str);

        msgready = 1;

        pthread_cond_signal(&buf_full);
        pthread_mutex_unlock(&buf_mux);

        // scroll the bottom if the line number exceed height
        if ( input != size_y/2-2 )
            input++;
        else
            scroll(bottom);
    }

    // Clean up
    endwin();  

    pthread_exit(NULL);
}
