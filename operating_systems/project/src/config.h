/* 
    config.h : chatFe 
*/

// #define VERBOSE

#define DEBUG

// time format
enum { UNIX_F, TIME_F, DATE_F, HUMS_F };


#define NO_INPUT_1   -1
#define NO_INPUT_2   -2

#define TOO_LONG     -3
#define INVALID_ARG  -4

// define server name & client name
#define FROM_SERVER "server:"
#define FROM_CLIENT "client: "
// define server & client prompt

#define UNDEFINIED 'U'
#define LOGGED 'C'

#define FALSE 0
#define TRUE 1

#define NOT_LOGGED 'N'

#define NOT_VALID -1
#define NOT_CONNECTED -2  
#define NOT_REGISTERED -3
 
#define MSG_LOGIN 'L'
#define MSG_REGLOG 'R'
#define MSG_OK 'O'
#define MSG_ERROR 'E'
#define MSG_SINGLE 'S'
#define MSG_BRDCAST 'B'
#define MSG_LIST 'I'
#define MSG_LOGOUT 'X'

// define the client, socket and command buff size
#define MSD_BUF_SIZE 1024
#define CSD_BUF_SIZE 1024
#define CMD_BUF_SIZE 1024
#define _MSGS_BUF_SIZE_ 10
// defines the size of a generic buffer
#define BUF_SIZE 256

// define the string length
#define SL 256
// define the hash length
#define HL 997

/* defines the max size of the path accepted by the user. 2048 mean the number 
 * char of the string and mean also the number of bytes + the null char */
#define MAX_PATH_SIZE 2048

// check path size
#define CHECK_SIZE(x) ((x) > MAX_PATH_SIZE ? 0 : -1)

/* debugging macros so we can pin down message origin at a glance 
thanks to http://en.wikipedia.org/wiki/C_preprocessor */
#define WHERESTR  "[###][ %s ][ %d ]: "
#define WHEREARG  __FILE__, __LINE__
#define DEBUGPRINT2(...)       fprintf(stderr, __VA_ARGS__)
#define DEBUG_PRINT(_fmt, ...)  DEBUGPRINT2(WHERESTR _fmt, WHEREARG, __VA_ARGS__)

// message pattern
#define MSG_PATTERN "[A-Za-z0-9!#-_]+"
// login pattern
#define LOGIN "^[A-Za-z0-9_-]{3,255}$"
// Username pattern
#define UNAME_PATTERN "^[A-Za-z0-9_-]{3,255}$"
// Fullname pattern
#define NAME_PATTERN "^[A-Za-z0-9]{3,127}\\s[A-Za-z0-9]{3,127}$"
// Email pattern
#define EMAIL_PATTERN "\
^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*@([a-z0-9])\
(([a-z0-9-])*([a-z0-9]))+(.([a-z0-9])([-a-z0-9_-])?\
([a-z0-9])+)+$"
// Registration pattern
#define REGISTRATION "\
[A-Za-z0-9]{3,256}[:][A-Za-z0-9]{2,128}[ ][A-Za-z0-9]{2,128}[:]([a-z0-9])\
(([-a-z0-9._])*([a-z0-9]))*@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+(.([a-z0-9])\
([-a-z0-9_-])?([a-z0-9])+)+$"
// Command pattern
#define CMD_PATTERN "^#[a-z]{2,64}(( [a-z]+:[a-z]+)|( :[a-z]+))?"    

// the alias
typedef struct data_t data_t;
typedef struct log_t log_t;
typedef struct msg_t msg_t;
typedef struct cmd_t cmd_t;
typedef struct node_t node_t;
typedef struct list_t list_t;
typedef struct hash_t hash_t;
typedef struct stats_t stats_t;
typedef struct depot_t depot_t;
typedef struct socket_t socket_t;

struct data_t {
	char uname[256];
	char name[256];
	char email[256];
	int  sockid;
};

struct log_t {
	char time[9];
	char logtype[6];
	char uname[256];
};

struct msg_t {
	char type;
	char sender[256];
	char receiver[256];
	char * msg;
	size_t msglen;
};

struct cmd_t {
    int type;
    char cmd[64];
    char dest[256];
    char text[1024];
};

struct node_t {
    data_t data;
    node_t * next;
    node_t * prev;
};

struct list_t {
	size_t count;
 	node_t * head;
	node_t * tail;
};

struct hash_t {
    size_t count;
	list_t * bucket;
};

struct depot_t {
    msg_t msgdep[_MSGS_BUF_SIZE_];
    size_t count;
    size_t read_pos;
    size_t write_pos;
    pthread_mutex_t mux;
    pthread_cond_t full;
    pthread_cond_t empty;
};

// Respectively: number of elements, size in bytes, the elements
struct socket_t {
    size_t len;
    size_t size;
    int * p_sockid;
};

// globals
char * p_user_file;
char * p_log_file;
hash_t * Table;
list_t * p_user_online; 
depot_t * p_depot;
                      
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * I know, this is not the correct way to work, but this is a small project, *
 * so i decided to move all prototypes here to keep a easy maintenance 		 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 * 	requests.c prototypes
 */
int requests_handler(int *, msg_t *, data_t *);
int requests_parser(void);
int get_registration(void);
int get_login(void);
int get_logout(void);
int get_list(void);
int add_msg (void);

/*
 * 	user.c prototypes
 */
int is_connected(data_t);
int is_registered(data_t);
int get_sockid(socket_t *, msg_t *);
int get_single_sockid(socket_t *, data_t);
int get_multiple_sockids(socket_t *, data_t);

/*
 * 	tools.c prototypes
 */
int is_file(char *);
int is_directory(char *);
void set_timestamp(char *, size_t);
int tools_path_checker(char *);
int tools_create_path(char *);
int tools_regex_checker(char *, const char *);
int tools_user_tokenizer(data_t *, msg_t *);
int tools_cmd_tokenizer(cmd_t *, msg_t *);

/*
 *	io.c prototypes
 */
char io_read_char(void);
int io_read_string(char *, size_t);
int io_check_user_answer_str(void);
int io_write_cmd_t(char *, char *);
int io_write_header_t(char *);
int io_write_data_t(char *, data_t *);
int io_read_data_t(char *);
// size_t io_read (int, void *, size_t);
// size_t io_write (int, const void *, size_t);

/*
 *	list.c prototypes
 */
list_t * create_list();
int insert_list (list_t *, data_t);
node_t * search_list(list_t *, data_t);
int delete_list(list_t *, data_t);
int empty_list(list_t *);
void print_list(list_t *);

/*
 *  hash.c prototypes
 */
int hashfunc(char *);
hash_t * create_hashtable();
node_t * search_hash(hash_t *, char *);
int insert_hash(hash_t *, data_t, char *);
void print_hashtable(hash_t *);

/*
 * test.c prototypes
 */
int get_registration_test();
int get_login_test();
int get_logout_test();
int get_list_test();
