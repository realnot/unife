#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

/*
 * Prime number sexy (991, 997): 997-991 = sex = 6
 * http://en.wikipedia.org/wiki/Sexy_prime
 *
 * Method adopted: Hashing with open addressing as collision resolution with
 * linear probing, in which the interval between probes is fixed (1)
 *
 * h(k,i) = (h'(k) + i) mod m
 *
 * See http://en.wikipedia.org/wiki/Hash_table#Open_addressing
 */
int hashfunc(char * key) {
	
	int ii = 0, hashval = 0;
	
	for ( ; ii < strlen(key); ii++)
		hashval = ((hashval * SL) + key[ii]) % HL;
	
	return hashval;
}

/*
 *
 */
hash_t * create_hashtable () {

	size_t ii;

	// reserve memory for 997 hash_t
	Table = calloc (HL, sizeof(hash_t));

	// creates 997 empty list
	for (ii = 0; ii < HL; ii++) 
		Table[ii].bucket = create_list();
	
	return Table;
}

/*
 *
 */
node_t * search_hash (hash_t * Table, char * key) {

	size_t ii = hashfunc(key);
	node_t * p_node = Table[ii].bucket->head;
	
	while (NULL != p_node) {
		if( ! strcmp(p_node->data.uname, key))
		    return p_node;
		else p_node = p_node->next;
	}
	
	return NULL;
}

/*
 *
 */
int insert_hash (hash_t * Table, data_t data, char * key) {

	size_t ii = hashfunc(key);
	
	if ( insert_list(Table[ii].bucket, data)) {
		fprintf(stderr,"server: Failed to add a node\n");
		exit(EXIT_FAILURE);
	} else {
	    // the number of registered user
	    Table->count++;
		return 0;
	}
}

/*
 *
 */
void print_hashtable (hash_t * Table) {

	size_t ii, count = 0;
	
	printf("\n[#] Credentials of registered users:\n |\n");
    
    for ( ii = 0; ii < HL; ii++) {
   		if(Table[ii].bucket->count != 0) {
    		printf(" |-[%03zu]\n", ii);
    		count += Table[ii].bucket->count;
    		print_list(Table[ii].bucket);
		}
	}
	printf(" | \n[#] We have %zu registered users.\n", count);
}



