#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

// User libraries
#include "config.h"

/*
 *  Checks if the_user is connected
 *
 * Return value:
 *  TRUE if registered
 *  FALSE if not registered
 */
int is_connected (data_t the_user) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n", "is_connected() reached!");
    #endif    
    
    node_t * p_the_user = NULL;
    
    // Get the_user from connected users
    p_the_user = search_list(p_user_online, the_user);
    if (NULL == p_the_user)
        return FALSE;
    else
        return TRUE;
}

/*
 *  Checks if the_user is registered
 *
 * Return value:
 *  TRUE if registered
 *  FALSE if not registered
 */
int is_registered (data_t the_user) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n", "is_registered() reached!");
    #endif    
    
    int is_registered;
    node_t * p_node = NULL;
    pthread_mutex_t mux;
    
    pthread_mutex_init(&mux, NULL);
    pthread_mutex_lock(&mux);
    
    if (NULL != (p_node = search_hash(Table, the_user.uname)))
        is_registered = TRUE;
    else
        is_registered = FALSE;
        
    pthread_mutex_unlock(&mux);
    pthread_mutex_destroy(&mux);
    
    return is_registered;
}

/*
 * Get the sockid from connected users
 *
 * Return value:
 *  0: On success.
 *  NOT_VALID: When a user try to do an invalid operation.
 *  NOT_CONNECTED: When a user try to send a msg to a not connected user.
 *  NOT_REGISTERED: When a user try to send a msg to a not registered user.
 */
int get_sockid(socket_t * p_socket, msg_t * p_msg) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n", "get_sockid() reached!");
    #endif
    
    int ret = 0;
    data_t the_sender;
    data_t the_receiver;
    node_t * p_receiver = NULL;
    const char * not_registered = "The user is not registered\n";
    const char * not_connected = "The user is not connected\n";
    const char * not_valid = "You can\'t send a message to yourself\n";
    
    // Necessary to work with search_list()
    strcpy(the_sender.uname, p_msg->sender);
    strcpy(the_receiver.uname, p_msg->receiver);
    
    /* There's only 1 user online "the sender" or the sender & receiver are 
    the same person. Get the sockid and set the properly error message. */
    if (1 == p_user_online->count ||
        0 == strcmp(p_msg->sender, p_msg->receiver)) {
        // Get the receiver from connected users
        p_receiver = search_list(p_user_online, the_receiver);
        p_socket->p_sockid[0] = p_receiver->data.sockid;
        // Set the properly message error
        p_msg->type = MSG_ERROR;
        p_msg->msglen = strlen(not_valid) + 1;
        p_msg->msg = realloc(p_msg->msg, p_msg->msglen);
        strcpy(p_msg->msg, not_valid);
        return NOT_VALID;
    }
    
    // Get the sockid/s
    if (MSG_SINGLE == p_msg->type)
        ret = get_single_sockid(p_socket, the_receiver);
    else if (MSG_BRDCAST == p_msg->type)
        ret = get_multiple_sockids(p_socket, the_sender);
    
    if (0 != ret) {
    //Get the sockid of the sender to send the error message
    ret = get_single_sockid(p_socket, the_sender);
    p_msg->type = MSG_ERROR;
        // Set the properly message error
        if (NOT_CONNECTED == ret) {
            p_msg->msglen = strlen(not_connected) + 1;
            p_msg->msg = realloc(p_msg->msg, p_msg->msglen);
            strcpy(p_msg->msg, not_connected);
            return NOT_CONNECTED;
        } else if (NOT_REGISTERED == ret) {
            p_msg->msglen = strlen(not_registered) + 1;
            p_msg->msg = realloc(p_msg->msg, p_msg->msglen);
            strcpy(p_msg->msg, not_registered);
            return NOT_REGISTERED;
        }
    }
    return 0;
}

/*
 * Get the sockid of one user (the receiver)
 *
 * Return value:
 *  0 on success
 * -1 on failure
 */
int get_single_sockid(socket_t * p_socket, data_t the_receiver) {
   
    #ifdef DEBUG
        DEBUG_PRINT("%s\n", "get_single_sockid() reached!");
    #endif
    
    node_t * p_receiver = NULL;
    
    // Get the receiver from connected users
    p_receiver = search_list(p_user_online, the_receiver);
    // If Not connected, check that it's registered
    if (NULL == p_receiver) {
        if ( FALSE == is_registered(the_receiver))
            return NOT_REGISTERED;
        else
            return NOT_CONNECTED;
    // Get the sockid
    } else
        p_socket->p_sockid[0] = p_receiver->data.sockid;

    return 0;
}

/*
 * Get the sockids of connected users (the recipients)
 *
 * Return value:
 *  0 on success
 * -1 on failure
 */
int get_multiple_sockids(socket_t * p_socket, data_t the_sender) {
    
    #ifdef DEBUG
        DEBUG_PRINT("%s\n", "get_multiple_sockids() reached!");
    #endif
    
    int ii;
    node_t * p_sender = NULL;
    node_t * p_receiver = NULL;
    node_t * p_current_user = p_user_online->head;
    
    // Get the sender from connected users
    p_sender = search_list(p_user_online, the_sender);
    // If Not connected, check that it's registered
    if (NULL == p_sender) {
        if (FALSE == is_registered(the_sender))
            return NOT_REGISTERED;
        else
            return NOT_CONNECTED;
    }
    
    // For each connected user
    for (ii = 0; NULL != p_current_user; ii++ ) {
        // Get the receiver from connected users
        p_receiver = search_list(p_user_online, p_current_user->data);
        // If Not connected, check that it's registered
        if (NULL == p_receiver) {
            if (FALSE == is_registered(p_current_user->data))
                return NOT_REGISTERED;
            else
                return NOT_CONNECTED;
        }
        // Exclude the sender from connected users
        if (0 != strcmp(p_sender->data.uname, p_receiver->data.uname)) {
            // Get the sockid
            p_socket->p_sockid[ii] = p_receiver->data.sockid;
        } else
            ii--;
            
        p_current_user = p_current_user->next;
    }
    return 0;
}
