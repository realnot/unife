#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#include "res.h"

// Number of Blocks
#define N 128

// Block Size
#define BS 256

// Leading Dimension
#define LD BS * BS

// The master node
#define MASTER 0

/*
 *
 */
int main (int argc, char ** argv) {
	
	size_t ii, jj;
	int my_rank, num_nodes;
	double t_start, t_end;
	double *A, *A_data, *B, *B_data, *C, *C_data;
	double *As, *x, *xs, *b, *bs;
	double *Y, *y, *D, *d, *D_tmp, *d_tmp;
	double *A_cap, *A_tmp, *b_cap, *b_tmp;
	double *xi, *xs_tmp;	
	double *x_res;
	
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_nodes);

	A = calloc (LD * N / num_nodes, sizeof(*A));
	B = calloc (LD * N / num_nodes, sizeof(*B));
	C = calloc (LD * N / num_nodes, sizeof(*C));
	Y = calloc (LD * N, sizeof(*Y));
	y = calloc (BS * N, sizeof(*y));
	D = calloc (LD * N, sizeof(*D));
	d = calloc (BS * N, sizeof(*d));
	b = calloc (BS * N, sizeof(*b));
	As = calloc (LD, sizeof(*As));
	x = calloc (BS * N, sizeof(*x));
	xs = calloc (BS, sizeof(*xs));
	bs = calloc (BS, sizeof(*bs));;
	A_cap = calloc (LD, sizeof(*A_cap));
	A_tmp = calloc (LD, sizeof(*A_tmp));
	b_cap = calloc (BS, sizeof(*b_cap));
	b_tmp = calloc (BS, sizeof(*b_tmp));
	d_tmp = calloc (BS, sizeof(*d_tmp));
	D_tmp = calloc (LD, sizeof(*D_tmp));
	xi = calloc (BS * N, sizeof(*xi));
	xs_tmp = calloc (BS, sizeof(*xs_tmp));
	x_res = calloc (BS * num_nodes, sizeof(*x_res));	
	
	if (my_rank == MASTER) {
		
		t_start = MPI_Wtime();
		
		A_data = calloc (LD * N, sizeof(*A_data));
		B_data = calloc (LD * N, sizeof(*B_data));
		C_data = calloc (LD * N, sizeof(*C_data));
		
		// Initialize the matrices
		for (ii = 0; ii < N; ii++){
			genera_Ai(&A_data[LD*ii], BS, ii+1);
			genera_Bi(&B_data[LD*ii], BS, ii+1);
			genera_Bi(&C_data[LD*ii], BS, ii+1);
		}
		
		genera_Ai(As, BS, N+1);
		
		for (ii = 0; ii < N; ii++) {
			for(jj = 0; jj < BS; jj++) 
				b[BS*ii + jj] = 1;
		}	
			
		for (ii = 0; ii < BS; ii++) 
			bs[ii] = 1;
	}
	
	// Send A matrix to all process
	MPI_Scatter(A_data, LD * N / num_nodes, MPI_DOUBLE, 
				A,      LD * N / num_nodes, MPI_DOUBLE, 
				MASTER, MPI_COMM_WORLD); 
	// Send B matrix to all process
	MPI_Scatter(B_data, LD * N / num_nodes, MPI_DOUBLE, 
				B,      LD * N / num_nodes, MPI_DOUBLE, 
				MASTER, MPI_COMM_WORLD);
	// Send C matrix to all process
	MPI_Scatter(C_data, LD * N / num_nodes, MPI_DOUBLE, 
				C,      LD * N / num_nodes, MPI_DOUBLE, 
				MASTER, MPI_COMM_WORLD);
	
	/* Step #1 
	 * Each processor performs the LU decomposition of their matrices Ai 
	 * generating the two matrices Li and Ui.
	 */
	for (ii = 0; ii < N / num_nodes; ii++) 
		LU (&A[LD*ii], BS);
		
	/* Step #2 
	 * Each processor calculates the term 'di' = Ci * Ai ^ -1 * bi for each 
	 * matrix Ai addressed to it using the LU decomposition calculated in step 
	 * number 1. Then it calculates the sum.
	 */
	// 2.1
	for (ii = 0; ii < N / num_nodes; ii++) {
		elim_avanti (&A[LD*ii], BS, &b[BS*ii], 1, &y[BS*ii]);
	}
	
	// 2.2
	for (ii = 0; ii < N / num_nodes; ii++)
		sost_indietro (&A[LD*ii], BS, &y[BS*ii], 1, &d[BS*ii]);
	
	 // #2.3 - Product matrix matrix, that is d_tmp = d_tmp + (Ci * di)
	for (ii = 0; ii < N / num_nodes; ii++)
		my_gemm (BS, 1, BS, &C[LD*ii], BS, 1.0, &d[BS*ii], 1, d_tmp, 1);
	
	/* Step #3
	 * Through a communication type "Reduce" with the sum on the terms 'di', 
	 * the master processor get the sum Ci = Ai^-1 * bi, with i from 0 to n-1 
	 * and with this calculates b_cap.
	 */
	// 3.1 - sum of d_tmp
	MPI_Reduce (d_tmp, b_tmp, BS, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);
	
	// #3.2 - Calcs di b_cap = bs - b_tmp
	for (ii = 0; ii < BS; ii++)
		b_cap[ii] = bs[ii] - b_tmp[ii];
	
	/* Step #4
	 * Each processor calculates the term Di = Ci * Ai^-1 * Bi by the 
	 * resolution of the n linear systems Ai * xj = Bij
	 */
	// 4.1
	for (ii = 0; ii < N / num_nodes; ii++)
		elim_avanti (&A[LD*ii], BS, &B[LD*ii], BS, &Y[LD*ii]);
	
	// 4.2
	for (ii = 0; ii < N / num_nodes; ii++)
		sost_indietro (&A[LD*ii], BS, &Y[LD*ii], BS, &D[LD*ii]);

	// #4.3 - Products matrix matrix, that is D_tmp = D_tmp + (Ci * Di)
	for (ii = 0; ii < N / num_nodes; ii++)
		my_gemm (BS, BS, BS, &C[LD*ii], BS, 1.0, &D[LD*ii], BS, D_tmp, BS);
	 
	/* Step #5
	 * Through a communication type "Reduce" with sum over the terms 'di', 
	 * the master processor get the sum Ci * Ai^-1 * Bi with i from 0 to n-1 
	 * and with this calculates A_cap.
	 */
	// 5.1 - sum of D_tmp
	MPI_Reduce (D_tmp, A_tmp, BS, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);
	
	// #5.2 - Calcs of A_cap, that is A_cap = As - A_tmp
	for (ii = 0; ii < BS; ii++)
		for (jj = 0; jj < BS; jj++)
			A_cap[jj+BS*ii] = As[jj+BS*ii] - A_tmp[jj+BS*ii];
	 
	/* Step #6 
	 * The master processor solves the linear system A_cap * xs = xs b_cap 
	 * to determine.
	 */
	LU (A_cap, BS);
	elim_avanti (A_cap, BS, b_cap, 1, xs_tmp);
	sost_indietro (A_cap, BS, xs_tmp, 1, xs);
	
	/* Step #7
	 * The solution xs is communicated to all processors via a broadcast.
	 */
	MPI_Bcast (xs, BS, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
	
	/* Step #8
	 * Each processor j-th solves linear systems Ai*xi = bi - Bi*xs
	 */
	for (ii = 0; ii < N / num_nodes; ii++) {
		// #8.1 Calcs Bi*xs = x
		my_gemm(BS, 1, BS, &B[LD*ii], BS, 1, xs, 1, &x[BS*ii], 1);
		
		// #8.2 Calcs bi-x = x
		for (jj = 0; jj < BS; jj++)
			x[BS*ii + jj] = b[BS*ii + jj] -x[BS*ii + jj];
		
		// #8.3 Resolve the system Ai*xi = x
		elim_avanti   (&A[LD*ii], BS, &x[BS*ii], 1, xs_tmp);
		sost_indietro (&A[LD*ii], BS, xs_tmp, 1, &x[BS*ii]);
	}
	
	/* Step #9
	 * The components xi are combined into a single vector with a "Gather". 
	 * Joined together to form the xs solution to the initial problem.
	 */
	MPI_Gather (xi, BS, MPI_DOUBLE,
				x_res, BS, MPI_DOUBLE,
				MASTER, MPI_COMM_WORLD);
	
	for (ii = 0; ii < N; ii++){
		for (jj = 0; jj < BS; jj++) {
			if (ii < N)
				xi[BS*ii + jj] = x[BS*ii + jj];
			else
				xi[BS*ii + jj] = xs[jj];
		}
	}
	
	// Stop timer
	if (my_rank == MASTER) {
		t_end = MPI_Wtime();
		printf("Time is %f\n", t_end - t_start);
		free(A_data); free(B_data); free(C_data);
	}
	
	free(A); free(A_cap); free(A_tmp); 
	free(D); free(D_tmp); free(d_tmp); free(d);
	free(b); free(b_cap); free(b_tmp);
	free(B); free(C); 
	free(Y); free(y); 
	free(As); free(bs);
	free(x); free(xs); free(xs_tmp); free(x_res);
	
	MPI_Finalize();
	return  0;
}
