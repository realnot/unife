#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include "res.h"

#define N 128

// Blocksize
#define BS 256

// Leading Dimension
#define LD (BS * BS)

// Prototypes
void init_sequence(void);
void shutdown_sequence(void);

// Global
double *A, *B, *C;
double *x, *b;
double *As, *bs, *xs;
double *AB, *Ab, *Y, *y;
clock_t start, stop;

/* 
 * Serial block bordered system 
 */
int main (int argc, char **argv) {
	
	size_t ii, jj, kk;
	start = clock();
	
	// Reserve memory and build the matrix
	init_sequence();
	
	// ( 1 ) Calc the LU decomposition of matrices A0 ... An-1
	for (ii = 0; ii < N-1; ii++) 
		LU (&A[LD*ii], BS);
	
	// Forward elimination Ai^-1 * Bi (resolve BS systems Ly = b) 
	for( ii = 0; ii < N-1; ii++){
		elim_avanti (&A[ii*LD], BS, &B[ii*LD], BS, &Y[ii*LD]);
		elim_avanti (&A[ii*LD], BS, &b[ii*BS], 1, &y[ii*BS]);
	}
	
	// Backward substitution Ai^-1 * bi (resolve BS systems Ux = y)
	for (ii = 0; ii < N-1; ii++) {
		sost_indietro (&A[LD*ii], BS, &Y[ii*LD], BS, &AB[LD*ii]);
		sost_indietro (&A[LD*ii], BS, &y[ii*BS], 1, &Ab[BS*ii]);
	}
	
	// ( 2 ) Calc of A_cap
	double* A_cap = ( double* ) calloc ( LD, sizeof(double) );
	double* D = (double*) calloc (LD, sizeof(double));
	
	// Calc of matrix D = D + Ci*AB 
	for (ii = 0; ii < N-1; ii++)
		my_gemm (BS, BS,BS, &C[LD *ii], BS, 1.0, &AB[LD *ii], BS, D, BS);
	
	// Calc of A_cap = As - D
	for (ii = 0; ii < BS; ii++)
		for (jj = 0; jj < BS; jj++)
			A_cap[jj + BS*ii]=As[jj + BS *ii] - D[jj + BS*ii];
		
	// Calc of b_cap
	double* b_cap = (double* ) calloc (BS, sizeof(double));
	double* d= (double*) calloc (BS, sizeof(double));
	
	// Calc of matrix d = d + Ci*Ab
	for( ii=0; ii < N-1; ii++)
		my_gemm (BS, 1, BS, &C[LD*ii], BS, 1.0, &Ab[ii*BS], 1, d, 1);
	
	// Calc of b_cap = bs - D
	for (ii = 0; ii < BS; ii++)
		b_cap[ii]=bs[ii]-d[ii];
	
	// Calc of A_cap * xs = b_cap
	// LU decomposition of A_cap
	LU (A_cap, BS);
	// Forward elimination
	double* xs_temp = (double*) calloc (BS, sizeof(double));
	elim_avanti (A_cap, BS, b_cap, 1, xs_temp);
	// Backward substitution
	sost_indietro (A_cap, BS,xs_temp, 1, xs);
	
	// Calc of xi
	for (ii = 0; ii < N-1; ii++) {
	// x = Bi*xs
	my_gemm (BS, 1, BS, &B[ii*LD], BS, 1, xs, 1, &x[ii*BS], 1);
	// x = bi-x
	for (jj = 0; jj < BS; jj++)
		x[ ii * BS + jj] = b[ ii * BS + jj] -x[ ii * BS + jj];
		// Solve the system Ai*x=x
		elim_avanti(&A[ii*LD], BS, &x[ii*BS], 1, xs_temp);
		sost_indietro(&A[ii*LD], BS, xs_temp, 1, &x[ii*BS]);
	}
	
	stop = clock();
	double *xi = (double*) malloc (BS*N*sizeof(double));
	
	// Memorize all the solutions in the final vector xi
	for (ii = 0; ii < N; ii++) {
		for (jj = 0; jj < BS; jj++) {
			if (ii < (N-1))
				xi[ii*BS + jj] = x[ii*BS + jj];
			else
				xi[ii*BS + jj] = xs[jj];
		}
	}
	
	// Show result and release the memory
	shutdown_sequence();
	
	return  0;
}

/*
 *
 */
void init_sequence (void) {
	
	size_t ii, jj;
	
	A = calloc (LD * (N-1), sizeof(*A));
	B = calloc (LD * (N-1), sizeof(*B));
	C = calloc (LD * (N-1), sizeof(*C));
	x = calloc (BS * (N-1), sizeof(*x));
	b = calloc (BS * (N-1), sizeof(*b));
	As = calloc (BS, sizeof(*As));
	xs = calloc (BS, sizeof(*xs));
	bs = calloc (BS, sizeof(*bs));
	*AB = calloc (LD * (N-1), sizeof(*AB));
	*Ab = calloc (BS * (N-1), sizeof(*Ab));
	*Y = calloc (LD * (N-1), sizeof(*Y));
	*y = calloc (BS * (N-1), sizeof(*y));
	
	// Generation of the matrices Ai, Bi e Ci. 
	for (ii = 0; ii < N-1; ii++) {
		genera_Ai(&A[LD * ii], BS, ii+1);
		genera_Bi(&B[LD * ii], BS, ii+1);
		genera_Bi(&C[LD * ii], BS, ii+1);
	}
	
	// Generation of matrix As
	genera_Ai(As, BS, N+1);	
	
	// Construction of the matrices "bi" and block "bs"
	for (ii = 0; ii < N-1; ii++)
		for (jj = 0; jj < BS; jj++)
			b[ii*BS + jj] = 1;
			
	for( ii = 0; ii < BS; ii++) 
		bs[ii] = 1;
}

/*
 *
 */
void shutdown_sequence (void) { 
	
	//printf("\nStampa dei risultati:\n");
	//for( ii = 0; ii < N; ii++)
	// print_mat(&xi[BS * ii], BS, 1);
	
	printf("\nTime: %0.2f\n",(stop-start)/(double)(CLOCKS_PER_SEC));
	
	// Release the memory
	free(A); free(B); free(C);
	free(As); free(xs); free(bs); free(x); free(b); 
	free(Ab); free(AB); free(Y); free(y);
	free(A_cap); free(D); free(d);
	free(b_cap); free(xs_temp); free(xi);
}