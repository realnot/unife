/*
 * QR decomposition via modified Gram-Schmidt algorithm
 * 
 * @Author = Mauro Crociara
 * @Contact = mauro.crociara@student.unife.it
 * @Repository = https://bitbucket.org/realnot/unife/src
 */

// Libraries
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

// Constant
#define PROG "QR_cpu"
#define VERSION "28.0913"
#define PACKAGE "QR-Decomposition"

// Prototypes
void r1_update(double *, double *, int);
void xTA (double *, double *, int);
void scale (double *, int, double);
void gram (double *, double *);
void print_matrix(double *);
int option_parser(int, char **);
void print_help (int);

// Number of rows
int M = -1; 
// Number of columns
int N = -1;
// Leading Dimension of A and R
int LDA = -1;
int LDR = -1;
// The clock
clock_t start, stop;

/*
 *
 */
int main (int argc, char **argv) {
	
	int i;
	
	// Get M, N from command line
	if (0 != option_parser(argc, argv)) {
		fprintf(stderr, "Bad option man!!!\n");
		exit(EXIT_FAILURE);
	}
	// Checks the size of M and N 
	if (M > 5000 || N > 5000) {
		fprintf(stderr, "Too big man!!!\n");
		exit(EXIT_FAILURE);
	}
	
	// Set the leading dimension of A and R
	LDA = N; LDR = N;
	
	// Reserve memory for A and R 
	double *A = calloc(M * N, sizeof(*A));
	double *R = calloc(N * N, sizeof(*R));
		
	// Set the diagonal of A as A(i,i) = i + 1 with i from 0 to N−1
	for (i = 0; i < N; i++) 
		A[i*LDA + i] = i + 1;
	
	start = clock();
	gram(A, R);
	stop = clock();
	
	printf("\nTime: %0.4f\n\n",(stop - start) / (double)(CLOCKS_PER_SEC));
	//print_matrix(A);
	//print_matrix(R);
	
	free(A); free(R);
	
	return 0;
}

/**
 * Rank 1 update of columns of A
 */
void r1_update (double *A, double *R, int k) {
	
	int i, j;
	
	for(i = 0; i < M; i++)
		for(j = k + 1; j < N; j++)
			A[i*LDA + j] -= A[i*LDA + k] * R[j];
}

/**
 * Matrix vector product
 * Performs R[i] =  x'A where x' is a row of A
 * A : m x k, leading dimebsion, lda
 * 
 * How leading dimension is used for matrices: http://ibm.co/19PLtIX
 */
//void xTA (double *y, int k, double *A, int m, int lda, double *x, int ldx) {
void xTA (double *R, double *A, int k) {
	
	int i, j;
	// upper triangular matrix 
	for (i = 0; i < N-k; i++)
		for (j = 0; j < M; j++)
			R[k*LDR + k + i] += A[k*LDA + j] * A[j*LDA + k + i];
}

/**
 * Mult. for constant s
 * d vector
 * ld leading dimension (distance from elements)
 */
void scale (double *d, int ld, double s) {
	
	int i;
	
	for (i = 0; i < M; i++) d[i*ld] *= s;
}

/**
 * Performs Modified Gram Schmidt
 * ortogonalization of columns of A
 * A m x n
 * R n x n
 */ 
void gram (double *A, double *R) {
	
	int i;
	double s;
	// Modified Gram Schmidt algorithm step by step
	for (i = 0; i < N; i++) {
		// Step #1 --> R(i,i:n-1) = A'(:,i) * A(:,i:n-1)
		xTA(R, A, i);
		// Step #2 (Normalizing) --> s = sqrt(R(i,i))
		s = 1 / sqrt(R[i*LDR + i]);
		// Step #3 (Is the scale of a column vector)
		scale(A + i, LDA, s);
		// Step #4 (Is the scale of a row)
		scale(R + LDR*i, 1, s);
		// Step #5 --> A(:,i+1:n−1) = A(:,i+1:n−1) − A(:,i) ∗ R(i,i+1:n−1)
		r1_update(A, R + i*LDA, i);
	}
}

/* 
 * Print Matrix
 * 
 * Print a matrix passed as argument
 */
void print_matrix (double * matrix) {
	
	int i, j;
	
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++)
			printf("%0.2f ", matrix[i*LDA + j]);
		printf("\n");
	}
}

/*
 * The option parser
 *
 * The function parses the parameters passed from the command line and run 
 * their own procedures.
 *
 * Return value:
 *   0 on success
 *  -1 on failure
 *
 * Please, see http://www.gnu.org/software/libc/manual/html_node/Getopt.html
 * for further informations. (thanks to Frodo Looijaard)
 */
int option_parser (int argc, char **argv) {
	
	int opt;
	
	if (argc < 2) {
		fprintf(stderr, "This program needs arguments...\n\n");
		print_help(1);
	}
	
	opterr = 0;
	
	while ( -1 != (opt = getopt (argc, argv, "hr:c:"))) {
		switch (opt) {
			case 'h': 
				print_help(0);
			case 'r': 
				printf("optarg: %s\n", optarg); 
				if ((M = atoi(optarg)) < 2) return -1; 
				break;
			case 'c': 
				printf("optarg: %s\n", optarg);
				if ((N = atoi(optarg)) < 2 || N > M) return -1; 
				break;
			case '?':
				if (optopt == 'r' || optopt == 'c')
					fprintf(stderr,"Option -%c requires an argument.\n",optopt);
				else if (isprint (optopt))
					fprintf(stderr,"Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr,"Unknown option chr `\\x%x'.\n", optopt);
				return -1;
			default:
				fprintf(stderr, "default switch-case statement reached\n");
				return -1;
		}
		//for (ii = optind; ii < argc; ii++)
		//	printf ("Non-option argument %s\n", argv[ii]);
	}
	return 0;
}

/*
 * The helper
 *
 * Shows the info to run the program in the correct way
 */
void print_help (int exit_val) {

	printf("\nPKG : %s\nPROGRAM : %s\nVERSION : %s\n\n",PACKAGE,PROG,VERSION);
	
	printf("%s [-h] [-r num of rows] [-c num of columns]\n\n", PROG);
	printf("  -h    print this help and exit\n");
	printf("  -r    provide the number of rows\n");
	printf("  -c    provide the number of colums\n\n");
	
	printf("  Example: ./QR_cpu -r 200 -c 400\n\n");
	
	exit_val == -1 ? exit(EXIT_FAILURE) : exit(EXIT_SUCCESS);
}
