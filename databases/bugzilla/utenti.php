<?php include 'header.php' ?>
	<div id="content">
	<div id="logo"><h1>Bugzilla</h1></div>
		<div id="add_user" class="add_user_form">
		<?php
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
$id_utente = (isset( $_GET['id_utente'])) ?  $_GET['id_utente'] : FALSE;
$session = (isset($_SESSION['loggedin'])) ? $_SESSION['loggedin'] : FALSE;

$store = '';

if(isset($_POST['store'])) $store = $_POST['store'];
else if(isset($_GET['store'])) $store = $_GET['store'];

$error_message = "";


    if($session != TRUE && $action == 'register') {
        
        include_once('include/user_form.php');
    
    }  else if($session && $action == 'edit') {
         
        include_once('include/user_form.php');

    } else if($action == 'store') {

        
        $data['username'] = (isset($_POST['username'])) ? $_POST['username'] : FALSE;
        $data['password'] = (isset($_POST['password'])) ? $_POST['password'] : FALSE;
        $data['passwd_rp'] = (isset($_POST['passwd_rp'])) ? $_POST['passwd_rp'] : FALSE;
        $data['firstname'] = (isset($_POST['firstname'])) ? $_POST['firstname'] : FALSE;
        $data['lastname'] = (isset($_POST['lastname'])) ? $_POST['lastname'] : NULL;
        $data['middlename'] = (isset($_POST['middlename'])) ? $_POST['middlename'] : FALSE;
        $data['address'] = (isset($_POST['address'])) ? $_POST['address'] : NULL;
        $data['city'] = (isset($_POST['city'])) ? $_POST['city'] : FALSE;
        $data['state'] = (isset($_SESSION['state'])) ? $_SESSION['state'] : FALSE;
        $data['zipcode'] = (isset($_POST['zipcode'])) ? $_POST['zipcode'] : FALSE;
        $data['telephone'] = (isset($_POST['telephone'])) ? $_POST['telephone'] : NULL;
        $data['fax'] = (isset($_POST['fax'])) ? $_POST['fax'] : FALSE;
        $data['mobile'] = (isset($_POST['mobile'])) ? $_POST['mobile'] : NULL;
        $data['email'] = (isset($_POST['email'])) ? $_POST['email'] : FALSE;
        $data['website'] = (isset($_SESSION['website'])) ? $_SESSION['website'] : FALSE;
        $data['comments'] = (isset($_POST['comments'])) ? $_POST['comments'] : FALSE;
        $data['id_utente'] = (isset($_POST['id_utente'])) ? $_POST['id_utente'] : FALSE;
      
        $data['user_id'] = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : FALSE;

        print $store;
        
        if( check_user_data($data) ) {

            # Apro una connessione al DB
            $connection = open_db_connection();

            if($store == 'update') {

                $query = "
                    UPDATE utenti SET
                       username = '{$data['username']}', 
                       password = '{$data['password']}',
                       nome = '{$data['firstname']}', 
                       cognome = '{$data['lastname']}', 
                       soprannome = '{$data['middlename']}',
                       via = '{$data['address']}', 
                       citta = '{$data['city']}', 
                       stato = '{$data['state']}', 
                       cap = '{$data['zipcode']}',
                       telefono = '{$data['telephone']}', 
                       fax = '{$data['fax']}',
                       cell = '{$data['mobile']}', 
                       email = '{$data['email']}', 
                       website = '{$data['website']}', 
                       commenti = '{$data['comments']}'
                    WHERE id_utente=".$data['id_utente'].";";
                
            } else if($store == 'insert') {
                
                $query = "
                    INSERT INTO utenti (
                        username, 
                        password,
                        nome,
                        cognome, 
                        soprannome,
                        via, 
                        citta, 
                        stato, 
                        cap,
                        telefono, 
                        fax, 
                        cell, 
                        email, 
                        website, 
                        commenti
                    ) VALUES (
                        '{$data['username']}', 
                        '{$data['password']}',
                        '{$data['firstname']}', 
                        '{$data['lastname']}', 
                        '{$data['middlename']}',
                        '{$data['address']}', 
                        '{$data['city']}', 
                        '{$data['state']}', 
                        '{$data['zipcode']}',
                        '{$data['telephone']}', 
                        '{$data['fax']}',
                        '{$data['mobile']}', 
                        '{$data['email']}', 
                        '{$data['website']}', 
                        '{$data['comments']}'
                    )";
        	   }

        	   print $query;
                if ( ! mysql_query( $query, $connection) ) {
                    died('Impossibile aggiungere i dati nel database!'.mysql_error());
                } else {
                    print_success('Dati registrati con successo!');
                    header('Refresh:3; URL=last_updates.php#bug_list');
                    mysql_close($connection);
                }
        }
    } else if ($action == 'delete') {

    	$data['user_id'] = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : FALSE;
    	$data['id_utente'] = (isset($_GET['id_utente'])) ? $_GET['id_utente'] : FALSE;

    	
    	print $data['id_utente'];
    	
        $connection = open_db_connection();
                    	
		$query = "DELETE FROM utenti WHERE id_utente=".$data['id_utente'].";";

		print $query;

		if ( ! mysql_query( $query, $connection) ) {
			$error_message .= mysql_error();
			died($error_message);
		} else {
			print_success('Utente '.$data['id_utente'].' eliminato con successo!');
			header('Refresh:3; URL=last_updates.php#user_registered');
			mysql_close($connection);
		}
	}

?>
		</div>
	</div>
<?php include 'footer.php' ?>