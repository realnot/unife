CREATE DATABASE bugzilla;
CONNECT bugzilla;

CREATE TABLE utenti
(
    id_utente INT(11) NOT NULL AUTO_INCREMENT,
    username VARCHAR(15) NOT NULL,
    password VARCHAR(15) NOT NULL,
    nome VARCHAR(15) NOT NULL,
    cognome VARCHAR(15) NOT NULL,
    soprannome VARCHAR(25),
    via VARCHAR(20),
    citta VARCHAR(20),
    stato VARCHAR(20),
    cap INT(5),
    telefono INT(10),
    fax INT(10),
    cell INT(10),
    email VARCHAR(35),
    website VARCHAR(40),
    commenti VARCHAR(500),
    PRIMARY KEY (id_utente)
);

CREATE TABLE prodotto
(
    id_prodotto INT(5) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    descrizione VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_prodotto)
);

CREATE TABLE riproducibilita 
(
    id_riproducibilita INT(5) NOT NULL AUTO_INCREMENT,
    descrizione VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_riproducibilita)
);

CREATE TABLE gravita 
(
    id_gravita INT(5) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    descrizione VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_gravita)
);

CREATE TABLE piattaforma_hardware
(
    id_hp INT(5) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    descrizione VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_hp)
);

CREATE TABLE sistema_operativo
(
    id_os INT(5) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    descrizione VARCHAR(250) NOT NULL,
    versione VARCHAR(30),
    licenza VARCHAR(20),
    PRIMARY KEY (id_os)
);

CREATE TABLE bug
(   
    id_bug INT(7) NOT NULL AUTO_INCREMENT,
    id_os INT(5) NOT NULL,
    id_hp INT(5) NOT NULL,
    id_prodotto INT(5) NOT NULL,
    riassunto VARCHAR(500) NOT NULL,
    descrizione VARCHAR(500),
    id_riproducibilita INT(5) NOT NULL,    
    info_aggiuntive VARCHAR(500),
    id_gravita INT(5) NOT NULL,
    id_utente INT(11) NOT NULL, 
    PRIMARY KEY (id_bug),
    FOREIGN KEY (id_os) REFERENCES sistema_operativo (id_os),
    FOREIGN KEY (id_hp) REFERENCES piattaforma_hardware (id_hp),
    FOREIGN KEY (id_utente) REFERENCES utenti (id_utente),
    FOREIGN KEY (id_prodotto) REFERENCES prodotto (id_prodotto),
    FOREIGN KEY (id_gravita) REFERENCES gravita (id_gravita),
    FOREIGN KEY (id_riproducibilita) REFERENCES riproducibilita (id_riproducibilita)
);

SHOW TABLES;
