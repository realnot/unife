INSERT INTO prodotto
    (id_prodotto, nome, descrizione)
VALUES
    ('00001','Linux','Problemi riguardanti linux.'),
    ('00002','Altra Documentazione','Documentazione che non riguarda bugzilla.com.'),
    ('00003','Traduzione Documentazione','Tutta la documentazione non in italiano.'),
    ('00004','Documentazione','Guide ed Handbook'),
    ('00005','Progetti Ospitati','Tutti i progetti hostati su bugzilla.com.'),
    ('00006','Infrastruttura Linux','Per i bugs o features relative a bugzilla.com.'),
    ('00007','Linux Release','Distribuzioni Linux'),
    ('00008','Installazione da CD','Se avete qualche problema con l''installazione da CD.'),
    ('00009','Sicurezza di Linux','Problemi riguardanti la sicurezza del sistema.'),
    ('00010','Gestore di Pacchetti','Problemi relativi all''installazione di nuovi pacchetti.');

INSERT INTO riproducibilita
    (id_riproducibilita, descrizione)
VALUES
    ('00001','Accade Sempre.'),
    ('00002','Accade qualche volta, ma non sempre.'),
    ('00003','Non ho provato a riprodurre l''errore.'),
    ('00004','Ho provato, ma non riesco ha riprodurre l''errore.');

INSERT INTO gravita
    (id_gravita, nome, descrizione)
VALUES
    ('00001','Bloccato','Blocca lo sviluppo e la possibilita'' di lavorare.'),
    ('00002','Critico','Il software va in crash, oppure perde i dati.'),
    ('00003','Maggiore','Le caratteristiche principali non funzionano.'),
    ('00004','Normale','E'' bug, dovrebbe essere risolto.'),
    ('00005','Minore','Minore perdita delle funzionalita'' c''e'' una soluzione facile.');

INSERT INTO piattaforma_hardware 
    (id_hp, nome, descrizione)
VALUES
    ('00001','Altro','Piattaforma non presente tra le seguenti'),
    ('00002','AMD86','x86-64, estensione a 64 bit del set x86'),
    ('00003','x86','Architettura inizialmente sviluppata da Intel'),
    ('00004','Alpha','Architettura RISC sviluppata da Digital Equipment Corp'),
    ('00005','Sparc','Architettura per microprocessori big-endian RISC disegnata da Sun Microsystems'),
    ('00006','IA64','Architettura a 64 bit sviluppata tra Intel e HP'),
    ('00007','PPC','Architettura RISC creata nel 1991 da Apple-IBM-Motorola'),
    ('00008','MIPS','Architettura per microprocessori RISC sviluppata da MIPS Computer Systems Inc'),
    ('00009','PPC64','Architettura PPC ottimizzata per applicazioni a 64 bit'),
    ('00010','HPPA','Hewlett-Packard Precision Architecture, architettura RISC'),
    ('00011','s390','Architettura per mainframe introdotta da IBM nel 1990'),
    ('00012','sh','Architettura a 32 bit RISC sviluppata da Hitachi'),
    ('00013','Sparc64','Microprocessore sviluppato da HAL Computer Systems a fabbricato da Fujitsu'),
    ('00014','ARM','Famiglia di microprocessori a 32 bit sviluppata da ARM'),
    ('00015','m68k','Famiglia di microprocessori 32 bit CISC sviluppata da Motorola');

INSERT INTO sistema_operativo
    (id_os, nome, descrizione, versione, licenza)
VALUES 
    ('00001','Altro','Sistema operativo non presente tra i seguenti','',''),
    ('00002','Tutti','Il bug include tutti i sistemi operativi','',''),
    ('00003','Linux','Famiglia di sistemi operativi di tipo Unix-Like','','GNU GPL'),
    ('00004','FreeBSD','Sistema di tipo UNIX derivato dalla distribuzione dell universita di Berkeley','','FreeBSD'), 
    ('00005','OS X','Sistema operativo sviluppato da Apple Inc','','Proprietario'),
    ('00006','GNU/kFreeBSD','Sistema operativo per architetture IA-32 e x86-64','','FreeBSD'),
    ('00007','NetBSD','Sistema operativo Unix-Like derivato da BSD altamente portabile','','BSD'),
    ('00008','OpenBSD','Sistema operativo libero basato sulla variante BSD di UNIX','','BSD'),
    ('00009','DragonFly','Sistema libero Unix-Like creato da un fork di FreeBSD 4.8','','BSD'),  
    ('00010','Solaris','Sistema operativo sviluppato da Sun Microsystems','','Varie'),
    ('00011','AIX','Serie di sistemi operativi UNIX proprietari sviluppati da IBM','','Proprietario'),
    ('00012','HPUX','Hewlett-Packard UNIX','','Proprietario'),
    ('00013','IRIX','Sistema operativo sviluppato da Silicon Graphic Inc','','Proprietario'),
    ('00014','Interix','Sottosistema POSIX per Windows NT','','Proprietario'),
    ('00015','FreeMiNT','Sistema alternativo per Atari ST','','GNU GPL');

INSERT INTO utenti
    (id_utente, username, password, nome, cognome, soprannome, via, citta, stato, cap, telefono, fax, cell, email, website, commenti)
 VALUES
    ('00000000001','salvo','admin','Salvatore','Alescio','','Emilia 5','Bologna','ITA','40023','','','','salvatore.alescio@student.unife.it','',''),
    ('00000000002','chiara','admin','Chiara','Silvestro','','Bagigi 12/a','Ferrara','ITA','44100','','','','chiara.silvestro@student.unife.it','',''),
    ('00000000003','enrico','admin','Enrico','Galavotti','','zipo 11','Cento','ITA','44100','','','','enrico.galavotti@student.unife.it','',''),
    ('00000000004','francesco','admin','Francesco','Barbieri','','Gimmy 45','Ferrara','ITA','44100','','','','francesco.barbieri@student.unife.it','',''),
    ('00000000005','mattia','admin','Mattia','De Stefani','','Paride 87/c','Ferrara','ITA','44100','','','','mattia.destefani@student.unife.it','',''),
    ('00000000006','giulia','admin','Giulia','Beltrami','','Picadilly Circus 1232','London','ENG','99231','','','','giulia.beltrami@student.unife.it','',''),
    ('00000000007','mirco','admin','Mirco','Civolani','','Provieri 222','Ferrara','ITA','44100','','','','mirco.civolani@student.unife.it','',''),
    ('00000000008','antonia','admin','Antonia','Paglione','','Mazzanti 23','Ferrara','ITA','44100','','','','antonia.paglione@student.unife.it','',''),
    ('00000000009','daniele','admin','Daniele','Berto','','Trombetti 44','Ferrara','ITA','44100','','','','daniele.berto@student.unife.it','',''),
    ('00000000010','andrea','admin','Andrea','Vettore','','Umbero Poggi 766','Ferrrara','ITA','44100','','','','andrea.vettore@student.unife.it','',''),
    ('00000000011','dario','admin','Dario','Vacchi','','Uscolo 121','Bologna','ITA','40023','','','','dario.vacchi@student.unife.it','',''),
    ('00000000012','nicola','admin','Nicola','Bettini','','Dimitri 233/d','Firenze','ITA','50100','','','','nicola.bettini@student.unife.it','',''),  
    ('00000000013','federico','admin','Federico','Fantini','','Ercole 4','Firenze','ITA','50100','','','','federico.fantini@student.unife.it','',''),
    ('00000000014','pierluca','admin','Pierluca','Tonido','','Rodolfo 99','Verona','ITA','37100','','','','pierluca.tonido@student.unife.it','',''),
    ('00000000015','filippo','admin','Filippo','Campi','','Mortara 1212','Rimini','ITA','47923','','','','filippo.campi@student.unife.it','',''),
    ('00000000016','alice','admin','Alice','Valentini','','Tulio 98','Palermo','ITA','90121','','','','alice.valentini@student.unife.it','',''),
    ('00000000017','michele','admin','Michele','Mantovani','','Ludovico 576','Roma','ITA','10010','','','','michele.mantovani@student.unife.it','',''),
    ('00000000018','paolo','admin','Paolo','Dolzani','','Vicentini 453','Torino','ITA','10010','','','','paolo.dolzani@student.unife.it','',''),
    ('00000000019','mari','admin','Filippo','Mari','','Asiago 283','Alfonsine','ITA','48011','','','','filippo.mari@student.unife.it','',''),
    ('00000000020','ferrari','admin','Laura','Ferrari','','Umberto 827','Bologna','ITA','40023','','','','laura.ferrari@student.unife.it','',''),
    ('00000000021','davide','admin','Davide','Penitenti','','Adolfo 222','Bologna','ITA','40023','','','','davide.penitenti@student.unife.it','',''),
    ('00000000022','massimo','admin','Massimo','Rizzo','','Giardi 221','Palermo','ITA','90121','','','','massimo.rizzo@student.unife.it','',''),
    ('00000000023','mauro','admin','Mauro','Crociara','','Toretto 224','Palermo','ITA','90121','','','','mauro.crociara@student.unife.it','',''),
    ('00000000024','marcello','admin','Marcello','Marzola','','Asgard 987','Cagliari','ITA','99010','','','','marcello.marzola@student.unife.it','',''),
    ('00000000025','michael','admin','Micheal','Cannella','','Avengers 656','Cagliari','ITA','99010','','','','michael.cannella@student.unife.it','','');

INSERT INTO bug
    (id_bug, id_os, id_hp, id_prodotto, riassunto, descrizione, id_riproducibilita, info_aggiuntive, id_gravita, id_utente)
VALUES
    ('0000001','00003','00005','00003','Ho riscontrato problemi...','','00001','','00003','00000000009'),
    ('0000002','00004','00003','00001','Qui non funziona niente','','00002','','00002','00000000009'),
    ('0000003','00005','00002','00001','Non ho voglia di scrivere...','','00005','','00001','00000000003'),
    ('0000004','00006','00012','00001','grande bugizllaaaa..','','00003','','00005','00000000003'),
    ('0000005','00007','00002','00003','asdsaddasdsadsasa...','','00001','','00005','00000000008'),
    ('0000006','00008','00012','00002','il team di sviluppo deve lavorare meglio...','','00005','','00005','00000000008'),
    ('0000007','00003','00002','00002','ma gli ingegneri sapevano quel che facevano quando...','','00005','','00005','00000000009'),
    ('0000008','00009','00011','00002','Ho riscontrato problemi...','','00001','','00004','00000000002'),
    ('0000009','00003','00002','00005','Ho riscontrato problemi...','','00001','','00003','00000000001'),
    ('0000010','00012','00006','00005','Ho riscontrato problemi...','','00002','','00003','00000000001'),
    ('0000011','00013','00007','00003','Ho riscontrato problemi...','','00004','','00003','00000000009'),
    ('0000012','00003','00008','00004','Ho riscontrato problemi...','','00004','','00002','00000000009'),
    ('0000013','00003','00009','00004','Ho riscontrato problemi...','','00001','','00001','00000000011'),
    ('0000014','00015','00002','00001','Ho riscontrato problemi...','','00003','','00001','00000000010'),
    ('0000015','00015','00003','00001','Ho riscontrato problemi...','','00004','','00002','00000000008');



   
