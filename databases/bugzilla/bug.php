<?php include 'header.php' ?>
	<div id="content">
	<div id="logo"><h1>Bugzilla</h1></div>
		<div id="add_bug" class="bug_form">
		<?php 
		
		$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
		$store = (isset($_POST['store'])) ? $_POST['store'] : $_GET['store'];
		$id_bug = (isset( $_GET['id_bug'])) ?  $_GET['id_bug'] : FALSE;
		$session = (isset($_SESSION['loggedin'])) ? $_SESSION['loggedin'] : FALSE;

		$error_message = '';
		
		if($session) {
		
			if($action == 'insert' || $action == 'edit') {
			    
			    include_once('include/bug_form.php');
		
			} else if($action == 'store') {
			
				$data['product'] = (isset($_POST['product'])) ? $_POST['product'] : FALSE; 
				$data['platform'] = (isset($_POST['platform'])) ? $_POST['platform'] : FALSE;
				$data['os'] = (isset($_POST['os'])) ? $_POST['os'] : FALSE;
				$data['summary'] = (isset($_POST['summary'])) ? $_POST['summary'] : FALSE;
				$data['description'] = (isset($_POST['description'])) ? $_POST['description'] : NULL;
				$data['reproducibility'] = (isset($_POST['reproducibility'])) ? $_POST['reproducibility'] : FALSE;
				$data['additional'] = (isset($_POST['additional'])) ? $_POST['additional'] : NULL;
				$data['severity'] = (isset($_POST['severity'])) ? $_POST['severity'] : FALSE;
				$data['id_bug'] = (isset($_POST['id_bug'])) ? $_POST['id_bug'] : FALSE;
				$data['user_id'] = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : FALSE;
				
				if ( ! $data['user_id'] ) {
					died('User id non valido o non esistente!');
				}
				
				if ( ! $data['summary'] ) {
					died('Il riassunto non esiste!');
				} else {
					
					if( check_bug_data($data['summary']) ) {
						
						# Apro una connessione al DB
						$connection = open_db_connection();
						
						if($store == 'update') {
														
							$query = "
								UPDATE bug SET
									id_os = '{$data['os']}', 
									id_hp ='{$data['platform']}', 
									id_prodotto = '{$data['product']}', 
									riassunto  = '{$data['summary']}',
									descrizione = '{$data['description']}',
									id_riproducibilita = '{$data['reproducibility']}',
									info_aggiuntive = '{$data['additional']}',
									id_gravita = '{$data['severity']}',
									id_utente = '{$data['user_id']}'
								WHERE id_bug=".$data['id_bug'].";";
							
						} else if($store == 'insert') {
							
							$query = "
								INSERT INTO bug (
									id_os, id_hp, id_prodotto, riassunto,
									descrizione, id_riproducibilita, info_aggiuntive, 
									id_gravita, id_utente
								) VALUES (
									'{$data['os']}', 
									'{$data['platform']}', 
									'{$data['product']}', 
									'{$data['summary']}',
									'{$data['description']}',
									'{$data['reproducibility']}',
									'{$data['additional']}',
									'{$data['severity']}',
									'{$data['user_id']}'
								);
							";
						} 
						
						if ( ! mysql_query( $query, $connection) ) {
							died('Impossibile aggiungere i dati nel database!'.mysql_error());
						} else {
							print_success('Dati registrati con successo!');
							header('Refresh:3; URL=last_updates.php#bug_list');
							mysql_close($connection);
						}
					}
				}
			} else if ($action == 'delete') {

				$data['user_id'] = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] : FALSE;
				$data['id_bug'] = (isset($_GET['id_bug'])) ? $_GET['id_bug'] : FALSE;
				
				$connection = open_db_connection();
					
				$query = "DELETE FROM bug WHERE id_bug=".$data['id_bug'].";";
				
				print $query;
				
				if ( ! mysql_query( $query, $connection) ) {
					$error_message .= mysql_error();
					died($error_message);
				} else {
					print_success('Bug '.$data['user_id'].' eliminato con successo!');
					header('Refresh:3; URL=last_updates.php#bug_list');
					mysql_close($connection);
				}
			}
		}
		?>
		</div>
	</div>
<?php include 'footer.php' ?>