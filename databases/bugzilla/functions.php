<?php 

/** Nome del database per Bugzilla */
define('DB_NAME', 'bugzilla');

/** Nome utente del database MySQL */
define('DB_USER', 'admin');

/** Passowrd del database MySQL */
define('DB_PASSWORD', '12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset per le tabelle. */
define('DB_CHARSET', 'utf8');

/**
 * La funzione stampa il messaggio di errore passato in ingresso ed usa die()
 * per terminare l'esecuzione dello script.
 * 
 * @Since 20130102
 */
function died($error_message) {
	die('<div id="error">' . $error_message . '</div>');
}

/**
 * La funzione stampa il messaggio di errore (formattato) passato in ingresso 
 * 
 * @Since 20130110
 */
function print_error($error_message) {
    echo '<div id="error">' . $error_message . '</div>';
}

/**
 * La funzione stampa il messaggio di successo (formattato) passato in ingresso
 *
 * @Since 20130110
 */
function print_success($success_message) {
    echo '<div id="success">' . $success_message . '</div>';
}

/**
 * La funzione tenta di creare una connessione al server di database ed al 
 * database definiti come in cima a questo file. Se la connessione e' andata
 * a buon fine, allora la funzione torna la connessione di ritorno, altrimenti
 * lo script termina stampando un messaggio d'errore.
 * 
 * @since 20130102
 */
function open_db_connection() {
	
	$error_message = '';
	
	$connection = mysql_connect( DB_HOST, DB_USER, DB_PASSWORD );
	
	if ( ! $connection ) {
		$error_message .= 'Impossibile connettersi al server: ' . mysql_error();
		print_error($error_message);
	}
	
	if ( ! mysql_select_db( DB_NAME, $connection )) {
		$error_message .= 'Impossibile aprire il db: '. mysql_error();
		print_error($error_message);
	}
	
	if(strlen($error_message) > 0) {
		died($error_message);
	} else {
		return $connection;
	}
}

/**
 * La funzione si occupa di mostrare a video le tabelle con gli ultimi 
 * aggiornamente al database, come i sistemi supportati, le piattaforma hardware
 * supportate, gli ultimi bug inseriti, gli ultimi utenti registrati ecc...
 * 
 * La funzione non accetta alcun parametro in ingresso e nella prima porzione
 * di codice tentiamo di stabile una connessione al DB per recuperare i dati,
 * se tutto e' andato a buon fine, lo script elabora differenti query e tramite
 * la funzione show_last_update_result() inserisce i risultati in una tabella.
 * 
 * @Version 20130103:4
 * @Since 20130103
 */
function print_table($query, $table) {
	 
	$is_logged = (isset($_SESSION['loggedin'])) ? $_SESSION['loggedin'] : FALSE;
	
	$error_message = "";
	
	$connection = open_db_connection();
	
	$result = mysql_query($query, $connection);
	
	if ( ! $result ) {
		$error_message .= 'Query errata!' . mysql_error();
		print_error($error_message);
	} else {

		$col_field = mysql_num_fields($result);
		
		echo "<table border='1'>";
		echo "<tr>";
		
		# stampo l'intestazione della tabella SQL data in ingresso
		for ($col = 0; $col < $col_field; $col++)
			echo "<th><span>" . mysql_field_name($result, $col) . "</span></th>";
		
		if($is_logged) {
			echo "<th><span>Modifica</span></th>";
			echo "<th><span>Elimina</span></th>";
		}
		echo "</tr>";
		
		# recupero il nome della prima colonna nella tabella
		$the_first_col = mysql_field_name($result, 0);
		
		#costruzione del link
		$before_link = '<td class="img_col">';
		$after_link = '</td>';
		$page = $table .'.php';
		$id = $the_first_col;
		
		# stampo tutte le righe della tabella SQL data in ingresso
		while ($row = mysql_fetch_row($result)) {
		    
			echo '<tr>';
		
			foreach($row as $field)
				echo "<td><span>$field</span></td>";
			
			if($is_logged) {
	            echo $before_link.'<a href="'.$page.'?action=edit&store=update&'.$id.'='.$row[0].'"><img src="img/edit_icon.png"></a>'.$after_link;
				echo $before_link.'<a href="'.$page.'?action=delete&'.$id.'='.$row[0].'"><img src="img/delete_icon.png"></a></td>'.$after_link;
			} 
			echo'</tr>';
		}
		echo '</table>';
	}	
	mysql_close($connection);
}

function print_bug_table() {
	
	$query = 'SELECT b.id_bug, o.nome AS sistema, p.nome AS piattaforma, pr.nome AS prodotto, b.riassunto
		FROM bug AS b, sistema_operativo AS o, piattaforma_hardware AS p, prodotto AS pr
		WHERE b.id_os=o.id_os AND b.id_hp=p.id_hp AND b.id_prodotto=pr.id_prodotto
		ORDER BY b.id_bug DESC
		LIMIT 0, 15';
	
	print_table($query, 'bug');
}

function print_user_table() {
	
	$query = 'SELECT id_utente, nome, cognome, via, citta, stato, cap
			  FROM utenti
			  ORDER BY id_utente DESC
			  LIMIT 0, 15';
	
	print_table($query, 'utenti');
}


/**
 *
 *
 *
 * @Since 20130103
 */
function check_user_data($data) {

	$error_message = '';

	// Controllo i campi obbligatori per assicurarmi la correttezza dei dati
	if(strcmp($data['password'], $data['passwd_rp'])) {
		$error_message .= "<p>La password non coincide</p>";
		died($error_message);
	}
	$str_email = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	if( ! preg_match($str_email, $data['email'])) {
		$error_message .= "<p>L''indirizzo email inserito non e'' valido</p>";
	}
	$string_exp = "/^[A-Za-z .'-]+$/";
	if( ! preg_match($string_exp, $data['firstname'])) {
		$error_message .= "<p>Il nome inserito non e'' valido</p>";
	}
	if( ! preg_match($string_exp, $data['lastname'])) {
		$error_message .= "<p>Il cognome inserito non e'' valido</p>";
	}
	$str_zipcode =  "/^[0-9]{5}$/";
	if( ! preg_match($str_zipcode, $data['zipcode'])) {
		$error_message .= "<p>Il CAP inserito non e'' valido</p>";
	}
	$str_address = "/^[A-Za-z]{3,} [0-9]{1,4}$/";
	if( ! preg_match($str_address, $data['address'])) {
		$error_message .= "<p>L''indirizzo inserito non e'' valido</p>";
	}
	if(strlen($error_message) > 0) {
		died($error_message);
	} else {

		$connection = open_db_connection();
		
		$username = $data['username'];

		$query = "SELECT username FROM utenti WHERE username='$username'";

		$count = mysql_affected_rows();

		if($count == 1) {
			$error_message .= "<p>L'username non e' valido!</p>";
			mysql_close($connection);
			died($error_message);
		} else {
			mysql_close($connection);
			return true;
		}
	}
}

/**
 * 
 * 
 * 
 */
function check_bug_data($summary) {
	
	$error_message = "";
	
	$string_exp = "/^[A-Za-z0-9 .'-]+$/";
	if( ! preg_match($string_exp, $summary)) {
		$error_message .= "<p>Il riassunto non e'' valido.</p>";
	}
	
	if(strlen($error_message) > 0) {
		died($error_message);
	} else {
		return true;
	}
}
		
/**
 * 
 * @Version  20130108:1
 * @Since 20130108
 */
function get_login_perform() {
	
	ob_start();
	
	$error_message = "";
	
	# Controllo se i campi obbligatori esistono gia'.
	if(isset($_POST['username']) && isset($_POST['password'])) {
	
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		# Connessione al server e selezione del DB
		$connection = open_db_connection();
			
		$query = "SELECT id_utente FROM utenti
				  WHERE username='$username' and password='$password'";
		
		$result = mysql_query($query);
		
		while($row = mysql_fetch_array($result))
			$user_id = $row['0'];
		
		# Guardiamo se l'utente inserito esiste
		$count = mysql_num_rows($result);
		
		if($count == 1) {
			$_SESSION['user_id'] = $user_id;
			$_SESSION['loggedin'] = true;
			
			echo "<div id='success'>Login avvenuto con successo!</div>";
			header('Refresh:3; URL=index.php');
			
		} else {
			$error_message .= "Username o Password non sono corretti. Se
			il problema persiste, scrivi a admin@bugzilla.com";
			died($error_message);
		}
		
		ob_end_flush();
	}
}
?>